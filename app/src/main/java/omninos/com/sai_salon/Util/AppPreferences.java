package omninos.com.sai_salon.Util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

public class AppPreferences {

    private static AppPreferences appPreferences;
    private SharedPreferences sharedPreferences;

    public List<String> getSalonlist() {
        return salonlist;
    }

    public void setSalonlist(List<String> salonlist) {
        this.salonlist = salonlist;
    }

    private List<String>salonlist;

    private AppPreferences(Context context){
        sharedPreferences=context.getSharedPreferences("SaiSalonProvider",0);

    }
    public static AppPreferences init(Context context){
        if(appPreferences==null){
            appPreferences=new AppPreferences(context);
        }
        return appPreferences;

    }
    public void SaveString(String key,String value){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public String GetString(String key){
        return sharedPreferences.getString(key,"");
    }
    public  void saveLanguage(Activity activity, String id){
        SharedPreferences sharedPreferences=activity.getSharedPreferences("SaiSalon",0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("Lng",id);
        editor.commit();
    }

    public  String getLanguage(Activity activity){
        SharedPreferences sharedPreferences=activity.getSharedPreferences("SaiSalon",0);
        return sharedPreferences.getString("Lng","");
    }

    public void Logout()
    {
        sharedPreferences.edit().clear().apply();
    }
}
