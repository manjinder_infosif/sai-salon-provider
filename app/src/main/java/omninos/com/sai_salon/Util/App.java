package omninos.com.sai_salon.Util;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    private static App instance;
    private Context context;

    private static AppPreferences appPreferences;
    private static SingletonPojo singletonPojo;

    public static App getContext(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;
        appPreferences=AppPreferences.init(context);
        singletonPojo=new SingletonPojo();
    }

    public static SingletonPojo getSingletonPojo() {
        return singletonPojo;
    }

    public static AppPreferences getAppPreferences() {
        return appPreferences;
    }
}
