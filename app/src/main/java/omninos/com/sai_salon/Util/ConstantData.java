package omninos.com.sai_salon.Util;

import java.util.ArrayList;
import java.util.List;

public class ConstantData {
    public static final String USER_ID="user_id";
    public static final String IMAGEPATH="imagepath";
    public static final String TOKEN="token";
    public static final String SALONIMAGE="salonimage";
    public static final String SALONNAME="salonname";
    public static final String SALONNAME_ARABIC="arabicsalonname";
    public static final String EMAIL="email";
    public static final String PHONE="phone";
    public static final String EMPLOYEEIMAGE="employeeimage";
    public static final String EMPLOYEEIMAGE_POSITION="imagepositin";
    public static final String STATUS="status";
    public static final String PACKAGEID="packageid";
    public static final String LANG_STATUS="langstatus";

    public static final List<String>status=new ArrayList<>();
}
