package omninos.com.sai_salon.Util;

import java.util.List;

import omninos.com.sai_salon.Modelclass.EmployessList;
import omninos.com.sai_salon.PojoClasses.ServicesListpojo;

public class SingletonPojo {
      private List<String>servicesImages;
      private List<String>servicenames;
      private List<String>photoslist;
      private List<String>salontypes;
      private List<EmployessList>employeelist;
      private List<ServicesListpojo>serviceslist;
      private List<String> servicesid;
      int employeimage_positon;
      String photoid;
    private String timeid,latitude,longitude,acceptrejectstatus, bookingid,phoneNumber,packageid,address,coupon,cardtype,cardno,cardcvv,cardexpiry,userid,useremail,salonphotopath,motno,licensecopy,name,surname,location,photouploadpath;

    public List<String> getSalontypestatus() {
        return salontypestatus;
    }

    public void setSalontypestatus(List<String> salontypestatus) {
        this.salontypestatus = salontypestatus;
    }

    public String getPhotoid() {
        return photoid;
    }

    public void setPhotoid(String photoid) {
        this.photoid = photoid;
    }

    public String getTimeid() {
        return timeid;
    }

    public void setTimeid(String timeid) {
        this.timeid = timeid;
    }

    public String getLatitude() {
        return latitude;
    }



    public int getEmployeimage_positon() {
        return employeimage_positon;
    }

    public void setEmployeimage_positon(int employeimage_positon) {
        this.employeimage_positon = employeimage_positon;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    private List<String>salontypestatus;

    public String getAcceptrejectstatus() {
        return acceptrejectstatus;
    }

    public void setAcceptrejectstatus(String acceptrejectstatus) {
        this.acceptrejectstatus = acceptrejectstatus;
    }

    public String getBookingid() {
        return bookingid;
    }

    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPackageid() {
        return packageid;
    }


    public void setPackageid(String packageid) {
        this.packageid = packageid;
    }


      public List<ServicesListpojo> getServiceslist() {
        return serviceslist;
    }

    public void setServiceslist(List<ServicesListpojo> serviceslist) {
        this.serviceslist = serviceslist;
    }

    public List<String> getSalontypes() {
        return salontypes;
    }

    public void setSalontypes(List<String> salontypes) {
        this.salontypes = salontypes;
    }

    public List<String> getServicesid() {
        return servicesid;
    }

    public void setServicesid(List<String> servicesid) {
        this.servicesid = servicesid;
    }

    public List<EmployessList> getEmployeelist() {
        return employeelist;
    }

    public String getCoupon() {
        return coupon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public void setEmployeelist(List<EmployessList> employeelist) {
        this.employeelist = employeelist;
    }

    public List<String> getServicesImages() {
        return servicesImages;
    }

    public List<String> getPhotoslist() {
        return photoslist;
    }

    public String getCardcvv() {
        return cardcvv;
    }

    public String getCardexpiry() {
        return cardexpiry;
    }

    public String getCardno() {
        return cardno;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public void setCardcvv(String cardcvv) {
        this.cardcvv = cardcvv;
    }

    public void setCardexpiry(String cardexpiry) {
        this.cardexpiry = cardexpiry;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public void setPhotoslist(List<String> photoslist) {
        this.photoslist = photoslist;
    }

    public List<String> getServicenames() {
        return servicenames;
    }

    public void setServicenames(List<String> servicenames) {
        this.servicenames = servicenames;
    }

    public void setServicesImages(List<String> servicesImages) {
        this.servicesImages = servicesImages;
    }

    public void setuserId(String id) {
        userid = id;
    }

    public String getuserId() {
        return userid;
    }

    public String getPhotouploadpath() {
        return photouploadpath;
    }

    public void setPhotouploadpath(String photouploadpath) {
        this.photouploadpath = photouploadpath;
    }

    public String getUseremail() {
        return useremail;
    }



    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getLicensecopy() {
        return licensecopy;

    }

    public void setLicensecopy(String licensecopy) {
        this.licensecopy = licensecopy;
    }

    public String getSalonphotopath() {
        return salonphotopath;
    }

    public void setSalonphotopath(String salonphotopath) {
        this.salonphotopath = salonphotopath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMotno() {
        return motno;
    }

    public void setMotno(String motno) {
        this.motno = motno;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }



    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }



}
