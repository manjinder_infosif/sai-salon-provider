package omninos.com.sai_salon.Modelclass;

public class EmployessList {
    public String employeename,employeeemail,employeephone,employeeimage;

    public String getEmployeeemail() {
        return employeeemail;
    }

    public void setEmployeeemail(String employeeemail) {
        this.employeeemail = employeeemail;
    }

    public String getEmployeephone() {
        return employeephone;
    }

    public void setEmployeephone(String employeephone) {
        this.employeephone = employeephone;
    }

    public String getEmployeeimage() {
        return employeeimage;
    }

    public void setEmployeeimage(String employeeimage) {
        this.employeeimage = employeeimage;
    }


    public String getEmployeename() {
        return employeename;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename;
    }
}
