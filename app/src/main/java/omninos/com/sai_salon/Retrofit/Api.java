package omninos.com.sai_salon.Retrofit;

import org.json.JSONArray;

import java.util.Map;

import omninos.com.sai_salon.PojoClasses.AcceptRejectPojo;
import omninos.com.sai_salon.PojoClasses.AddAddressPojo;
import omninos.com.sai_salon.PojoClasses.AddEMployeepojo;
import omninos.com.sai_salon.PojoClasses.BookinglistPojo;
import omninos.com.sai_salon.PojoClasses.BusinessPojo;
import omninos.com.sai_salon.PojoClasses.CheckaddresssPojo;
import omninos.com.sai_salon.PojoClasses.ConversationMessagepojo;
import omninos.com.sai_salon.PojoClasses.DeleteEmployeepojo;
import omninos.com.sai_salon.PojoClasses.DeleteOfferPojo;
import omninos.com.sai_salon.PojoClasses.DeletePojo;
import omninos.com.sai_salon.PojoClasses.DeleteServicepojo;
import omninos.com.sai_salon.PojoClasses.EditProfilePojo;
import omninos.com.sai_salon.PojoClasses.EditTimeslotspojo;
import omninos.com.sai_salon.PojoClasses.ForgetPasswordPojo;
import omninos.com.sai_salon.PojoClasses.GetAdminIdPojo;
import omninos.com.sai_salon.PojoClasses.GetCategoryModel;
import omninos.com.sai_salon.PojoClasses.GetCityPojo;
import omninos.com.sai_salon.PojoClasses.GetContactPojo;
import omninos.com.sai_salon.PojoClasses.GetCountryListPojo;
import omninos.com.sai_salon.PojoClasses.GetCoupondetails;
import omninos.com.sai_salon.PojoClasses.GetEmployeelist;
import omninos.com.sai_salon.PojoClasses.GetHomeServiceListModel;
import omninos.com.sai_salon.PojoClasses.GetOffersListPojo;
import omninos.com.sai_salon.PojoClasses.GetPendingjobsPojo;
import omninos.com.sai_salon.PojoClasses.GetServiceIDPojo;
import omninos.com.sai_salon.PojoClasses.GetServicesnamelist;
import omninos.com.sai_salon.PojoClasses.GetStatePojo;
import omninos.com.sai_salon.PojoClasses.GetUpcomingJobs;
import omninos.com.sai_salon.PojoClasses.ImagesPojo;
import omninos.com.sai_salon.PojoClasses.Loginpojo;
import omninos.com.sai_salon.PojoClasses.NotificationPojo;
import omninos.com.sai_salon.PojoClasses.OnlineOfflinestatus;
import omninos.com.sai_salon.PojoClasses.Packagespojoo;
import omninos.com.sai_salon.PojoClasses.ProviderSpecialOffersPojo;
import omninos.com.sai_salon.PojoClasses.RegisterModelClass;
import omninos.com.sai_salon.PojoClasses.SalonSpecialOfferspojo;
import omninos.com.sai_salon.PojoClasses.SalonimagesPojo;
import omninos.com.sai_salon.PojoClasses.SendMessagePojo;
import omninos.com.sai_salon.PojoClasses.ServicesListpojo;
import omninos.com.sai_salon.PojoClasses.Servicestypesspojo;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.PojoClasses.ShowRatingPojo;
import omninos.com.sai_salon.PojoClasses.TimeslotPojo;
import omninos.com.sai_salon.PojoClasses.UpdateAddserviesPojo;
import omninos.com.sai_salon.PojoClasses.UpdatePhotoPojooo;
import omninos.com.sai_salon.PojoClasses.UpdateServiceslistPojo;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {
    @Multipart
    @POST("providerRegister")
    Call<RegisterModelClass> registration(
//            @Part("language") RequestBody language,
            @Part("salonName") RequestBody salonName,
            @Part("phoneNumber") RequestBody phoneNumber,
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
//                                          @Part("motNumber") RequestBody motNumber,
            @Part("salonType") RequestBody salonType,
            @Part("kidsStatus") RequestBody kidsStatus,
            @Part("reg_id") RequestBody reg_id,
            @Part("device_type") RequestBody device_type,
            @Part MultipartBody.Part licenseCopy,
            @Part MultipartBody.Part salonImage,
            @Part("homeServiceStatus") RequestBody homeServiceStatus,
            @Part("phoneCode") RequestBody flagCode,
//                                          @Part("country") RequestBody country,
            @Part("salonNameArbic") RequestBody salonNameArbic,
            @Part("licenseNumberAndCivilNumber") RequestBody licenseNumberAndCivilNumber);


//    @Multipart
//    @POST("addProviderInformation")
//    Call<ProviderinfoPojo> information(@Part("userId") RequestBody userid,
//                                       @Part("motNumber") RequestBody motNumber,
//                                       @Part("licenseCopy") RequestBody licenseCopy,
//                                       @Part("name") RequestBody name,
//                                       @Part("email") RequestBody email,
//                                       @Part("address") RequestBody address,
//                                       @Part("location") RequestBody location,
//                                       @Part("services[]") RequestBody[] services,
//                                       @Part("sundayStartTime") RequestBody sundayStartTime,
//                                       @Part("sundayCloseTime") RequestBody sundayCloseTime,
//                                       @Part("mondayStartTime") RequestBody mondayStartTime,
//                                       @Part("modayCloseTime") RequestBody mondayCloseTime,
//                                       @Part("tuesdayStartTime") RequestBody tuesdayStartTime,
//                                       @Part("tuesdayCloseTime") RequestBody tuesdayCloseTime,
//                                       @Part("wednessStartTime") RequestBody wednessStartTime,
//                                       @Part("wednessCloseTime") RequestBody wednessCloseTime,
//                                       @Part("thursdayStartTime") RequestBody thursdayStartTime,
//                                       @Part("thursdayCloseTime") RequestBody thursdayCloseTime,
//                                       @Part("fridayStartTime") RequestBody fridayStartTime,
//                                       @Part("fridayCloseTime") RequestBody fridayCloseTime,
//                                       @Part("saturdayStartTime") RequestBody saturdayStartTime,
//                                       @Part("saturdayCloseTime") RequestBody saturdayCloseTime,
//                                       @Part MultipartBody.Part salonLogo,
//                                       @Part("employees[]") RequestBody[] employees,
//                                       @Part("cardType") RequestBody cardType,
//                                       @Part("cardNumber") RequestBody cardNumber,
//                                       @Part("cardExpiry") RequestBody cardExpiry,
//                                       @Part("cardCvv") RequestBody cardCvv,
//                                       @Part("coupon") RequestBody coupon,
//                                       @Part("surname") RequestBody surname);
//

    @GET("serviceList")
    Call<ServicesListpojo> services();

    @GET("getPackageInformation")
    Call<Packagespojoo> getpackages();

    @FormUrlEncoded
    @POST("providerLogin")
    Call<Loginpojo> userlogin(@Field("email") String email,
                              @Field("password") String password,
                              @Field("reg_id") String reg_id,
                              @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("providerPackage")
    Call<Map> providerpackage(@Field("packageId") String packageid,
                              @Field("userId") String userid);


    @FormUrlEncoded
    @POST("getProviderService")
    Call<GetHomeServiceListModel> getServiceList(@Field("providerId") String providerId);

    @FormUrlEncoded
    @POST("getProviderCategory")
    Call<GetCategoryModel> getCategory(@Field("serviceType") String servicetype);

    @FormUrlEncoded
    @POST("providerAddService")
    Call<Map> addNewService(@Field("serviceType") String serviceType,
                            @Field("providerId") String providerId,
                            @Field("serviceLocation") String servicelocation,
                            @Field("categoryId") String categoryId,
                            @Field("title") String title,
                            @Field("arabicTitle") String arabictitle,
                            @Field("salePrice") String salesprice,
                            @Field("serviceTime") String servicetime);


    @FormUrlEncoded
    @POST("proiderSerivceType")
    Call<Servicestypesspojo> getservicetype(@Field("providerId") String providerId);

    @FormUrlEncoded
    @POST("getUpcomingJobs")
    Call<GetUpcomingJobs> getupcomingjobs(@Field("providerId") String providerId);

    @FormUrlEncoded
    @POST("getPendingJobs")
    Call<GetPendingjobsPojo> getpendingjobs(@Field("providerId") String providerId);

    @FormUrlEncoded
    @POST("providerUpdatePassword")
    Call<ForgetPasswordPojo> updatedepasswd(@Field("phone") String phone,
                                            @Field("password") String password);

    @FormUrlEncoded
    @POST("getProviderOffersNew")
    Call<ProviderSpecialOffersPojo> getspecialoffers(@Field("providerId") String providerid);


    @FormUrlEncoded
    @POST("bookingNotifications")
    Call<NotificationPojo> notify(@Field("providerId") String providerid);

    @FormUrlEncoded
    @POST("acceptRejectBookingServicesNew")
    Call<AcceptRejectPojo> acceptrejectstatus(@Field("status") String status,
                                              @Field("bookingId") String bookingid);

    @Multipart
    @POST("sendMessage")
    Call<SendMessagePojo> sendMessage(@Part("sender_id") RequestBody senderid,
                                      @Part("reciver_id") RequestBody receiverid,
                                      @Part("type") RequestBody type,
                                      @Part("chatType") RequestBody chattype,
                                      @Part("message") RequestBody message,
                                      @Part MultipartBody.Part image,
                                      @Part("messageType")RequestBody messagetype);

    @GET("getSuperAdminId")
    Call<GetAdminIdPojo> adminid();

    @FormUrlEncoded
    @POST("ConversationMessage")
    Call<ConversationMessagepojo> conversation(@Field("sender_id") String senderid,
                                               @Field("reciver_id") String receiverid,
                                               @Field("chatType") String chattype);

    @FormUrlEncoded
    @POST("providerGetServiceListNew")
    Call<UpdateAddserviesPojo> updateservices(@Field("serviceId") String serviceid,
                                              @Field("providerId") String providerid);

    @FormUrlEncoded
    @POST("updateProviderService")
    Call<UpdateServiceslistPojo> getupdated(@Field("serviceType") String serviceType,
                                            @Field("providerId") String providerId,
                                            @Field("serviceLocation") String servicelocation,
                                            @Field("categoryId") String categoryId,
                                            @Field("title") String title,
                                            @Field("arabicTitle") String arabictitle,
                                            @Field("salePrice") String salesprice,
                                            @Field("serviceTime") String servicetime,
                                            @Field("serviceId") String serviceid);

    @FormUrlEncoded
    @POST("getProviderRatingList")
    Call<ShowRatingPojo> getlist(@Field("providerId") String providerId);


    @FormUrlEncoded
    @POST("providerBookingNotifications")
    Call<BookinglistPojo> booking(@Field("providerId") String providerid);

    @GET("contactUs")
    Call<GetContactPojo> getcontactdetails();

    @FormUrlEncoded
    @POST("getProviderTimeSlots")
    Call<TimeslotPojo> gettimeslot(@Field("providerId") String providerId);


    @Multipart
    @POST("providerAddImages")
    Call<ImagesPojo> sendimages(@Part("providerId") RequestBody providerId,
                                @Part MultipartBody.Part image[]);

    @FormUrlEncoded
    @POST("getProviderBusinessDetails")
    Call<BusinessPojo> getprofile(@Field("providerId") String providerId);


    @Multipart
    @POST("providerAddEmployee")
    Call<AddEMployeepojo> sendemplyess(@Part MultipartBody.Part image,
                                       @Part("name") RequestBody name,
                                       @Part("email") RequestBody email,
                                       @Part("phone") RequestBody phone,
                                       @Part("providerId") RequestBody providerId);


    @FormUrlEncoded
    @POST("providerEditProfile")
    Call<EditProfilePojo> editprofile(@Field("providerId") String providerId,
                                      @Field("name") String name,
                                      @Field("email") String email,
                                      @Field("phone") String phone);

    @FormUrlEncoded
    @POST("editTimeSlots")
    Call<EditTimeslotspojo> edittime(@Field("timeSlots") JSONArray timeSlots,
                                     @Field("providerId") String ptoviderid);


    @FormUrlEncoded
    @POST("providerSalonImage")
    Call<SalonimagesPojo> getsalonimages(@Field("providerId") String providerId);


    @FormUrlEncoded
    @POST(" providerOnlineOfflineStatusNew")
    Call<OnlineOfflinestatus> getstatus(@Field("providerId") String providerId,
                                        @Field("status") String status);

    @FormUrlEncoded
    @POST(" providerGetServiceNew")
    Call<GetServicesnamelist> getserviceslist(@Field("providerId") String providerId,
                                              @Field("serviceType") String serviceType,
                                              @Field("serviceLocation") String servicelocation);

    @Multipart
    @POST("providerAddSalonOffers")
    Call<SalonSpecialOfferspojo> specialoffers(
            @Part("providerId") RequestBody providerId,
            @Part("servicesId") RequestBody servicesId,
            @Part("serviceType") RequestBody serviceType,
            @Part("serviceLocation") RequestBody serviceLocation,
            @Part("title") RequestBody title,
            @Part("offer") RequestBody offer,
            @Part MultipartBody.Part image,
            @Part("sdate")RequestBody startdate,
            @Part("edate")RequestBody enddate,
            @Part("arabicTitle")RequestBody arabictitle,
            @Part("totalPrice")RequestBody totalprice,
            @Part("price")RequestBody price);


    @Multipart
    @POST(" providerUpdateImages")
    Call<UpdatePhotoPojooo> updatephotos(@Part("id") RequestBody id,
                                         @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("providerDeleteImages")
    Call<DeletePojo> deletephotos(@Field("id") String id);


    @FormUrlEncoded
    @POST("applyCoupen")
    Call<GetCoupondetails> getcoupon(@Field("providerId") String providerId,
                                     @Field("coupenName") String coupenName,
                                     @Field("packageId") String packageId);

    @GET("getCountry")
    Call<GetCountryListPojo> getcountries();

    @FormUrlEncoded
    @POST("getStates")
    Call<GetStatePojo> getstate(@Field("countryId") String id);

    @FormUrlEncoded
    @POST("getCities")
    Call<GetCityPojo> getcities(@Field("stateId") String id);

    @FormUrlEncoded
    @POST("getProviderOfferList")
    Call<GetOffersListPojo> getofferslist(@Field("providerId") String providerid);


    @FormUrlEncoded
    @POST("providerDeleteOffers")
    Call<DeleteOfferPojo> deleteoffer(@Field("id") String id);


    @FormUrlEncoded
    @POST("providerDeleteServicess")
    Call<DeleteServicepojo> deletservice(@Field("id") String id);

    @FormUrlEncoded
    @POST("addSalonAddress")
    Call<AddAddressPojo> add_address(@Field("providerId") String providerId,
                                     @Field("address") String address,
                                     @Field("cityid") String cityid,
                                     @Field("pinCode") String pinCode,
                                     @Field("stateid") String stateid,
                                     @Field("countryid") String countryid);


    @FormUrlEncoded
    @POST("getSalonAddressDetails")
    Call<CheckaddresssPojo> checkaddress(@Field("providerId") String id);

    @GET("getAllTimeSlots")
    Call<TimeslotPojo> newtimeslot();

    @FormUrlEncoded
    @POST("getProviderEmployee")
    Call<GetEmployeelist> getemployee(@Field("providerId") String id);


    @FormUrlEncoded
    @POST("deleteProviderEmployee")
    Call<DeleteEmployeepojo> deleteemployee(@Field("employeeId") String id);


    @FormUrlEncoded
    @POST("getServiceTotalPrice")
    Call<GetServiceIDPojo> serviceidget(@Field("servicesId") String id);

}
// @Part MultipartBody.Part[] images,