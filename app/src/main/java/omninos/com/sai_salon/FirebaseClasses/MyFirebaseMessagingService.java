package omninos.com.sai_salon.FirebaseClasses;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import omninos.com.sai_salon.Activities.AcceptRejectActivity;
import omninos.com.sai_salon.Activities.ChatApplication;
import omninos.com.sai_salon.Activities.HomeActivity;
import omninos.com.sai_salon.Activities.MessageActivity;
import omninos.com.sai_salon.Activities.WebViewActivity;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
//vibrate'	=> 1,
//						'sound'		=> 1,
//						'largeIcon'	=> 'large_icon',
//						'smallIcon'	=> 'small_icon',
//						'link' => $this->input->post('link'),
//						'image' => $path,
//						'type'      => 'linkMessage'
//   linkMessage 'message' 	=> $this->input-
//						'title'		=> 'Sai Salon',
//                                'subtitle'	=> $this->input-subtitle

//----------'image' => $path,
//						'type' => 'simpleMessage'

//    'title'		=> 'Sai Salon',
//            'subtitle'	=> 'Response',
//            'vibrate'	=> 1,
//            'sound'		=> 1,
//            'largeIcon'	=> 'large_icon',
//            'smallIcon'	=> 'small_icon',
//            'type'      => 'userBooking',
//            'username' => $userDeta['businessName'],
//            'userImage' => $userDeta['image'],
//            'userAddress' => $userDeta['address'],
//            'services' =>$finalTitle,
//            'appointmentDate' => $detaa['serviceDate'],
//            'appointmentTime' =>
    // Bundle[{subtitle=saisalon, google.delivered_priority=normal, google.sent_time=1564147565235, smallIcon=small_icon, google.ttl=2419200, google.original_priority=normal, from=606137491648, type=message, sound=1, title=Sai Salon, vibrate=1, google.message_id=0:1564147565242677%af4f8797f9fd7ecd, largeIcon=large_icon, message=qwerty }]

    String subtitle, image, title, message, useremail, link, username, userImage, userAddress, services, AppointmentDate, Appointmenttime, type, bookingId;

    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;
    NotificationChannel mChannel;
    Notification notification;
    Uri defaultSound;
    Bitmap imageBit;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        title = remoteMessage.getData().get("title");
        username = remoteMessage.getData().get("username");
        useremail = remoteMessage.getData().get("userEmail");
        userImage = remoteMessage.getData().get("userImage");
        userAddress = remoteMessage.getData().get("userAddress");
        services = remoteMessage.getData().get("services");
        image = remoteMessage.getData().get("image");
        link = remoteMessage.getData().get("link");
        AppointmentDate = remoteMessage.getData().get("appointmentDate");
        subtitle = remoteMessage.getData().get("subtitle");
        Appointmenttime = remoteMessage.getData().get("appointmentTime");
        type = remoteMessage.getData().get("type");
        message = remoteMessage.getData().get("message");
        bookingId = remoteMessage.getData().get("bookingId");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setOreoNotification(title, message, username, userImage, userAddress, services, AppointmentDate, Appointmenttime, type, bookingId, useremail, link, image, subtitle);
            if (type.equalsIgnoreCase("message")) {
                this.startForegroundService(new Intent(this, ChatApplication.class));
            } else if (type.equalsIgnoreCase("userBooking")) {

                this.startForegroundService(new Intent(this, AcceptRejectActivity.class));
            } else if (type.equalsIgnoreCase("linkMessage")) {
                this.startForegroundService(new Intent(this, WebViewActivity.class));
            } else if (type.equalsIgnoreCase("simpleMessage")) {
                this.startForegroundService(new Intent(this, HomeActivity.class));
            }
        } else {
            showNotification(title, message, username, userImage, userAddress, services, AppointmentDate, Appointmenttime, type, bookingId, useremail, link, image, subtitle);
            // this.startService(new Intent(this, AcceptRejectActivity.class));
            if (type.equalsIgnoreCase("message")) {
                this.startService(new Intent(this, ChatApplication.class));
            } else if (type.equalsIgnoreCase("userBooking")) {

                this.startService(new Intent(this, AcceptRejectActivity.class));
            } else if (type.equalsIgnoreCase("linkMessage")) {
                this.startService(new Intent(this, WebViewActivity.class));
            } else if (type.equalsIgnoreCase("simpleMessage")) {
                this.startService(new Intent(this, HomeActivity.class));
            }
        }

    }

    private void showNotification(String title, String message, String username, String userImage, String userAddress, String services, String appointmentDate, String appointmenttime, String type, String bookingId, String useremail, String link, String image, String subtitle) {
        Intent intent = new Intent();
        if (type.equalsIgnoreCase("userBooking")) {
            intent = new Intent(this, AcceptRejectActivity.class);
            intent.putExtra("From", "Notification");
            intent.putExtra("username", username);
            intent.putExtra("userImage", userImage);
            intent.putExtra("userAddress", userAddress);
            intent.putExtra("services", services);
            intent.putExtra("appointmentDate", appointmentDate);
            intent.putExtra("appointmenttime", appointmenttime);
            intent.putExtra("bookingId", bookingId);
            intent.putExtra("usereamil", useremail);
            App.getSingletonPojo().setBookingid(bookingId);
        } else if (type.equalsIgnoreCase("message")) {
            intent = new Intent(this, ChatApplication.class);
        } else if (type.equalsIgnoreCase("linkMessage")) {
            intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("Link", link);
            intent.putExtra("Subtitle", subtitle);
            intent.putExtra("Message", message);
        } else if (type.equalsIgnoreCase("simpleMessage")) {
            intent = new Intent(this, HomeActivity.class);
       ////     intent.putExtra("Image", image);
         //   intent.putExtra("Subtitle", subtitle);
        //    intent.putExtra("Message", message);
            // intent.putExtra("Title",title);
        }
        URL url = null;
        try {
            url = new URL(image);
            imageBit = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentText(subtitle)
                .setContentTitle(message)
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(imageBit))
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);

    }

    private void setOreoNotification(String title, String message, String username, String userImage, String userAddress, String services, String appointmentDate, String appointmenttime, String type, String bookingId, String usereamil, String link, String image, String subtitle) {
        Intent intent = new Intent();
        if (type.equalsIgnoreCase("userBooking")) {
            intent = new Intent(this, AcceptRejectActivity.class);
            intent.putExtra("From", "Notification");
            intent.putExtra("username", username);
            intent.putExtra("userImage", userImage);
            intent.putExtra("userAddress", userAddress);
            intent.putExtra("services", services);
            intent.putExtra("appointmentDate", appointmentDate);
            intent.putExtra("appointmenttime", appointmenttime);
            intent.putExtra("bookingId", bookingId);
            intent.putExtra("usereamil", usereamil);
            App.getSingletonPojo().setBookingid(bookingId);
        } else if (type.equalsIgnoreCase("message")) {
            intent = new Intent(this, ChatApplication.class);
        } else if (type.equalsIgnoreCase("linkMessage")) {
            intent = new Intent(this, WebViewActivity.class);
            intent.putExtra("Link", link);
            intent.putExtra("Subtitle", subtitle);
            intent.putExtra("Message", message);
            //  intent.putExtra("Title",title);
        } else if (type.equalsIgnoreCase("simpleMessage")) {
            intent = new Intent(this, HomeActivity.class);
            //intent.putExtra("Image", image);
           // intent.putExtra("Subtitle", subtitle);
           // intent.putExtra("Message", message);
            // intent.putExtra("Title",title);
        }
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Sets an ID for the notification, so it can be updated.
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "ove";// The user-visible name of the channel.

        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
        URL url = null;
        try {
            url = new URL(image);
            imageBit = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

// Create a notification and set the notification channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(this)
                    .setContentTitle(subtitle)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.logo)
                    .setSound(defaultSound)
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setChannelId(CHANNEL_ID)
                    .setStyle(new Notification.BigPictureStyle().bigPicture(imageBit))
                    .build();
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }

// Issue the notification.
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }
}
