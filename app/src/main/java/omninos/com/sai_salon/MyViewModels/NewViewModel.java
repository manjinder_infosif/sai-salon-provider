package omninos.com.sai_salon.MyViewModels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import omninos.com.sai_salon.PojoClasses.AddAddressPojo;
import omninos.com.sai_salon.PojoClasses.CheckaddresssPojo;
import omninos.com.sai_salon.PojoClasses.DeleteEmployeepojo;
import omninos.com.sai_salon.PojoClasses.DeleteOfferPojo;
import omninos.com.sai_salon.PojoClasses.DeleteServicepojo;
import omninos.com.sai_salon.PojoClasses.GetEmployeelist;
import omninos.com.sai_salon.PojoClasses.GetOffersListPojo;
import omninos.com.sai_salon.Retrofit.Api;
import omninos.com.sai_salon.Retrofit.ApiClient;
import omninos.com.sai_salon.Util.Commonutil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewViewModel extends ViewModel {

    private MutableLiveData<GetOffersListPojo> offers;
    public LiveData<GetOffersListPojo> getofferlist(final Activity activity, String providerId){
        offers=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.getofferslist(providerId).enqueue(new Callback<GetOffersListPojo>() {
            @Override
            public void onResponse(Call<GetOffersListPojo> call, Response<GetOffersListPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    offers.setValue(response.body());
                   // Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetOffersListPojo> call, Throwable t) {
                Commonutil.dismiss();
               // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return offers;
    }

    private MutableLiveData<DeleteOfferPojo> deleteoffers;
    public LiveData<DeleteOfferPojo> deleteoffer(final Activity activity, String id){
        deleteoffers=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.deleteoffer(id).enqueue(new Callback<DeleteOfferPojo>() {
            @Override
            public void onResponse(Call<DeleteOfferPojo> call, Response<DeleteOfferPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    deleteoffers.setValue(response.body());
                  //  Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteOfferPojo> call, Throwable t) {
                Commonutil.dismiss();
               // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return deleteoffers;
    }

    private MutableLiveData<DeleteServicepojo> deletservices;
    public LiveData<DeleteServicepojo> deleteservices(final Activity activity, String id){
        deletservices=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.deletservice(id).enqueue(new Callback<DeleteServicepojo>() {
            @Override
            public void onResponse(Call<DeleteServicepojo> call, Response<DeleteServicepojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    deletservices.setValue(response.body());
                    Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteServicepojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return deletservices;
    }
    private MutableLiveData<AddAddressPojo> adddata;
    public LiveData<AddAddressPojo> add_addres(final Activity activity, String providerid, String address,String cityid,String pinCode,String stateid,String countryid){
        adddata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.add_address(providerid,address,cityid,pinCode,stateid,countryid).enqueue(new Callback<AddAddressPojo>() {
            @Override
            public void onResponse(Call<AddAddressPojo> call, Response<AddAddressPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    adddata.setValue(response.body());
                  //  Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddAddressPojo> call, Throwable t) {
                Commonutil.dismiss();
              //  Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return adddata;
    }

    private MutableLiveData<CheckaddresssPojo> chechaddress;
    public LiveData<CheckaddresssPojo> checkaddress(final Activity activity, String providerId){
        chechaddress=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.checkaddress(providerId).enqueue(new Callback<CheckaddresssPojo>() {
            @Override
            public void onResponse(Call<CheckaddresssPojo> call, Response<CheckaddresssPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    chechaddress.setValue(response.body());
                   // Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckaddresssPojo> call, Throwable t) {
                Commonutil.dismiss();
                // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return chechaddress;
    }

    private MutableLiveData<GetEmployeelist> getemployeww;
    public LiveData<GetEmployeelist> getemployess(final Activity activity, String providerId){
        getemployeww=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.getemployee(providerId).enqueue(new Callback<GetEmployeelist>() {
            @Override
            public void onResponse(Call<GetEmployeelist> call, Response<GetEmployeelist> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    getemployeww.setValue(response.body());
                    // Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetEmployeelist> call, Throwable t) {
                Commonutil.dismiss();
                // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return getemployeww;
    }

    private MutableLiveData<DeleteEmployeepojo> deletemp;
    public LiveData<DeleteEmployeepojo> deletemp(final Activity activity, String employeeid){
        deletemp=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.deleteemployee(employeeid).enqueue(new Callback<DeleteEmployeepojo>() {
            @Override
            public void onResponse(Call<DeleteEmployeepojo> call, Response<DeleteEmployeepojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    deletemp.setValue(response.body());
                    // Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteEmployeepojo> call, Throwable t) {
                Commonutil.dismiss();
                // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return deletemp;
    }


}
