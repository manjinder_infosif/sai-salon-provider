package omninos.com.sai_salon.MyViewModels;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import java.util.Map;

import omninos.com.sai_salon.PojoClasses.ForgetPasswordPojo;
import omninos.com.sai_salon.PojoClasses.GetPendingjobsPojo;
import omninos.com.sai_salon.PojoClasses.GetUpcomingJobs;
import omninos.com.sai_salon.PojoClasses.Loginpojo;
import omninos.com.sai_salon.PojoClasses.Packagespojoo;
import omninos.com.sai_salon.PojoClasses.RegisterModelClass;
import omninos.com.sai_salon.PojoClasses.ServicesListpojo;
import omninos.com.sai_salon.Retrofit.Api;
import omninos.com.sai_salon.Retrofit.ApiClient;
import omninos.com.sai_salon.Util.Commonutil;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewModell extends ViewModel {
    private MutableLiveData<RegisterModelClass>data;
    public LiveData<RegisterModelClass> register(final Activity activity,RequestBody language, RequestBody salonName, RequestBody phoneNumber, RequestBody email, RequestBody password, RequestBody motNumber, RequestBody salonType, RequestBody kidsStatus, RequestBody reg_id,RequestBody device_type,MultipartBody.Part licenseCopy, MultipartBody.Part salonImage,
                                                 RequestBody homeServicesStatus, RequestBody flagCode,RequestBody country,RequestBody arabicName,RequestBody licenceNumber
                                                 ){
        data=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.registration(salonName,phoneNumber,email,password,salonType,kidsStatus,reg_id,device_type,licenseCopy,salonImage,homeServicesStatus,flagCode,arabicName,licenceNumber).enqueue(new Callback<RegisterModelClass>() {
            @Override
            public void onResponse(Call<RegisterModelClass> call, Response<RegisterModelClass> response) {
             Commonutil.dismiss();
                if (response.isSuccessful()){
                    data.setValue(response.body());
                   Toast.makeText(activity, ""+data.getValue().getMessage(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(activity, ""+response.toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RegisterModelClass> call, Throwable t) {
//                Toast.makeText(activity, "", Toast.LENGTH_SHORT).show();
                Commonutil.dismiss();
            }
        });

        return data;
  }

    private MutableLiveData<ServicesListpojo>service;
    public LiveData<ServicesListpojo>getservices(final Activity activity) {
        service = new MutableLiveData();

        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Api api = ApiClient.getApiClient().create(Api.class);
        api.services().enqueue(new Callback<ServicesListpojo>() {
            @Override
            public void onResponse(Call<ServicesListpojo> call, Response<ServicesListpojo> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    service.setValue(response.body());
                    Toast.makeText(activity, "" + service.getValue().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ServicesListpojo> call, Throwable t) {
                Toast.makeText(activity, "" + service.getValue().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return service;
    }


    private MutableLiveData<Packagespojoo>packages;
    public LiveData<Packagespojoo>packageget( final Activity activity){
        packages=new MutableLiveData<>();
       Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.getpackages().enqueue(new Callback<Packagespojoo>() {
            @Override
            public void onResponse(Call<Packagespojoo> call, Response<Packagespojoo> response) {
                Commonutil.dismiss();
                if(response.isSuccessful()){
                    packages.setValue(response.body());
                    //Toast.makeText(activity, "" + packages.getValue().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Packagespojoo> call, Throwable t) {
                Toast.makeText(activity, "" + packages.getValue().getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return packages;
    }


    private MutableLiveData<Loginpojo>login;
    public LiveData<Loginpojo>getlogin(final Activity activity, String email, String password, String reg_id, String device_type){
        login=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.userlogin( email,password,reg_id,device_type).enqueue(new Callback<Loginpojo>() {
            @Override
            public void onResponse(Call<Loginpojo> call, Response<Loginpojo> response) {
                Commonutil.dismiss();
                if(response.isSuccessful()){

                    login.setValue(response.body());
                  //  Toast.makeText(activity, ""+login.getValue().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Loginpojo> call, Throwable t) {
//                Toast.makeText(activity, ""+login.getValue().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return login;
    }

    private MutableLiveData<Map>providepackages;
    public LiveData<Map>getpackagesdetail(final Activity activity,String packageId,String userId ){
        providepackages=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.providerpackage(packageId,userId).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                Commonutil.dismiss();
                if(response.isSuccessful()){
                    providepackages.setValue(response.body());
                    //Toast.makeText(activity, "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {

            }
        });
        return providepackages;
    }

    private MutableLiveData<GetUpcomingJobs>upcomingjobsdata;
    public LiveData<GetUpcomingJobs>getupcomingjondetails(final Activity activity,String providerId){
        upcomingjobsdata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.getupcomingjobs(providerId).enqueue(new Callback<GetUpcomingJobs>() {
            @Override
            public void onResponse(Call<GetUpcomingJobs> call, Response<GetUpcomingJobs> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    upcomingjobsdata.setValue(response.body());
                }
                else{
          //          Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetUpcomingJobs> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return upcomingjobsdata;
    }
    private MutableLiveData<GetPendingjobsPojo>pendingjobsdata;
    public LiveData<GetPendingjobsPojo>getpendingjobs(final Activity activity,String providerId){
        pendingjobsdata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.getpendingjobs(providerId).enqueue(new Callback<GetPendingjobsPojo>() {
            @Override
            public void onResponse(Call<GetPendingjobsPojo> call, Response<GetPendingjobsPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    pendingjobsdata.setValue(response.body());
                }
                else{
                //    Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPendingjobsPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return pendingjobsdata;
    }

    private MutableLiveData<ForgetPasswordPojo>updatepasswdata;
    public LiveData<ForgetPasswordPojo>getupdatepassswd(final Activity activity, String phonenumber,String password){
        updatepasswdata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api=ApiClient.getApiClient().create(Api.class);
        api.updatedepasswd(phonenumber,password).enqueue(new Callback<ForgetPasswordPojo>() {
            @Override
            public void onResponse(Call<ForgetPasswordPojo> call, Response<ForgetPasswordPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    updatepasswdata.setValue(response.body());
                }
                else{
                    Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgetPasswordPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return updatepasswdata;
    }

}
