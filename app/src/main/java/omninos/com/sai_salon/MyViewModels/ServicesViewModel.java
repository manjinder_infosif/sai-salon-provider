package omninos.com.sai_salon.MyViewModels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import java.util.Map;

import omninos.com.sai_salon.PojoClasses.GetCategoryModel;
import omninos.com.sai_salon.PojoClasses.GetHomeServiceListModel;
import omninos.com.sai_salon.PojoClasses.Servicestypesspojo;
import omninos.com.sai_salon.PojoClasses.ShowRatingPojo;
import omninos.com.sai_salon.PojoClasses.UpdateAddserviesPojo;
import omninos.com.sai_salon.PojoClasses.UpdateServiceslistPojo;
import omninos.com.sai_salon.Retrofit.Api;
import omninos.com.sai_salon.Retrofit.ApiClient;
import omninos.com.sai_salon.Util.Commonutil;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesViewModel extends ViewModel {

    private MutableLiveData<GetHomeServiceListModel> getHomeServiceListModelMutableLiveData;

    public LiveData<GetHomeServiceListModel> getServices(Activity activity, String providerId) {
        if (Commonutil.InternetCheck(activity)) {

            getHomeServiceListModelMutableLiveData = new MutableLiveData<>();
            Commonutil.showProgress(activity);

            Api api = ApiClient.getApiClient().create(Api.class);

            api.getServiceList(providerId).enqueue(new Callback<GetHomeServiceListModel>() {
                @Override
                public void onResponse(Call<GetHomeServiceListModel> call, Response<GetHomeServiceListModel> response) {
                    Commonutil.dismiss();
                    if (response.body() != null) {
                        getHomeServiceListModelMutableLiveData.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<GetHomeServiceListModel> call, Throwable t) {
                    Commonutil.dismiss();

                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return getHomeServiceListModelMutableLiveData;
    }

    private MutableLiveData<GetCategoryModel> getCategoryModelMutableLiveData;

    public LiveData<GetCategoryModel> getCategoryModelLiveData(Activity activity, String servicetype) {
        if (Commonutil.InternetCheck(activity)) {

            getCategoryModelMutableLiveData = new MutableLiveData<>();

            Api api = ApiClient.getApiClient().create(Api.class);

            Commonutil.showProgress(activity);

            api.getCategory(servicetype).enqueue(new Callback<GetCategoryModel>() {
                @Override
                public void onResponse(Call<GetCategoryModel> call, Response<GetCategoryModel> response) {
                    Commonutil.dismiss();
                    if (response.body() != null) {
                        getCategoryModelMutableLiveData.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<GetCategoryModel> call, Throwable t) {
                    Commonutil.dismiss();

                }
            });
        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return getCategoryModelMutableLiveData;
    }

    private MutableLiveData<Map> addservice;

    public LiveData<Map> addnewService(Activity activity, String servicetype,String providerid,String serviceloc,String categoryId,String title,String arabictitle,String salesprice,String serviceTime) {
        if (Commonutil.InternetCheck(activity)) {

            addservice = new MutableLiveData<>();

            Commonutil.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);

            api.addNewService(servicetype,providerid,serviceloc,categoryId,title,arabictitle,salesprice,serviceTime).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    Commonutil.dismiss();
                    if (response.body() != null) {
                        addservice.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    Commonutil.dismiss();

                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return addservice;
    }


    private MutableLiveData<Servicestypesspojo>getservicesmutable;
    public LiveData<Servicestypesspojo>getservicestypes(Activity activity,String providerid){
        if(Commonutil.InternetCheck(activity)){
            getservicesmutable=new MutableLiveData<>();
            Api api=ApiClient.getApiClient().create(Api.class);
            Commonutil.showProgress(activity);
            api.getservicetype(providerid).enqueue(new Callback<Servicestypesspojo>() {
                @Override
                public void onResponse(Call<Servicestypesspojo> call, Response<Servicestypesspojo> response) {
                    Commonutil.dismiss();
                    if(response.body()!=null){
                        getservicesmutable.setValue(response.body());
                    }

                }

                @Override
                public void onFailure(Call<Servicestypesspojo> call, Throwable t) {
                Commonutil.dismiss();
                }
            });
        }
        else{
            Toast.makeText(activity, "Network issue", Toast.LENGTH_SHORT).show();
        }
        return getservicesmutable;
    }

    private MutableLiveData<UpdateAddserviesPojo>data;
    public LiveData<UpdateAddserviesPojo>updateservice(final Activity activity,String serviceid,String providerid){
        data=new MutableLiveData<>();
        Api api=ApiClient.getApiClient().create(Api.class);

        api.updateservices(serviceid,providerid).enqueue(new Callback<UpdateAddserviesPojo>() {
            @Override
            public void onResponse(Call<UpdateAddserviesPojo> call, Response<UpdateAddserviesPojo> response) {
                if(response.body()!=null){
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UpdateAddserviesPojo> call, Throwable t) {

            }
        });
        return data;
    }

    private MutableLiveData<UpdateServiceslistPojo> updatedservice;

    public LiveData<UpdateServiceslistPojo> getupdatedservice(Activity activity, String servicetype,String providerid,String serviceloc,String categoryId,String title,String arabictitle,String salesprice,String serviceTime,String serviceid) {
        if (Commonutil.InternetCheck(activity)) {

            updatedservice = new MutableLiveData<>();

            Commonutil.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);

            api.getupdated(servicetype,providerid,serviceloc,categoryId,title,arabictitle,salesprice,serviceTime,serviceid).enqueue(new Callback<UpdateServiceslistPojo>() {
                @Override
                public void onResponse(Call<UpdateServiceslistPojo> call, Response<UpdateServiceslistPojo> response) {
                    Commonutil.dismiss();
                    if (response.body() != null) {
                        updatedservice.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<UpdateServiceslistPojo> call, Throwable t) {
                    Commonutil.dismiss();

                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return updatedservice;
    }


    private MutableLiveData<ShowRatingPojo>datarating;
    public LiveData<ShowRatingPojo>getratingofsalon(final Activity activity,String providerid){
        datarating=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.getlist(providerid).enqueue(new Callback<ShowRatingPojo>() {
            @Override
            public void onResponse(Call<ShowRatingPojo> call, Response<ShowRatingPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    datarating.setValue(response.body());
                   // Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowRatingPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return datarating;
    }

}
