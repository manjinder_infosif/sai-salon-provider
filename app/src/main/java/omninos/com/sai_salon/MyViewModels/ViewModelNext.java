package omninos.com.sai_salon.MyViewModels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Activities.EditProfile;
import omninos.com.sai_salon.PojoClasses.AddEMployeepojo;
import omninos.com.sai_salon.PojoClasses.BookinglistPojo;
import omninos.com.sai_salon.PojoClasses.BusinessPojo;
import omninos.com.sai_salon.PojoClasses.DeletePojo;
import omninos.com.sai_salon.PojoClasses.EditProfilePojo;
import omninos.com.sai_salon.PojoClasses.EditTimeslotspojo;
import omninos.com.sai_salon.PojoClasses.GetCityPojo;
import omninos.com.sai_salon.PojoClasses.GetContactPojo;
import omninos.com.sai_salon.PojoClasses.GetCountryListPojo;
import omninos.com.sai_salon.PojoClasses.GetCoupondetails;
import omninos.com.sai_salon.PojoClasses.GetServiceIDPojo;
import omninos.com.sai_salon.PojoClasses.GetServicesnamelist;
import omninos.com.sai_salon.PojoClasses.GetStatePojo;
import omninos.com.sai_salon.PojoClasses.ImagesPojo;
import omninos.com.sai_salon.PojoClasses.NewTimeSlotpojo;
import omninos.com.sai_salon.PojoClasses.OnlineOfflinestatus;
import omninos.com.sai_salon.PojoClasses.SalonSpecialOfferspojo;
import omninos.com.sai_salon.PojoClasses.SalonimagesPojo;
import omninos.com.sai_salon.PojoClasses.TimeslotPojo;
import omninos.com.sai_salon.PojoClasses.UpdatePhotoPojooo;
import omninos.com.sai_salon.Retrofit.Api;
import omninos.com.sai_salon.Retrofit.ApiClient;
import omninos.com.sai_salon.Util.Commonutil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Multipart;

public class ViewModelNext extends ViewModel {

    private MutableLiveData<BookinglistPojo>bookingdata;
    public LiveData<BookinglistPojo>getbookings(final Activity activity,String providerId){
        bookingdata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.booking(providerId).enqueue(new Callback<BookinglistPojo>() {
            @Override
            public void onResponse(Call<BookinglistPojo> call, Response<BookinglistPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    bookingdata.setValue(response.body());
                   //Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BookinglistPojo> call, Throwable t) {
            Commonutil.dismiss();
              //  Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return bookingdata;
    }

    private MutableLiveData<GetContactPojo>getdata;
    public LiveData<GetContactPojo>getcontacts(final Activity activity){
        getdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.getcontactdetails().enqueue(new Callback<GetContactPojo>() {
            @Override
            public void onResponse(Call<GetContactPojo> call, Response<GetContactPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    getdata.setValue(response.body());
                }
                else{

                }
            }

            @Override
            public void onFailure(Call<GetContactPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return getdata;
    }

    private MutableLiveData<TimeslotPojo>timedata;
    public LiveData<TimeslotPojo>gettime(final Activity activity,String providerid){
        timedata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.gettimeslot(providerid).enqueue(new Callback<TimeslotPojo>() {
            @Override
            public void onResponse(Call<TimeslotPojo> call, Response<TimeslotPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    timedata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<TimeslotPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return timedata;
    }

    private MutableLiveData<ImagesPojo>imagesdata;
    public LiveData<ImagesPojo>sendimagess(final Activity activity, RequestBody providerId, MultipartBody.Part images[]){
        imagesdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.sendimages(providerId,images).enqueue(new Callback<ImagesPojo>() {
            @Override
            public void onResponse(Call<ImagesPojo> call, Response<ImagesPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                   imagesdata.setValue(response.body());
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ImagesPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return imagesdata;
    }

    private MutableLiveData<BusinessPojo>businessdata;
    public LiveData<BusinessPojo>businessprofile(final Activity activity,String providerid){
        businessdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.getprofile(providerid).enqueue(new Callback<BusinessPojo>() {
            @Override
            public void onResponse(Call<BusinessPojo> call, Response<BusinessPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    businessdata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<BusinessPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return businessdata;
    }

    private MutableLiveData<EditProfilePojo>editdata;
    public LiveData<EditProfilePojo>editprofile(final Activity activity,String providerid,String name,String email,String phone){
        editdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.editprofile(providerid,name,email,phone).enqueue(new Callback<EditProfilePojo>() {
            @Override
            public void onResponse(Call<EditProfilePojo> call, Response<EditProfilePojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    editdata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<EditProfilePojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return editdata;
    }


        private MutableLiveData<EditTimeslotspojo>timeslotdata;
     public LiveData<EditTimeslotspojo>edittimeget(final Activity activity, JSONArray timeslots, String providerId){
        timeslotdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.edittime(timeslots,providerId).enqueue(new Callback<EditTimeslotspojo>() {
            @Override
            public void onResponse(Call<EditTimeslotspojo> call, Response<EditTimeslotspojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    timeslotdata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<EditTimeslotspojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return timeslotdata;
    }

    private MutableLiveData<AddEMployeepojo>addemp;
    public LiveData<AddEMployeepojo>addemployee(final Activity activity,MultipartBody.Part image,RequestBody name,RequestBody email,RequestBody phone,RequestBody providerid){
        addemp=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.sendemplyess(image,name,email,phone,providerid).enqueue(new Callback<AddEMployeepojo>() {
            @Override
            public void onResponse(Call<AddEMployeepojo> call, Response<AddEMployeepojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    addemp.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<AddEMployeepojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return addemp;
    }

    private MutableLiveData<SalonimagesPojo>imagedata;
    public LiveData<SalonimagesPojo>getimages(final Activity activity,String providerid){
        imagedata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.getsalonimages(providerid).enqueue(new Callback<SalonimagesPojo>() {
            @Override
            public void onResponse(Call<SalonimagesPojo> call, Response<SalonimagesPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    imagedata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SalonimagesPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return imagedata;
    }

    private MutableLiveData<OnlineOfflinestatus>onoffdata;
    public LiveData<OnlineOfflinestatus>getstatus(final Activity activity,String providerid,String status){
        onoffdata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.getstatus(providerid,status).enqueue(new Callback<OnlineOfflinestatus>() {
            @Override
            public void onResponse(Call<OnlineOfflinestatus> call, Response<OnlineOfflinestatus> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    onoffdata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<OnlineOfflinestatus> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return onoffdata;
    }

    private MutableLiveData<GetServicesnamelist>servicelist;
    public LiveData<GetServicesnamelist>getserviceslist(final Activity activity,String providerid,String servicetype,String servicelocation){
        servicelist=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.getserviceslist(providerid,servicetype,servicelocation).enqueue(new Callback<GetServicesnamelist>() {
            @Override
            public void onResponse(Call<GetServicesnamelist> call, Response<GetServicesnamelist> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    servicelist.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetServicesnamelist> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return servicelist;
    }

    private MutableLiveData<SalonSpecialOfferspojo>specialoffer;
    public LiveData<SalonSpecialOfferspojo>getspecialoffers(final Activity activity,RequestBody providerid,RequestBody servicesId,RequestBody servicetype,RequestBody servicelocation,RequestBody title, RequestBody offer,MultipartBody.Part image,RequestBody startdate,RequestBody enddate,RequestBody arabictitle,RequestBody totalprice,RequestBody price){
        specialoffer=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
       api.specialoffers(providerid,servicesId,servicetype,servicelocation,title,offer,image,startdate,enddate,arabictitle,totalprice,price).enqueue(new Callback<SalonSpecialOfferspojo>() {
            @Override
            public void onResponse(Call<SalonSpecialOfferspojo> call, Response<SalonSpecialOfferspojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    specialoffer.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SalonSpecialOfferspojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return specialoffer;
    }

    private MutableLiveData<UpdatePhotoPojooo>updatephoto;
    public LiveData<UpdatePhotoPojooo>updatephotos(final Activity activity,RequestBody id,MultipartBody.Part image){
        updatephoto=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.updatephotos(id,image).enqueue(new Callback<UpdatePhotoPojooo>() {
            @Override
            public void onResponse(Call<UpdatePhotoPojooo> call, Response<UpdatePhotoPojooo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    updatephoto.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UpdatePhotoPojooo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return updatephoto;
    }

    private MutableLiveData<DeletePojo>deletphoto;
    public LiveData<DeletePojo>deletephots(final Activity activity,String id){
        deletphoto=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.deletephotos(id).enqueue(new Callback<DeletePojo>() {
            @Override
            public void onResponse(Call<DeletePojo> call, Response<DeletePojo> response) {

                if(response.body()!=null){
                    deletphoto.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<DeletePojo> call, Throwable t) {

                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return deletphoto;
    }

    private MutableLiveData<GetCoupondetails>coupondata;
    public LiveData<GetCoupondetails>getcoupon(final Activity activity,String ptoviderid,String couponname,String packageId){
        coupondata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.getcoupon(ptoviderid,couponname,packageId).enqueue(new Callback<GetCoupondetails>() {
            @Override
            public void onResponse(Call<GetCoupondetails> call, Response<GetCoupondetails> response) {

                if(response.body()!=null){
                    coupondata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetCoupondetails> call, Throwable t) {

                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return coupondata;
    }

    private MutableLiveData<GetCountryListPojo>countries;
    public LiveData<GetCountryListPojo>getcountry(final Activity activity){
        countries=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.getcountries().enqueue(new Callback<GetCountryListPojo>() {
            @Override
            public void onResponse(Call<GetCountryListPojo> call, Response<GetCountryListPojo> response) {

                if(response.body()!=null){
                    countries.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetCountryListPojo> call, Throwable t) {

                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return countries;
    }


    private MutableLiveData<GetStatePojo>states;
    public LiveData<GetStatePojo>getstates(final Activity activity,String countryid){
        states=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.getstate(countryid).enqueue(new Callback<GetStatePojo>() {
            @Override
            public void onResponse(Call<GetStatePojo> call, Response<GetStatePojo> response) {

                if(response.body()!=null){
                    states.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetStatePojo> call, Throwable t) {

                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return states;
    }

    private MutableLiveData<GetCityPojo>city;
    public LiveData<GetCityPojo>getcity(final Activity activity,String stateid){
        city=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.getcities(stateid).enqueue(new Callback<GetCityPojo>() {
            @Override
            public void onResponse(Call<GetCityPojo> call, Response<GetCityPojo> response) {

                if(response.body()!=null){
                    city.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<GetCityPojo> call, Throwable t) {

                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return city;
    }

    private MutableLiveData<TimeslotPojo>newtimedata;
    public LiveData<TimeslotPojo>getnewtime(final Activity activity){
        newtimedata=new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        Commonutil.showProgress(activity);
        api.newtimeslot().enqueue(new Callback<TimeslotPojo>() {
            @Override
            public void onResponse(Call<TimeslotPojo> call, Response<TimeslotPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    newtimedata.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<TimeslotPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return newtimedata;
    }


    private MutableLiveData<GetServiceIDPojo> servieidd;
    public LiveData<GetServiceIDPojo> getservieid(final Activity activity, String serviceid){
        servieidd=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.serviceidget(serviceid).enqueue(new Callback<GetServiceIDPojo>() {
            @Override
            public void onResponse(Call<GetServiceIDPojo> call, Response<GetServiceIDPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    servieidd.setValue(response.body());
                    // Toast.makeText(activity, ""+response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetServiceIDPojo> call, Throwable t) {
                Commonutil.dismiss();
                // Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return servieidd;
    }
}
