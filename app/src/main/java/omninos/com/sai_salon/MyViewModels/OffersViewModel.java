package omninos.com.sai_salon.MyViewModels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.PojoClasses.AcceptRejectPojo;
import omninos.com.sai_salon.PojoClasses.ConversationMessagepojo;
import omninos.com.sai_salon.PojoClasses.GetAdminIdPojo;
import omninos.com.sai_salon.PojoClasses.NotificationPojo;
import omninos.com.sai_salon.PojoClasses.ProviderSpecialOffersPojo;
import omninos.com.sai_salon.PojoClasses.SendMessagePojo;
import omninos.com.sai_salon.Retrofit.Api;
import omninos.com.sai_salon.Retrofit.ApiClient;
import omninos.com.sai_salon.Util.Commonutil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersViewModel extends ViewModel {

    private MutableLiveData<ProviderSpecialOffersPojo>specialoffers;
    public LiveData<ProviderSpecialOffersPojo>getoffers(final Activity activity,String providerId){
        specialoffers=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);

        api.getspecialoffers(providerId).enqueue(new Callback<ProviderSpecialOffersPojo>() {
            @Override
            public void onResponse(Call<ProviderSpecialOffersPojo> call, Response<ProviderSpecialOffersPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    specialoffers.setValue(response.body());
                    //Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProviderSpecialOffersPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return specialoffers;
    }

    private MutableLiveData<NotificationPojo>notifydata;
    public LiveData<NotificationPojo>getnotify(final Activity activity, String providerId){
        notifydata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);

        api.notify(providerId).enqueue(new Callback<NotificationPojo>() {
            @Override
            public void onResponse(Call<NotificationPojo> call, Response<NotificationPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    notifydata.setValue(response.body());
                  //  Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return notifydata;
    }

    private MutableLiveData<AcceptRejectPojo>acceptdata;
    public LiveData<AcceptRejectPojo>getacpetrejectstatus(final Activity activity,String status,String bookingid){
        acceptdata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api= ApiClient.getApiClient().create(Api.class);
        api.acceptrejectstatus(status,bookingid).enqueue(new Callback<AcceptRejectPojo>() {
            @Override
            public void onResponse(Call<AcceptRejectPojo> call, Response<AcceptRejectPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    acceptdata.setValue(response.body());
                    Toast.makeText(activity, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AcceptRejectPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return acceptdata;
    }

    private MutableLiveData<GetAdminIdPojo>admindata;
    public LiveData<GetAdminIdPojo>getadminid(final Activity activity){
        admindata=new MutableLiveData<>();
        Commonutil.showProgress(activity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.adminid().enqueue(new Callback<GetAdminIdPojo>() {
            @Override
            public void onResponse(Call<GetAdminIdPojo> call, Response<GetAdminIdPojo> response) {
                Commonutil.dismiss();
                if(response.body()!=null){
                    admindata.setValue(response.body());
                    // Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetAdminIdPojo> call, Throwable t) {
                Commonutil.dismiss();
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return admindata;
    }

    private MutableLiveData<SendMessagePojo>sendmsgdta;
    public LiveData<SendMessagePojo>sendingmessage(final Activity activity, RequestBody senderid, RequestBody receiverid, RequestBody type, RequestBody chattype, RequestBody message, MultipartBody.Part image,RequestBody messagetype){
        sendmsgdta=new MutableLiveData<>();
        //  CommonUtils.showProgress(activity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.sendMessage(senderid,receiverid,type,chattype,message,image,messagetype).enqueue(new Callback<SendMessagePojo>() {
            @Override
            public void onResponse(Call<SendMessagePojo> call, Response<SendMessagePojo> response) {
                //  CommonUtils.dismiss();
                if(response.body()!=null){
                    sendmsgdta.setValue(response.body());
                    //Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SendMessagePojo> call, Throwable t) {
                //     CommonUtils.dismiss();
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return sendmsgdta;
    }

    private MutableLiveData<ConversationMessagepojo>conversationdata;
    public LiveData<ConversationMessagepojo>converstaionmsg(final Activity activity,String senderid,String receiverid,String chattype){
        conversationdata=new MutableLiveData<>();

       Api api = ApiClient.getApiClient().create(Api.class);
        api.conversation(senderid,receiverid,chattype).enqueue(new Callback<ConversationMessagepojo>() {
            @Override
            public void onResponse(Call<ConversationMessagepojo> call, Response<ConversationMessagepojo> response) {

                if(response.body()!=null){
                    conversationdata.setValue(response.body());
                    // Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ConversationMessagepojo> call, Throwable t) {

               // Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return conversationdata;
    }
}
