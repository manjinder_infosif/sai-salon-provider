package omninos.com.sai_salon.Fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Activities.AcceptRejectActivity;
import omninos.com.sai_salon.Adapters.UpcomingJobAdapter;
import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.PojoClasses.AcceptRejectPojo;
import omninos.com.sai_salon.PojoClasses.GetUpcomingJobs;
import omninos.com.sai_salon.PojoClasses.NotificationPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingjobFragment extends Fragment {

    View v;
    RecyclerView jobsrv;
    ViewModell viewModel;
    OffersViewModel offerviewmodel;
    TextView text;
    List<GetUpcomingJobs.Detail> details = new ArrayList<>();
    Context context;
    Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_upcomingjob, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModell.class);
        offerviewmodel = ViewModelProviders.of(getActivity()).get(OffersViewModel.class);
        jobsrv = v.findViewById(R.id.jobsrv);
        text = v.findViewById(R.id.text);
        context = LocaleHelper.setLocale(getActivity(), App.getAppPreferences().getLanguage(getActivity()));
        resources = context.getResources();
        ChangeLanguage();
        getdata();


        jobsrv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        return v;
    }

    private void ChangeLanguage() {
        text.setText(resources.getString(R.string.no_upcoming_jobs));
    }

    private void getdata() {

        viewModel.getupcomingjondetails(getActivity(), App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(getActivity(), new Observer<GetUpcomingJobs>() {
            @Override
            public void onChanged(@Nullable final GetUpcomingJobs getUpcomingJobs) {
                if (getUpcomingJobs.getSuccess().equalsIgnoreCase("1")) {
                    if (getUpcomingJobs.getDetails() != null) {
                        text.setVisibility(View.GONE);
                        jobsrv.setVisibility(View.VISIBLE);
                        UpcomingJobAdapter adapter2 = new UpcomingJobAdapter(getActivity(), getUpcomingJobs.getDetails(), new UpcomingJobAdapter.OnAcceptClick() {
                            @Override
                            public void onaccpet(int position, String status) {
                                offerviewmodel.getacpetrejectstatus(getActivity(), status, getUpcomingJobs.getDetails().get(position).getId()).observe(getActivity(), new Observer<AcceptRejectPojo>() {
                                    @Override
                                    public void onChanged(@Nullable AcceptRejectPojo acceptRejectPojo) {
                                        if (acceptRejectPojo.getSuccess().equalsIgnoreCase("1")) {
                                            Toast.makeText(getContext(), "" + acceptRejectPojo.getMessage(), Toast.LENGTH_SHORT).show();
                                            getdata();
                                        }
                                    }
                                });
                            }
                        });
                        jobsrv.setAdapter(adapter2);
                    } else {
                        text.setVisibility(View.VISIBLE);
                        jobsrv.setVisibility(View.GONE);
                    }
                } else {
                    text.setVisibility(View.VISIBLE);
                    jobsrv.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "" + getUpcomingJobs.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
