package omninos.com.sai_salon.Fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Activities.AcceptRejectActivity;
import omninos.com.sai_salon.Activities.TermsncondiActivity;
import omninos.com.sai_salon.Adapters.PendingJobsAdapter;
import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.PojoClasses.AcceptRejectPojo;
import omninos.com.sai_salon.PojoClasses.GetPendingjobsPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingjobFragment extends Fragment {


   RecyclerView jobsrv;
   ViewModell viewModell;
   OffersViewModel offersViewModel;
   TextView text;
   View v;
   List<GetPendingjobsPojo.Detail> detailList=new ArrayList<>();

    Context context;
    Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      v=inflater.inflate(R.layout.fragment_home_jobsfragment, container, false);
      viewModell= ViewModelProviders.of(getActivity()).get(ViewModell.class);
      offersViewModel=ViewModelProviders.of(getActivity()).get(OffersViewModel.class);
      jobsrv=v.findViewById(R.id.jobsrv);
      text=v.findViewById(R.id.text);
        context = LocaleHelper.setLocale(getActivity(), App.getAppPreferences().getLanguage(getActivity()));
        resources = context.getResources();
        ChangeLanguage();
      getdata();

        jobsrv.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

      return v;
    }

    private void ChangeLanguage() {
        text.setText(resources.getString(R.string.no_pending_jobs));
    }

    private void getdata() {
        viewModell.getpendingjobs(getActivity(), App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(getActivity(), new Observer<GetPendingjobsPojo>() {
            @Override
            public void onChanged(@Nullable final GetPendingjobsPojo getPendingjobsPojo) {
                if(getPendingjobsPojo.getSuccess().equalsIgnoreCase("1")){

                    text.setVisibility(View.GONE);
                    jobsrv.setVisibility(View.VISIBLE);
                    if (getPendingjobsPojo.getDetails()!=null){
                        PendingJobsAdapter adapter2=new PendingJobsAdapter(getActivity(), getPendingjobsPojo.getDetails(), new PendingJobsAdapter.OnAcceptClick() {
                            @Override
                            public void acceptclik(int position, String status) {
                                offersViewModel.getacpetrejectstatus(getActivity(),status,getPendingjobsPojo.getDetails().get(position).getId()).observe(getActivity(), new Observer<AcceptRejectPojo>() {
                                    @Override
                                    public void onChanged(@Nullable AcceptRejectPojo acceptRejectPojo) {
                                        if(acceptRejectPojo.getSuccess().equalsIgnoreCase("1")){
                                           // Toast.makeText(getContext(), ""+acceptRejectPojo.getMessage(), Toast.LENGTH_SHORT).show();
                                            getdata();


                                        }
                                    }
                                });
                            }
                        });
                        jobsrv.setAdapter(adapter2);
                    }
                }
                else {
                    text.setVisibility(View.VISIBLE);
                    jobsrv.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), ""+getPendingjobsPojo.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }


}
