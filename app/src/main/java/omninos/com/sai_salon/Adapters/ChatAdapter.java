package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Modelclass.ChatModel;
import omninos.com.sai_salon.PojoClasses.ConversationMessagepojo;
import omninos.com.sai_salon.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context context;

    List<ConversationMessagepojo.MessageDetail> models;

    public ChatAdapter(Context context, List<ConversationMessagepojo.MessageDetail> models) {
        this.context = context;
        this.models = models;
    }
    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.chat_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatAdapter.ViewHolder viewHolder, int i) {
        if (models.get(i).getMessageType().equalsIgnoreCase("1")) {
            if (models.get(i).getType().equalsIgnoreCase("admin")) {
                viewHolder.right.setVisibility(View.GONE);
                viewHolder.left.setVisibility(View.VISIBLE);
                viewHolder.rightimglay.setVisibility(View.GONE);
                viewHolder.leftimglay.setVisibility(View.GONE);
                viewHolder.leftChat.setText(models.get(i).getMessage());
            } else if (models.get(i).getType().equalsIgnoreCase("provider")) {
                viewHolder.right.setVisibility(View.VISIBLE);
                viewHolder.left.setVisibility(View.GONE);
                viewHolder.rightimglay.setVisibility(View.GONE);
                viewHolder.leftimglay.setVisibility(View.GONE);
                viewHolder.rightChat.setText(models.get(i).getMessage());
            }
        } else if (models.get(i).getMessageType().equalsIgnoreCase("2")) {
            if (models.get(i).getType().equalsIgnoreCase("admin")) {
                viewHolder.right.setVisibility(View.GONE);
                viewHolder.left.setVisibility(View.GONE);
                viewHolder.leftimglay.setVisibility(View.VISIBLE);
                viewHolder.rightimglay.setVisibility(View.GONE);
                Glide.with(context).load(models.get(i).getMessage()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        viewHolder.leftProgress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.leftProgress.setVisibility(View.GONE);
                        return false;
                    }
                }).into(viewHolder.leftimg);
                //  viewHolder.leftChat.setText(models.get(i).getMessage());
            } else if (models.get(i).getType().equalsIgnoreCase("provider")) {
                viewHolder.right.setVisibility(View.GONE);
                viewHolder.left.setVisibility(View.GONE);
                viewHolder.leftimglay.setVisibility(View.GONE);
                viewHolder.rightimglay.setVisibility(View.VISIBLE);
                Glide.with(context).load(models.get(i).getMessage()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        viewHolder.rightProgress.setVisibility(View.GONE);
                        return false;

                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.rightProgress.setVisibility(View.GONE);
                        return false;
                    }
                }).into(viewHolder.rightimg);
                //viewHolder.rightChat.setText(models.get(i).getMessage());
            }

        }

    }

    @Override
    public int getItemCount() {

        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout right, left;
        ImageView leftimg, rightimg;
        TextView rightChat, leftChat;
        ProgressBar leftProgress, rightProgress;
        RelativeLayout rightrelative, leftrelative, leftimglay, rightimglay;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            right = itemView.findViewById(R.id.right);
            left = itemView.findViewById(R.id.left);
            rightChat = itemView.findViewById(R.id.rightChat);
            leftChat = itemView.findViewById(R.id.leftChat);
            leftimg = itemView.findViewById(R.id.leftimg);
            rightimg = itemView.findViewById(R.id.rightimg);
            leftimglay = itemView.findViewById(R.id.leftimglay);
            rightimglay = itemView.findViewById(R.id.rightimglay);
            rightrelative = itemView.findViewById(R.id.rightRelative);
            leftrelative = itemView.findViewById(R.id.leftrelative);
            leftProgress = itemView.findViewById(R.id.leftProgress);
            rightProgress = itemView.findViewById(R.id.rightProgress);
        }
    }
}
