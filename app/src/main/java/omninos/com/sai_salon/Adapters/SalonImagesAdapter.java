package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.Activities.SalonimagesActivity;
import omninos.com.sai_salon.PojoClasses.SalonimagesPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;

public class SalonImagesAdapter extends RecyclerView.Adapter<SalonImagesAdapter.ViewHolder> {
    Context context;
    List<SalonimagesPojo.Detail>list;
    OnClick onClick;
    Removephoto removephoto;

    public interface OnClick{
        void onImageclick(int position);
    }
    public interface Removephoto{
        void remove(int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.photolayout,viewGroup,false);
        return new ViewHolder(view);
    }

    public SalonImagesAdapter(Context context, List<SalonimagesPojo.Detail> list,OnClick onClick,Removephoto removephoto) {
        this.context = context;
        this.list = list;
        this.onClick=onClick;
        this.removephoto=removephoto;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        SalonimagesPojo.Detail detail=list.get(i);
        Glide.with(context).load(detail.getImage()).into(viewHolder.backimg);
        App.getSingletonPojo().setPhotoid(detail.getId());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView backimg,cancel;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            backimg=itemView.findViewById(R.id.imgback);
            cancel=itemView.findViewById(R.id.cancel);
            backimg.setOnClickListener(this);
            cancel.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imgback:
                    onClick.onImageclick(getLayoutPosition());
                    break;
                case R.id.cancel:
                    removephoto.remove(getLayoutPosition());
                    break;
            }
        }
    }
}
