package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.PojoClasses.ServicesListpojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private Context context;
    private List<ServicesListpojo> servicelist;
    private List<String> serviceimageslist;
    private List<String> servicenamelist;
    private List<String> serviceIds = new ArrayList<>();


    public ServicesAdapter(Context context, List<ServicesListpojo> serviceslist) {
        this.context = context;
        this.servicelist = serviceslist;
        serviceimageslist = new ArrayList<>();
        servicenamelist = new ArrayList<>();
    }


    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.servicieslayout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ServicesAdapter.ViewHolder viewHolder, final int i) {
        final ServicesListpojo servicesListpojo = servicelist.get(i);
        Glide.with(context).load(servicesListpojo.getDetails().get(i).getImage()).into(viewHolder.serviceimage);
        viewHolder.salontitle.setText(servicesListpojo.getDetails().get(i).getTitle());
        viewHolder.serviceimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(viewHolder.checksevices.getVisibility()== View.GONE) {

                if (!serviceIds.contains(servicesListpojo.getDetails().get(i).getId())) {
                    serviceIds.add(servicesListpojo.getDetails().get(i).getId());
                    viewHolder.checksevices.setVisibility(View.VISIBLE);

                } else {
                    viewHolder.checksevices.setVisibility(View.GONE);

                    serviceIds.remove(servicesListpojo.getDetails().get(i).getId());
                }
//                    String service_id=servicesListpojo.getDetails().get(i).getId();
//                    serviceimageslist.add(servicesListpojo.getDetails().get(i).getImage());
//                    servicenamelist.add(servicesListpojo.getDetails().get(i).getTitle());
//                    App.getSingletonPojo().setServicesImages(serviceimageslist);
//                    App.getSingletonPojo().setServicenames(servicenamelist);
//                    Toast.makeText(context, ""+service_id, Toast.LENGTH_SHORT).show();
//                    App.getSingletonPojo().setServicesid(service_id);
//                }
//                else{
//                    viewHolder.checksevices.setVisibility(View.GONE);
//                    serviceimageslist.remove(servicesListpojo.getDetails().get(i).getImage());
//                }
//                Toast.makeText(context, ""+servicesListpojo.getDetails().get(i).getImage(), Toast.LENGTH_SHORT).show();


                App.getSingletonPojo().setServicesid(serviceIds);
            }
        });

    }

    @Override
    public int getItemCount() {
        return servicelist.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView serviceimage, checksevices;
        TextView salontitle;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            serviceimage = itemView.findViewById(R.id.serviceimage);
            salontitle = itemView.findViewById(R.id.salontitle);
            checksevices = itemView.findViewById(R.id.checkservice);
        }
    }
}
