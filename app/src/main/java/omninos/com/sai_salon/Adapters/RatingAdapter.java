package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import omninos.com.sai_salon.PojoClasses.ShowRatingPojo;
import omninos.com.sai_salon.R;

public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.ViewHolder> {



    Context context;
    List<ShowRatingPojo.Detail>list;

    public RatingAdapter(Context context, List<ShowRatingPojo.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.ratinglist,viewGroup,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingAdapter.ViewHolder viewHolder, int i) {
        ShowRatingPojo.Detail detail=list.get(i);
        viewHolder.ratingBar.setRating(Float.parseFloat(detail.getUserRating()));
        viewHolder.name.setText(detail.getUserName());
        viewHolder.comment.setText(detail.getComment());

        if(Integer.parseInt(detail.getUserRating())>=3){
            viewHolder.thumbsup.setVisibility(View.VISIBLE);
            viewHolder.thumbsdown.setVisibility(View.GONE);
        }
        else{
            viewHolder.thumbsup.setVisibility(View.GONE);
            viewHolder.thumbsdown.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,comment;
        RatingBar ratingBar;
        RelativeLayout thumbsup,thumbsdown;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.Name);
            comment=itemView.findViewById(R.id.comment);
            thumbsup=itemView.findViewById(R.id.thumbs_up);
            thumbsdown=itemView.findViewById(R.id.thumbs_down);
            ratingBar=itemView.findViewById(R.id.ratingBarSalonItem);
        }
    }
}
