package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import omninos.com.sai_salon.R;

public class PackageServiceAdapter extends RecyclerView.Adapter<PackageServiceAdapter.ViewHolder> {
    Context context;
    List<String>servicename;
    OnCancelClick onCancelClick;
    public PackageServiceAdapter(Context context,List<String>servicename,OnCancelClick onCancelClick) {
        this.context = context;
        this.servicename=servicename;
        this.onCancelClick=onCancelClick;
    }

    public interface OnCancelClick{
        void onclick(int position);
    }
    @NonNull
    @Override
    public PackageServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.packageservice_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageServiceAdapter.ViewHolder viewHolder, int i) {
        viewHolder.servicename.setText(servicename.get(i));

    }

    @Override
    public int getItemCount() {
        return servicename.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView servicename;
        ImageView cansel;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            servicename=itemView.findViewById(R.id.servicename);
            cansel=itemView.findViewById(R.id.cansel);
            cansel.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cansel:
                    onCancelClick.onclick(getLayoutPosition());
                    break;
            }
        }
    }
}
