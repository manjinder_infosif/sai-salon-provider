package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.Activities.AcceptRejectActivity;
import omninos.com.sai_salon.Activities.LoginActivity;
import omninos.com.sai_salon.PojoClasses.GetPendingjobsPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class PendingJobsAdapter extends RecyclerView.Adapter<PendingJobsAdapter.ViewHolder> {



    Activity context;
    Context context2;
    Resources resources;
    OnAcceptClick onAcceptClick;
    List<GetPendingjobsPojo.Detail>details;

    public interface OnAcceptClick{
        void acceptclik(int position,String status);
    }


    public PendingJobsAdapter(Activity context, List<GetPendingjobsPojo.Detail> details, OnAcceptClick onAcceptClick) {
        this.context = context;
        this.details = details;
        this.onAcceptClick=onAcceptClick;
    }

    @NonNull
    @Override
    public PendingJobsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.home_layout,viewGroup,false);
        context2 = LocaleHelper.setLocale(context, App.getAppPreferences().getLanguage(context));
        resources = context2.getResources();
        return new ViewHolder(view);
    }

    private void ChooseLang() {

    }

    @Override
    public void onBindViewHolder(@NonNull PendingJobsAdapter.ViewHolder viewHolder, final int i) {
        final GetPendingjobsPojo.Detail list=details.get(i);
        if(App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")){
            Glide.with(context).load(list.getUserImage()).into(viewHolder.userimage);
            viewHolder.username.setText(list.getUserName());
            viewHolder.location.setText(resources.getString(R.string.service_location)+ " : "+list.getServiceLocationArabic());
        }
        else {
            Glide.with(context).load(list.getUserImage()).into(viewHolder.userimage);
            viewHolder.username.setText(list.getUserName());
            viewHolder.location.setText(resources.getString(R.string.service_location) + " : " + list.getServiceLocation());
        }
            String location=list.getServiceLocation();

        if(location.equalsIgnoreCase("salon")){
            viewHolder.Address.setVisibility(View.GONE);
        }
        else {
            viewHolder.Address.setVisibility(View.VISIBLE);
            viewHolder.Address.setText(resources.getString(R.string.useraddress) +" : " +list.getUserBookingAddress());

        }

        if(list.getServices()!=null) {
            if (App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
                viewHolder.services.setText(resources.getString(R.string.services) + " : " + list.getServicesArabicTitle());
            }
            else{
                viewHolder.services.setText(resources.getString(R.string.services) + " : " + list.getServices());
            }
        }
        if(list.getStatus().equalsIgnoreCase("1")){
            viewHolder.accept.setText(resources.getString(R.string.accepted));
            viewHolder.cansel.setVisibility(View.GONE);
            viewHolder.accept.setVisibility(View.VISIBLE);
        }
        else if(list.getStatus().equalsIgnoreCase("2")) {
            viewHolder.cansel.setText(resources.getString(R.string.rejected));
            viewHolder.accept.setVisibility(View.GONE);
            viewHolder.cansel.setVisibility(View.VISIBLE);
        }
        else{
            viewHolder.accept.setText(resources.getString(R.string.accept));
            viewHolder.cansel.setText(resources.getString(R.string.reject));
            viewHolder.cansel.setVisibility(View.VISIBLE);
            viewHolder.accept.setVisibility(View.VISIBLE);
            viewHolder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAcceptClick.acceptclik(i,"1");
                }
            });
            viewHolder.cansel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAcceptClick.acceptclik(i,"2");
                }
            });
        }
        viewHolder.date.setText(resources.getString(R.string.appointment)+ " : "+list.getServiceDate()+" , "+list.getServiceTime());

        viewHolder.jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, AcceptRejectActivity.class);
                intent.putExtra("From", "Sample");
                App.getSingletonPojo().setAcceptrejectstatus(list.getStatus());
                App.getSingletonPojo().setBookingid(list.getId());

                intent.putExtra("username", list.getUserName());
                intent.putExtra("userImage", list.getUserImage());
                intent.putExtra("userAddress", list.getUserAddress());
                intent.putExtra("services", list.getServices());
                intent.putExtra("appointmentDate", list.getServiceDate());
                intent.putExtra("appointmenttime", list.getServiceTime());
                intent.putExtra("bookingId", list.getId());
                intent.putExtra("usereamil", list.getUserEmail());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        CardView jobs;
        ImageView userimage;
        TextView username,services,date,location,Address;
        Button accept,cansel;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            jobs = itemView.findViewById(R.id.next);
            userimage = itemView.findViewById(R.id.userimage);
            username = itemView.findViewById(R.id.username);
            services = itemView.findViewById(R.id.servicename);
            date = itemView.findViewById(R.id.Date);
            accept = itemView.findViewById(R.id.Accept);
            cansel = itemView.findViewById(R.id.cancel);
            location=itemView.findViewById(R.id.location);
            Address=itemView.findViewById(R.id.Address);

        }


//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.Accept:
//                    onAcceptClick.acceptclik(getLayoutPosition(),"1");
//            }
//
//        }
    }
}
