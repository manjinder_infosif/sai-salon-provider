package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.R;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    public PhotosAdapter(Context context, List<String> photolist, Click click) {
        this.context = context;
        this.click=click;
        this.photolist=photolist;
    }

    private Context context;
    private Click click;
    private List<String>photolist;
    public interface Click{
        void removephoto(int position);
    }



    @NonNull
    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.photolayout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosAdapter.ViewHolder viewHolder, int i) {
        Glide.with(context).load("file://"+photolist.get(i)).into(viewHolder.backimg);
    }

    @Override
    public int getItemCount() {
        return photolist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView backimg,cancel;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            backimg=itemView.findViewById(R.id.imgback);
            cancel=itemView.findViewById(R.id.cancel);
            cancel.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cancel:
                    click.removephoto(getLayoutPosition());
            }

        }
    }
}
