package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import omninos.com.sai_salon.PojoClasses.NotificationPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Activity context;
    Context context2;
    Resources resources;
    List<NotificationPojo.Detail>list;

    public NotificationAdapter(Activity context,List<NotificationPojo.Detail>list) {
        this.context = context;
        this.list=list;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_layout,parent,false);
        context2= LocaleHelper.setLocale(context,App.getAppPreferences().getLanguage(context));
        resources = context.getResources();
        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.MyViewHolder holder, int position) {
        NotificationPojo.Detail detail=list.get(position);

        if(App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
            holder.tv1.setText(detail.getUserName() +" "+ resources.getString(R.string.booked) + detail.getServicesArabicTitle() + " .");
            holder.tv2.setText(detail.getTime());
        }
        else{
            holder.tv1.setText(detail.getUserName() +" "+ resources.getString(R.string.booked) + detail.getServices() + " .");
            holder.tv2.setText(detail.getTime());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv1,tv2;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv1=itemView.findViewById(R.id.tv1);
            tv2=itemView.findViewById(R.id.tv2);
        }
    }
}

