package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.PojoClasses.BookinglistPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {

    Activity context;
    Context context2;
    Resources resources;
    List<BookinglistPojo.Detail> list;

    public BookingAdapter(Activity context, List<BookinglistPojo.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public BookingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookinglayout,parent,false);
        context2 = LocaleHelper.setLocale(context, App.getAppPreferences().getLanguage(context));
        resources = context2.getResources();
        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookingAdapter.MyViewHolder holder, int position) {
        if (App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
            holder.tv1.setText(resources.getString(R.string.bookingtime) + " : " + list.get(position).getServiceTime() + "\n" + resources.getString(R.string.date) + " : " + list.get(position).getServiceDate());
            Glide.with(context).load(list.get(position).getUserImage()).into(holder.img);
            holder.tv2.setText(list.get(position).getUserAddress());
            holder.tv3.setText(resources.getString(R.string.services) + " : " + list.get(position).getServicesArabicTitle().toUpperCase());
            holder.tv4.setText(resources.getString(R.string.service_location) + " : " + list.get(position).getServiceLocationArabic());

        }
        else{
            holder.tv1.setText(resources.getString(R.string.bookingtime) + " : " + list.get(position).getServiceTime() + "\n" + resources.getString(R.string.date) + " : " + list.get(position).getServiceDate());
            Glide.with(context).load(list.get(position).getUserImage()).into(holder.img);
            holder.tv2.setText(list.get(position).getUserAddress());
            holder.tv3.setText(resources.getString(R.string.services) + " : " + list.get(position).getServices().toUpperCase());
            holder.tv4.setText(resources.getString(R.string.service_location) + " : " + list.get(position).getServiceLocation());
        }
        if(list.get(position).getServiceLocation().equalsIgnoreCase("salon")){
            holder.tv2.setVisibility(View.GONE);
        }
        else{
            holder.tv2.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tv1,tv2,tv3,tv4;
        ImageView img;


        public MyViewHolder(View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.img);
            tv1=itemView.findViewById(R.id.tv1);
            tv2=itemView.findViewById(R.id.tv2);
            tv3=itemView.findViewById(R.id.tv3);
            tv4=itemView.findViewById(R.id.tv4);
        }
    }
}

