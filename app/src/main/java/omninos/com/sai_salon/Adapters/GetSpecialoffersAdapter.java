package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.Activities.GetSpecialOfferActivity;
import omninos.com.sai_salon.PojoClasses.GetOffersListPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class GetSpecialoffersAdapter extends RecyclerView.Adapter<GetSpecialoffersAdapter.ViewHolder> {
    Activity context;
    DeleteOffer deleteOffer;
    Context context2;
    Resources resources;
    List<GetOffersListPojo.Detail>list;
    public interface DeleteOffer{
        void delete(int position);

    }

    public GetSpecialoffersAdapter(Activity context, List<GetOffersListPojo.Detail> list, DeleteOffer deleteOffer) {
        this.context = context;
        this.deleteOffer = deleteOffer;
        this.list = list;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.specialofferlayout,viewGroup,false);
        context2 = LocaleHelper.setLocale(context, App.getAppPreferences().getLanguage(context));
        resources = context2.getResources();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
            viewHolder.offername.setText(list.get(i).getArabicTitle());
            viewHolder.services.setText(resources.getString(R.string.services) + " : " + list.get(i).getServicesArabicTitle());
            viewHolder.price.setText(resources.getString(R.string.price) + " : " + list.get(i).getOfferPrice() + " KWD ");
            Glide.with(context).load(list.get(i).getImage()).into(viewHolder.offerimg);
        } else {
            viewHolder.offername.setText(list.get(i).getTitle());
            viewHolder.services.setText(resources.getString(R.string.services) + " : " + list.get(i).getServices());
            viewHolder.price.setText(resources.getString(R.string.price) + " : " + list.get(i).getOfferPrice() + " KWD ");
            Glide.with(context).load(list.get(i).getImage()).into(viewHolder.offerimg);

        }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView offerimg,delete;
        TextView services,offername,price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            offerimg=itemView.findViewById(R.id.offerimg);
            delete=itemView.findViewById(R.id.delete);
            services=itemView.findViewById(R.id.services);
            offername=itemView.findViewById(R.id.offername);
            price=itemView.findViewById(R.id.price);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.delete:
                    deleteOffer.delete(getLayoutPosition());
                    break;
            }
        }
    }
}
