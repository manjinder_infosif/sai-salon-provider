package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import omninos.com.sai_salon.PojoClasses.NewTimeSlotpojo;
import omninos.com.sai_salon.PojoClasses.TimeslotPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;

public class Timeslotadapter2 extends RecyclerView.Adapter<Timeslotadapter2.ViewHolder> {
    Context context;
    List<NewTimeSlotpojo.Detail> list;

    StartTime startTime;
    EndTime endTime;
    SwitchbtnClick switchbtnclick;

    public Timeslotadapter2(Context context, List<NewTimeSlotpojo.Detail> list, StartTime startTime, EndTime endTime, SwitchbtnClick switchbtnclick) {
        this.context = context;
        this.list = list;
        this.startTime = startTime;
        this.endTime = endTime;
        this.switchbtnclick = switchbtnclick;
    }

    public interface StartTime {
        void onstarttime(int position);
    }

    public interface EndTime{
        void onendtime(int position);
    }

    public interface SwitchbtnClick{
        void onswitchclick(int position,String status);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.timeslotlayout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        NewTimeSlotpojo.Detail detail=list.get(i);
        viewHolder.day.setText(list.get(i).getDays());
        viewHolder.starttime_et.setText(list.get(i).getStartTime());
        viewHolder.endtime_et.setText(list.get(i).getEndTime());

        App.getSingletonPojo().setTimeid(list.get(i).getId());
        if (list.get(i).getStatus().equalsIgnoreCase("active")) {
            viewHolder.starttime_et.setFocusable(true);
            viewHolder.endtime_et.setFocusable(true);
            viewHolder.switchbtn.setChecked(true);
            viewHolder.starttime_et.setTextColor(Color.parseColor("#000000"));
            viewHolder.endtime_et.setTextColor(Color.parseColor("#000000"));
        } else if (list.get(i).getStatus().equalsIgnoreCase("inactive")) {
            viewHolder.starttime_et.setFocusable(false);
            viewHolder.endtime_et.setFocusable(false);
            viewHolder.switchbtn.setChecked(false);
            viewHolder.starttime_et.setTextColor(Color.parseColor("#e8148f"));
            viewHolder.endtime_et.setTextColor(Color.parseColor("#e8148f"));
            // notifyDataSetChanged();

        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
        TextView day;
        EditText endtime_et, starttime_et;
        Switch switchbtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            endtime_et = itemView.findViewById(R.id.endtime);
            starttime_et = itemView.findViewById(R.id.starttime);
            day = itemView.findViewById(R.id.day);
            starttime_et.setOnClickListener(this);
            endtime_et.setOnClickListener(this);
            switchbtn=itemView.findViewById(R.id.switchbtn);
            switchbtn.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                //switchbtn.setChecked(false);
                switchbtnclick.onswitchclick(getLayoutPosition(), "active");

            }
            else{
                // switchbtn.setChecked(true);
                switchbtnclick.onswitchclick(getLayoutPosition(),"inactive");
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.starttime:
                    startTime.onstarttime(getLayoutPosition());
                    break;
                case R.id.endtime:
                    endTime.onendtime(getLayoutPosition());
                    break;
                case R.id.switchbtn:
                    break;
            }
        }
    }
}
