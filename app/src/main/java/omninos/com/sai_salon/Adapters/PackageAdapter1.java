package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.PojoClasses.ProviderSpecialOffersPojo;
import omninos.com.sai_salon.R;

public class PackageAdapter1 extends RecyclerView.Adapter<PackageAdapter1.ViewHolder> {


   private Context context;
    List<ProviderSpecialOffersPojo.Detail> list;
    @NonNull
    @Override
    public PackageAdapter1.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.packagees_layout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageAdapter1.ViewHolder viewHolder, int i) {
        ProviderSpecialOffersPojo.Detail detail=list.get(i);
        viewHolder.offerprice.setText(detail.getOffer()+" % "+" on "+ detail.getServices());
        if(detail.getImage()!=null) {
            Glide.with(context).load(detail.getImage()).into(viewHolder.img);
        }
    }

    public PackageAdapter1(Context context, List<ProviderSpecialOffersPojo.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView offerprice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.offerimg);
            offerprice=itemView.findViewById(R.id.offer);
        }
    }
}
