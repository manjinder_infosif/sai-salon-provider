package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.PojoClasses.Packagespojoo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class PackagesubcripAdapter extends RecyclerView.Adapter<PackagesubcripAdapter.ViewHolder> {
    private Activity context;
    Context context2;
    Resources resources;
    private List<Packagespojoo> packagesList;
    private List<String> packagesId = new ArrayList<>();
    private List<Packagespojoo.Detail> detailList;
    private ChoosePackage choosePackage;


    public interface ChoosePackage {
        void Choose(int position);
    }

    public PackagesubcripAdapter(Activity context, List<Packagespojoo.Detail> detailList, ChoosePackage choosePackage) {
        this.context = context;
        this.detailList = detailList;
        this.choosePackage = choosePackage;
    }


    @NonNull
    @Override
    public PackagesubcripAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.packagsubscrip_layout, viewGroup, false);
        context2= LocaleHelper.setLocale(context, App.getAppPreferences().getLanguage(context));
        resources = context2.getResources();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackagesubcripAdapter.ViewHolder viewHolder, final int i) {
        final Packagespojoo.Detail packagespojoo = detailList.get(i);
        //  viewHolder.title.setText(detailList.get(i).getTitle());
        viewHolder.title.setText(packagespojoo.getTitle());
        viewHolder.price.setText(packagespojoo.getPrice());
        viewHolder.description.setText(packagespojoo.getDescription());
        viewHolder.offer.setText(packagespojoo.getOffer());
//        viewHolder.select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                context.startActivity(new Intent(context, CheckoutActivity.class));
//                Toast.makeText(context, "" + packagespojoo.getId(), Toast.LENGTH_SHORT).show();
//            }
//        });
            viewHolder.select.setText(resources.getString(R.string.select));
        if (packagespojoo.getOffer().equalsIgnoreCase("0")) {
            viewHolder.offer.setVisibility(View.GONE);
        } else {
            viewHolder.offer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, offer, description, price;
        Button select;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            offer = itemView.findViewById(R.id.offer);
            description = itemView.findViewById(R.id.description);
            price = itemView.findViewById(R.id.price);
            select = itemView.findViewById(R.id.select);
            select.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.select:
                    choosePackage.Choose(getLayoutPosition());
                    break;
            }
        }
    }
}
