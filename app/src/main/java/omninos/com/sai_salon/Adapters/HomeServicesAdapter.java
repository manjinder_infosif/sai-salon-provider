package omninos.com.sai_salon.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.Activities.AddnewActivity;
import omninos.com.sai_salon.PojoClasses.GetHomeServiceListModel;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class HomeServicesAdapter extends RecyclerView.Adapter<HomeServicesAdapter.MyViewHolder> {

    Activity context;
    Context context2;
    Resources resources;
    private List<GetHomeServiceListModel.Detail> list;
    private RemoveService removeservice;
    public interface RemoveService{
        void remove(int position);
    }


    public HomeServicesAdapter(Activity context, List<GetHomeServiceListModel.Detail> list, RemoveService removeService) {
        this.context = context;
        this.list = list;
        this.removeservice=removeService;
    }


    @Override
    public HomeServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homeservices_layout, parent, false);
        context2 = LocaleHelper.setLocale(context, App.getAppPreferences().getLanguage(context));
        resources = context2.getResources();
        return new HomeServicesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeServicesAdapter.MyViewHolder holder, final int position) {
        if (App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
            holder.tv1.setText(resources.getString(R.string.category) + " : "+list.get(position).getCatArabicTitle().toUpperCase());
            // Glide.with(context).load(list.get(position).getImage()).into(holder.serviceImage);
            holder.serviceTitle.setText(list.get(position).getArabicTitle().toUpperCase());
            holder.tv2.setText( resources.getString(R.string.price)+ " : "+list.get(position).getSalePrice()+ " KWD");
            holder.tv3.setText(resources.getString(R.string.time)+ ": "+list.get(position).getServiceTime()+ resources.getString(R.string.minute));

        }
        else {
            holder.tv1.setText(resources.getString(R.string.category) + " : " + list.get(position).getCategoryTitle().toUpperCase());
            // Glide.with(context).load(list.get(position).getImage()).into(holder.serviceImage);
            holder.serviceTitle.setText(list.get(position).getTitle().toUpperCase());
            holder.tv2.setText(resources.getString(R.string.price) + " : " + list.get(position).getSalePrice() + " KWD");
            holder.tv3.setText(resources.getString(R.string.time) + ": " + list.get(position).getServiceTime() + resources.getString(R.string.minute));
        }
        holder.pencil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(App.getAppPreferences().getLanguage(context).equalsIgnoreCase("ar")) {
                    Intent intent = new Intent(context, AddnewActivity.class);
                    intent.putExtra("serviceid", list.get(position).getId());
                    intent.putExtra("status", "1");
                    context.startActivity(intent);
                }
                else{
                    Intent intent = new Intent(context, AddnewActivity.class);
                    intent.putExtra("serviceid", list.get(position).getId());
                    intent.putExtra("status", "1");
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView delete,pencil;
        private TextView serviceTitle, tv1,tv2,tv3;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv1 = itemView.findViewById(R.id.tv1);
            serviceTitle = itemView.findViewById(R.id.serviceTitle);
            delete = itemView.findViewById(R.id.delete);
            delete.setOnClickListener(this);
            tv2 = itemView.findViewById(R.id.tv2);
            pencil=itemView.findViewById(R.id.pencil);
            tv3 = itemView.findViewById(R.id.tv3);
        }


        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.delete:
                    removeservice.remove(getLayoutPosition());
                    break;


            }
        }
    }

}
