package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.Modelclass.EmployessList;
import omninos.com.sai_salon.R;

public class EmployessAdapter extends RecyclerView.Adapter<EmployessAdapter.ViewHolder> {

    private Context context;
    private List<EmployessList> employessList;
    private Remove removee;
    private Imageclick imageclick;

    public EmployessAdapter(Context context, List<EmployessList> employessList,Imageclick imageclick,Remove removee) {
        this.context = context;
        this.employessList = employessList;
        this.removee=removee;
        this.imageclick=imageclick;
    }

    public interface Imageclick{
        void onimageclick(int position);
    }

    @NonNull
    @Override
    public EmployessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.employeeslayout, viewGroup, false);
        return new ViewHolder(view);
    }
    public interface Remove{
        void Cleardata(Object object);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployessAdapter.ViewHolder viewHolder, final int i) {
        EmployessList list=employessList.get(i);
       viewHolder.email.setText(list.getEmployeeemail());
        viewHolder.name.setText(list.getEmployeename());
        viewHolder.phone.setText(list.getEmployeephone());
        Glide.with(context).load(list.getEmployeeimage()).into(viewHolder.img);

//        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                employessList.remove(i);
//                notifyDataSetChanged();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return employessList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView removeimg, img;
        EditText name, email, phone;
        String namestr,emailst,phonestr,imagestr;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            removeimg = itemView.findViewById(R.id.remove);
            removeimg.setOnClickListener(this);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
            phone = itemView.findViewById(R.id.phone);
            img = itemView.findViewById(R.id.img);
            img.setOnClickListener(this);
            // employee=itemView.findViewById(R.id.employee);

           name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                   // employessList.get(i).employeename = s.toString();
                    employessList.get(getLayoutPosition()).setEmployeename(name.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            phone.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //employessList.get(i).employeephone=s.toString();
                    employessList.get(getLayoutPosition()).setEmployeephone(phone.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

          email.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //employessList.get(i).employeeemail=s.toString();
                    employessList.get(getLayoutPosition()).setEmployeeemail(email.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.remove:
                    removee.Cleardata(employessList.get(getLayoutPosition()));
                    break;
                case R.id.img:
                    imageclick.onimageclick(getLayoutPosition());
                    break;
            }

        }
    }
}
