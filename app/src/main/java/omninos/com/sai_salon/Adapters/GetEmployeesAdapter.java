package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.sai_salon.PojoClasses.GetEmployeelist;
import omninos.com.sai_salon.R;

public class GetEmployeesAdapter extends RecyclerView.Adapter<GetEmployeesAdapter.ViewHolder> {

    List<GetEmployeelist.Detail>list;
    Context context;
    Delete delete;

    public interface Delete{
        void deleteemp(int position,String id);
    }

    public GetEmployeesAdapter( Context context,List<GetEmployeelist.Detail> list, Delete delete) {
        this.list = list;
        this.context = context;
        this.delete = delete;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.getemployeelayout,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.name.setText(list.get(i).getName());
        viewHolder.email.setText(list.get(i).getEmail());
        viewHolder.phone.setText(list.get(i).getPhone());
        Glide.with(context).load(list.get(i).getImage()).into(viewHolder.img);
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete.deleteemp(i,list.get(i).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,phone,email;
        ImageView img,delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            phone=itemView.findViewById(R.id.phone);
            email=itemView.findViewById(R.id.email);
            img=itemView.findViewById(R.id.img);
            delete=itemView.findViewById(R.id.delete);
        }
    }
}
