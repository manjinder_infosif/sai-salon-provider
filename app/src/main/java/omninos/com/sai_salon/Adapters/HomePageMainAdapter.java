package omninos.com.sai_salon.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import omninos.com.sai_salon.Fragments.PendingjobFragment;
import omninos.com.sai_salon.Fragments.UpcomingjobFragment;

public class HomePageMainAdapter extends FragmentPagerAdapter {
    Context context;
    int tabcounts;

    public HomePageMainAdapter(Context context, FragmentManager fm, int tabcounts) {
        super(fm);
        this.context = context;
        this.tabcounts=tabcounts;
    }




    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                PendingjobFragment fragment1 = new PendingjobFragment();
                return fragment1;
            case 1:
               UpcomingjobFragment fragment2 = new UpcomingjobFragment();
                return fragment2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabcounts;
    }
}
