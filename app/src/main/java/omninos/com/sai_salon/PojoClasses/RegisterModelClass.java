package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterModelClass {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }
    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("salonName")
        @Expose
        private String salonName;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("salonImage")
        @Expose
        private String salonImage;
        @SerializedName("motNumber")
        @Expose
        private String motNumber;
        @SerializedName("licenseCopy")
        @Expose
        private String licenseCopy;
        @SerializedName("salonType")
        @Expose
        private String salonType;
        @SerializedName("kidsStatus")
        @Expose
        private String kidsStatus;
        @SerializedName("homeServiceStatus")
        @Expose
        private String homeServiceStatus;
        @SerializedName("reg_id")
        @Expose
        private String regId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("packageId")
        @Expose
        private String packageId;
        @SerializedName("salonNameArbic")
        @Expose
        private String salonNameArbic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getSalonName() {
            return salonName;
        }

        public void setSalonName(String salonName) {
            this.salonName = salonName;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSalonImage() {
            return salonImage;
        }

        public void setSalonImage(String salonImage) {
            this.salonImage = salonImage;
        }

        public String getMotNumber() {
            return motNumber;
        }

        public void setMotNumber(String motNumber) {
            this.motNumber = motNumber;
        }

        public String getLicenseCopy() {
            return licenseCopy;
        }

        public void setLicenseCopy(String licenseCopy) {
            this.licenseCopy = licenseCopy;
        }

        public String getSalonType() {
            return salonType;
        }

        public void setSalonType(String salonType) {
            this.salonType = salonType;
        }

        public String getKidsStatus() {
            return kidsStatus;
        }

        public void setKidsStatus(String kidsStatus) {
            this.kidsStatus = kidsStatus;
        }

        public String getHomeServiceStatus() {
            return homeServiceStatus;
        }

        public void setHomeServiceStatus(String homeServiceStatus) {
            this.homeServiceStatus = homeServiceStatus;
        }

        public String getRegId() {
            return regId;
        }

        public void setRegId(String regId) {
            this.regId = regId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getPackageId() {
            return packageId;
        }

        public void setPackageId(String packageId) {
            this.packageId = packageId;
        }
        public String getSalonNameArbic() {
            return salonNameArbic;
        }

        public void setSalonNameArbic(String salonNameArbic) {
            this.salonNameArbic = salonNameArbic;
        }
    }
}
