package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPendingjobsPojo {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("serviceDate")
        @Expose
        private String serviceDate;
        @SerializedName("serviceTime")
        @Expose
        private String serviceTime;
        @SerializedName("serviceId")
        @Expose
        private String serviceId;
        @SerializedName("providerId")
        @Expose
        private String providerId;
        @SerializedName("userBookingAddress")
        @Expose
        private String userBookingAddress;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("userImage")
        @Expose
        private String userImage;
        @SerializedName("userAddress")
        @Expose
        private String userAddress;
        @SerializedName("userEmail")
        @Expose
        private String userEmail;
        @SerializedName("Services")
        @Expose
        private String services;
        @SerializedName("serviceLocation")
        @Expose
        private String serviceLocation;
        @SerializedName("ServicesArabicTitle")
        @Expose
        private String servicesArabicTitle;
        @SerializedName("serviceLocationArabic")
        @Expose
        private String serviceLocationArabic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getServiceDate() {
            return serviceDate;
        }

        public void setServiceDate(String serviceDate) {
            this.serviceDate = serviceDate;
        }

        public String getServiceTime() {
            return serviceTime;
        }

        public void setServiceTime(String serviceTime) {
            this.serviceTime = serviceTime;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getUserBookingAddress() {
            return userBookingAddress;
        }

        public void setUserBookingAddress(String userBookingAddress) {
            this.userBookingAddress = userBookingAddress;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public String getUserAddress() {
            return userAddress;
        }

        public void setUserAddress(String userAddress) {
            this.userAddress = userAddress;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public String getServices() {
            return services;
        }

        public void setServices(String services) {
            this.services = services;
        }

        public String getServiceLocation() {
            return serviceLocation;
        }

        public void setServiceLocation(String serviceLocation) {
            this.serviceLocation = serviceLocation;
        }
        public String getServicesArabicTitle() {
            return servicesArabicTitle;
        }

        public void setServicesArabicTitle(String servicesArabicTitle) {
            this.servicesArabicTitle = servicesArabicTitle;
        }
        public String getServiceLocationArabic() {
            return serviceLocationArabic;
        }

        public void setServiceLocationArabic(String serviceLocationArabic) {
            this.serviceLocationArabic = serviceLocationArabic;
        }
    }
}
