package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Servicestypesspojo {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details {

        @SerializedName("womanStatus")
        @Expose
        private String womanStatus;
        @SerializedName("manStatus")
        @Expose
        private String manStatus;
        @SerializedName("kidsStatus")
        @Expose
        private String kidsStatus;

        public String getWomanStatus() {
            return womanStatus;
        }

        public void setWomanStatus(String womanStatus) {
            this.womanStatus = womanStatus;
        }

        public String getManStatus() {
            return manStatus;
        }

        public void setManStatus(String manStatus) {
            this.manStatus = manStatus;
        }

        public String getKidsStatus() {
            return kidsStatus;
        }

        public void setKidsStatus(String kidsStatus) {
            this.kidsStatus = kidsStatus;
        }

    }
}
