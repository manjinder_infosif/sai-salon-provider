package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessPojo {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("active_status")
        @Expose
        private String activeStatus;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("salonName")
        @Expose
        private String salonName;
        @SerializedName("salonNameArbic")
        @Expose
        private String salonNameArbic;
        @SerializedName("phoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("flagCode")
        @Expose
        private String flagCode;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("salonImage")
        @Expose
        private String salonImage;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("motNumber")
        @Expose
        private String motNumber;
        @SerializedName("licenseCopy")
        @Expose
        private String licenseCopy;
        @SerializedName("licenseNumberAndCivilNumber")
        @Expose
        private String licenseNumberAndCivilNumber;
        @SerializedName("salonType")
        @Expose
        private String salonType;
        @SerializedName("kidsStatus")
        @Expose
        private String kidsStatus;
        @SerializedName("homeServiceStatus")
        @Expose
        private String homeServiceStatus;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("reg_id")
        @Expose
        private String regId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("packageId")
        @Expose
        private String packageId;
        @SerializedName("coupenId")
        @Expose
        private String coupenId;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("salonRating")
        @Expose
        private Double salonRating;
        @SerializedName("businessId")
        @Expose
        private String businessId;
        @SerializedName("businessLink")
        @Expose
        private String businessLink;
        @SerializedName("TotalCustomers")
        @Expose
        private String totalCustomers;
        @SerializedName("TotalEarnings")
        @Expose
        private String totalEarnings;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getActiveStatus() {
            return activeStatus;
        }

        public void setActiveStatus(String activeStatus) {
            this.activeStatus = activeStatus;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getSalonName() {
            return salonName;
        }

        public void setSalonName(String salonName) {
            this.salonName = salonName;
        }

        public String getSalonNameArbic() {
            return salonNameArbic;
        }

        public void setSalonNameArbic(String salonNameArbic) {
            this.salonNameArbic = salonNameArbic;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getFlagCode() {
            return flagCode;
        }

        public void setFlagCode(String flagCode) {
            this.flagCode = flagCode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getSalonImage() {
            return salonImage;
        }

        public void setSalonImage(String salonImage) {
            this.salonImage = salonImage;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getMotNumber() {
            return motNumber;
        }

        public void setMotNumber(String motNumber) {
            this.motNumber = motNumber;
        }

        public String getLicenseCopy() {
            return licenseCopy;
        }

        public void setLicenseCopy(String licenseCopy) {
            this.licenseCopy = licenseCopy;
        }

        public String getLicenseNumberAndCivilNumber() {
            return licenseNumberAndCivilNumber;
        }

        public void setLicenseNumberAndCivilNumber(String licenseNumberAndCivilNumber) {
            this.licenseNumberAndCivilNumber = licenseNumberAndCivilNumber;
        }

        public String getSalonType() {
            return salonType;
        }

        public void setSalonType(String salonType) {
            this.salonType = salonType;
        }

        public String getKidsStatus() {
            return kidsStatus;
        }

        public void setKidsStatus(String kidsStatus) {
            this.kidsStatus = kidsStatus;
        }

        public String getHomeServiceStatus() {
            return homeServiceStatus;
        }

        public void setHomeServiceStatus(String homeServiceStatus) {
            this.homeServiceStatus = homeServiceStatus;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getRegId() {
            return regId;
        }

        public void setRegId(String regId) {
            this.regId = regId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getPackageId() {
            return packageId;
        }

        public void setPackageId(String packageId) {
            this.packageId = packageId;
        }

        public String getCoupenId() {
            return coupenId;
        }

        public void setCoupenId(String coupenId) {
            this.coupenId = coupenId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public Double getSalonRating() {
            return salonRating;
        }

        public void setSalonRating(Double salonRating) {
            this.salonRating = salonRating;
        }

        public String getBusinessId() {
            return businessId;
        }

        public void setBusinessId(String businessId) {
            this.businessId = businessId;
        }

        public String getBusinessLink() {
            return businessLink;
        }

        public void setBusinessLink(String businessLink) {
            this.businessLink = businessLink;
        }

        public String getTotalCustomers() {
            return totalCustomers;
        }

        public void setTotalCustomers(String totalCustomers) {
            this.totalCustomers = totalCustomers;
        }

        public String getTotalEarnings() {
            return totalEarnings;
        }

        public void setTotalEarnings(String totalEarnings) {
            this.totalEarnings = totalEarnings;
        }

    }
}
