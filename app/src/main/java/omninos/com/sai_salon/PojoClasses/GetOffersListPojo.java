package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetOffersListPojo implements Serializable{
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }
    public class OfferService {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("serviceType")
        @Expose
        private String serviceType;
        @SerializedName("providerId")
        @Expose
        private String providerId;
        @SerializedName("categoryId")
        @Expose
        private String categoryId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("arabicTitle")
        @Expose
        private String arabicTitle;
        @SerializedName("actualPrice")
        @Expose
        private String actualPrice;
        @SerializedName("salePrice")
        @Expose
        private String salePrice;
        @SerializedName("serviceTime")
        @Expose
        private String serviceTime;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("serviceLocation")
        @Expose
        private String serviceLocation;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArabicTitle() {
            return arabicTitle;
        }

        public void setArabicTitle(String arabicTitle) {
            this.arabicTitle = arabicTitle;
        }

        public String getActualPrice() {
            return actualPrice;
        }

        public void setActualPrice(String actualPrice) {
            this.actualPrice = actualPrice;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getServiceTime() {
            return serviceTime;
        }

        public void setServiceTime(String serviceTime) {
            this.serviceTime = serviceTime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getServiceLocation() {
            return serviceLocation;
        }

        public void setServiceLocation(String serviceLocation) {
            this.serviceLocation = serviceLocation;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }
    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("providerId")
        @Expose
        private String providerId;
        @SerializedName("servicesId")
        @Expose
        private String servicesId;
        @SerializedName("serviceType")
        @Expose
        private String serviceType;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("arabicTitle")
        @Expose
        private String arabicTitle;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("offer")
        @Expose
        private String offer;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("endDate")
        @Expose
        private String endDate;
        @SerializedName("serviceLocation")
        @Expose
        private String serviceLocation;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("Services")
        @Expose
        private String services;
        @SerializedName("ServicesArabicTitle")
        @Expose
        private String servicesArabicTitle;
        @SerializedName("servicesPrice")
        @Expose
        private String servicesPrice;
        @SerializedName("servicesTotalTime")
        @Expose
        private String servicesTotalTime;
        @SerializedName("offerPrice")
        @Expose
        private String offerPrice;
        @SerializedName("offerService")
        @Expose
        private List<OfferService> offerService = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getServicesId() {
            return servicesId;
        }

        public void setServicesId(String servicesId) {
            this.servicesId = servicesId;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArabicTitle() {
            return arabicTitle;
        }

        public void setArabicTitle(String arabicTitle) {
            this.arabicTitle = arabicTitle;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getServiceLocation() {
            return serviceLocation;
        }

        public void setServiceLocation(String serviceLocation) {
            this.serviceLocation = serviceLocation;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getServices() {
            return services;
        }

        public void setServices(String services) {
            this.services = services;
        }

        public String getServicesArabicTitle() {
            return servicesArabicTitle;
        }

        public void setServicesArabicTitle(String servicesArabicTitle) {
            this.servicesArabicTitle = servicesArabicTitle;
        }

        public String getServicesPrice() {
            return servicesPrice;
        }

        public void setServicesPrice(String servicesPrice) {
            this.servicesPrice = servicesPrice;
        }

        public String getServicesTotalTime() {
            return servicesTotalTime;
        }

        public void setServicesTotalTime(String servicesTotalTime) {
            this.servicesTotalTime = servicesTotalTime;
        }

        public String getOfferPrice() {
            return offerPrice;
        }

        public void setOfferPrice(String offerPrice) {
            this.offerPrice = offerPrice;
        }

        public List<OfferService> getOfferService() {
            return offerService;
        }

        public void setOfferService(List<OfferService> offerService) {
            this.offerService = offerService;
        }

    }
}
