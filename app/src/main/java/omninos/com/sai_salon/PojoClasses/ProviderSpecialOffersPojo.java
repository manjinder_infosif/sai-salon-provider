package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProviderSpecialOffersPojo {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("salonId")
        @Expose
        private String salonId;
        @SerializedName("serviceType")
        @Expose
        private String serviceType;
        @SerializedName("serviceLocation")
        @Expose
        private String serviceLocation;
        @SerializedName("servicesId")
        @Expose
        private String servicesId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("endDate")
        @Expose
        private String endDate;
        @SerializedName("offer")
        @Expose
        private String offer;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("Services")
        @Expose
        private String services;
        @SerializedName("servicesPrice")
        @Expose
        private Integer servicesPrice;
        @SerializedName("offerPrice")
        @Expose
        private Integer offerPrice;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSalonId() {
            return salonId;
        }

        public void setSalonId(String salonId) {
            this.salonId = salonId;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getServiceLocation() {
            return serviceLocation;
        }

        public void setServiceLocation(String serviceLocation) {
            this.serviceLocation = serviceLocation;
        }

        public String getServicesId() {
            return servicesId;
        }

        public void setServicesId(String servicesId) {
            this.servicesId = servicesId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getOffer() {
            return offer;
        }

        public void setOffer(String offer) {
            this.offer = offer;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getServices() {
            return services;
        }

        public void setServices(String services) {
            this.services = services;
        }

        public Integer getServicesPrice() {
            return servicesPrice;
        }

        public void setServicesPrice(Integer servicesPrice) {
            this.servicesPrice = servicesPrice;
        }

        public Integer getOfferPrice() {
            return offerPrice;
        }

        public void setOfferPrice(Integer offerPrice) {
            this.offerPrice = offerPrice;
        }

    }
}
