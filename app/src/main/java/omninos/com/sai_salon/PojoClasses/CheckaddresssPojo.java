package omninos.com.sai_salon.PojoClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckaddresssPojo {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("salonid")
        @Expose
        private String salonid;
        @SerializedName("countryid")
        @Expose
        private String countryid;
        @SerializedName("stateid")
        @Expose
        private String stateid;
        @SerializedName("cityid")
        @Expose
        private String cityid;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pin")
        @Expose
        private String pin;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("cityName")
        @Expose
        private String cityName;
        @SerializedName("stateName")
        @Expose
        private String stateName;
        @SerializedName("countryName")
        @Expose
        private String countryName;
        @SerializedName("countryArabicName")
        @Expose
        private String countryArabicName;
        @SerializedName("cityNameArabic")
        @Expose
        private String cityNameArabic;
        @SerializedName("stateNameArabic")
        @Expose
        private String stateNameArabic;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSalonid() {
            return salonid;
        }

        public void setSalonid(String salonid) {
            this.salonid = salonid;
        }

        public String getCountryid() {
            return countryid;
        }

        public void setCountryid(String countryid) {
            this.countryid = countryid;
        }

        public String getStateid() {
            return stateid;
        }

        public void setStateid(String stateid) {
            this.stateid = stateid;
        }

        public String getCityid() {
            return cityid;
        }

        public void setCityid(String cityid) {
            this.cityid = cityid;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }
        public String getCountryArabicName() {
            return countryArabicName;
        }

        public void setCountryArabicName(String countryArabicName) {
            this.countryArabicName = countryArabicName;
        }
        public String getCityNameArabic() {
            return cityNameArabic;
        }

        public void setCityNameArabic(String cityNameArabic) {
            this.cityNameArabic = cityNameArabic;
        }
        public String getStateNameArabic() {
            return stateNameArabic;
        }

        public void setStateNameArabic(String stateNameArabic) {
            this.stateNameArabic = stateNameArabic;
        }
    }
}
