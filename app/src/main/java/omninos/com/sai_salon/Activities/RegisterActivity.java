package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.bumptech.glide.Glide;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.PojoClasses.RegisterModelClass;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.Commonutil;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Util.LocaleHelper;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView regaction,licensetv,livensecopytv,nametv1,nametv2,salonimgtv,salontypetv,women,men,kids,homeservuces,phonetv,emailtv,passwdtv,confirmpasswdtv;
    private Button next, ok;
    private AwesomeValidation awesomeValidation;
    private ImageView back, salonimage, plusimage, licenseimage, plusimage1;
    private ViewModell viewModell;
    private StringBuffer builder;
    private String UserimagePath;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private File photoFile;
    private int count = 0;
    private Dialog dialog;
    private String salontypestatus = "0";
    private String kidsstatus = "0";
    private String homeservicesstatus = "0";
    private CountryCodePicker countrypicker;
    private List<String> salontypess;
    private String device_type = "android", login_type = "normal", reg_id = "";
    private Spinner lang;
    private CheckBox chckBox1, chckBox2, chckBox3, chckBox4, termschckbox;
    private EditText salonname, password, email, confirmpasword, phone, motno, License_Number, salonnameArabic;
    private String motnostr, salnonname1, password1, email1, confirmpasword1, phone1, lang1, salonphotopath, licensepath, flagcode, country, Str_License_Number, Str_salonnameArabic, StrCountryCode;

    public static int APP_REQUEST_CODE = 99;
    Context context;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        builder = new StringBuffer();
        salontypess = new ArrayList<>();
        initId();
        context = LocaleHelper.setLocale(RegisterActivity.this, App.getAppPreferences().getLanguage(RegisterActivity.this));
        resources = context.getResources();

        ChangeLanguage();

        App.getSingletonPojo().setSalontypes(salontypess);
        viewModell = ViewModelProviders.of(this).get(ViewModell.class);

        // login.setPaintFlags(login.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(this, R.id.salonname, "[a-zA-Z\\s]+", R.string.err_name);
        awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);


        String regexPassword = "(?=.*[a-z]).{6,}";
        awesomeValidation.addValidation(this, R.id.passwd, regexPassword, R.string.passwordError);
        awesomeValidation.addValidation(this, R.id.passwd, R.id.confirmpasswd, R.string.passNotMatch);

//        phone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        // textcolor();
    }



//    private void textcolor() {
//        SpannableStringBuilder builder=new SpannableStringBuilder();
//        String black="I agree all ";
//        SpannableString blackstring=new SpannableString(black);
//        blackstring.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)),0,black.length(),0);
//        builder.append(blackstring);
//
//        String pink="Terms & Conditions";
//        SpannableString pinkstring=new SpannableString(pink);
//        pinkstring.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)),0,pink.length(),0);
//        builder.append(pinkstring);
//
//        txt.setText(builder, TextView.BufferType.SPANNABLE);
//    }

    private void initId() {

        next = (Button) findViewById(R.id.next);
        back = findViewById(R.id.back);
        next.setOnClickListener(this);
        back.setOnClickListener(this);
        salonname = findViewById(R.id.salonname);
        password = findViewById(R.id.passwd);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        confirmpasword = findViewById(R.id.confirmpasswd);
        chckBox1 = findViewById(R.id.chckbox1);
        chckBox2 = findViewById(R.id.chckbox2);
        chckBox3 = findViewById(R.id.chckbox3);
        chckBox4 = findViewById(R.id.chckbox4);
        chckBox1.setOnClickListener(this);
        chckBox2.setOnClickListener(this);
        chckBox3.setOnClickListener(this);
        chckBox4.setOnClickListener(this);
        //  motno = findViewById(R.id.motno);
        //  lang = findViewById(R.id.sp1);
        //   lang.setOnItemSelectedListener(this);
        licenseimage = findViewById(R.id.licenseimage);
        plusimage1 = findViewById(R.id.plusimg1);
        salonimage = findViewById(R.id.salonimage);
        plusimage = findViewById(R.id.plusimg);
        plusimage.setOnClickListener(this);
        plusimage1.setOnClickListener(this);
        countrypicker = findViewById(R.id.countrypicker);
        dialog = new Dialog(this);
        License_Number = findViewById(R.id.License_Number);
        salonnameArabic = findViewById(R.id.salonnameArabic);

        phone.setFocusable(false);
        phone.setOnClickListener(this);

        phonetv=findViewById(R.id.phonetv);
        emailtv=findViewById(R.id.emailtv);
        nametv1=findViewById(R.id.nametv);
        nametv2=findViewById(R.id.nametv2);
        passwdtv=findViewById(R.id.passwdtv);
        confirmpasswdtv=findViewById(R.id.confrmpassdtv);
        salonimgtv=findViewById(R.id.salonimgtv);
        licensetv=findViewById(R.id.licensetv);
        livensecopytv=findViewById(R.id.licncopytv);
        salontypetv=findViewById(R.id.salontypetv);
        women=findViewById(R.id.women);
        men=findViewById(R.id.men);
        kids=findViewById(R.id.kids);
        homeservuces=findViewById(R.id.services);
        regaction=findViewById(R.id.registeraction);

    }
    private void ChangeLanguage() {
        regaction.setText(resources.getString(R.string.register));
        nametv1.setText(resources.getString(R.string.salon_name_english));
        salonname.setHint(resources.getString(R.string.salon_name_english));
        nametv2.setText(resources.getString(R.string.salon_name_arabic));
        salonnameArabic.setHint(resources.getString(R.string.salon_name_arabic));
        emailtv.setText(resources.getString(R.string.email));
        email.setHint(resources.getString(R.string.email));
        passwdtv.setText(resources.getString(R.string.password));
        password.setHint(resources.getString(R.string.password));
        phonetv.setText(resources.getString(R.string.phone_number));
        phone.setHint(resources.getString(R.string.phone));
        confirmpasword .setHint(resources.getString(R.string.confirm_password));
        confirmpasswdtv .setText(resources.getString(R.string.confirm_password));
        livensecopytv.setText(resources.getString(R.string.license_copy));
        licensetv.setText(resources.getString(R.string.license_number_civil_number));
        License_Number.setHint(resources.getString(R.string.license_number_civil_number));
        salonimgtv.setText(resources.getString(R.string.salon_image));
        salontypetv.setText(resources.getString(R.string.salon_type));
        women.setText(resources.getString(R.string.women));
        men.setText(resources.getString(R.string.men));
        kids.setText(resources.getString(R.string.kids));
        homeservuces.setText(resources.getString(R.string.home_services));
        next.setText(resources.getString(R.string.register));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                if (awesomeValidation.validate() && salonimage.getDrawable() != null && licenseimage.getDrawable() != null)

                {
                    if (chckBox1.isChecked() || chckBox2.isChecked() || chckBox3.isChecked() || chckBox4.isChecked())

                    {
                        registeration();
                    } else {
                        Toast.makeText(this, "Choose SalonType", Toast.LENGTH_SHORT).show();
                    }

                    //    startActivity(new Intent(RegisterActivity.this, PackageSubscriptionActivity.class));
                } else {
                    Toast.makeText(this, "Enter all credentials  ", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.back:
                onBackPressed();
                break;
            case R.id.plusimg1:
                count = 2;
                OpenImageData();
                //  plusimage1.setVisibility(View.GONE);
                break;

            case R.id.plusimg:
                count = 1;
                OpenImageData();
                //  plusimage.setVisibility(View.GONE);
                break;

            case R.id.chckbox1:
                if (chckBox1.isChecked()) {
                    //salontypess.add("women");
                    salontypestatus = "0";
                    chckBox2.setEnabled(false);
                } else {
                    chckBox2.setEnabled(true);
                    salontypestatus = "1";
//                    if(salontypess.contains("women"))
//                         salontypess.remove("women");
                }
                break;
            case R.id.chckbox2:
                if (chckBox2.isChecked()) {
                    //salontypess.add("men");
                    salontypestatus = "1";
                    chckBox1.setEnabled(false);
                } else {
                    chckBox1.setEnabled(true);
                    salontypestatus = "0";
//                    if(salontypess.contains("men"))
//                        salontypess.remove("men");
                }
                break;
            case R.id.chckbox3:
                if (chckBox3.isChecked()) {
                    // salontypess.add("kids");
                    kidsstatus = "1";
                } else {
                    kidsstatus = "0";
//                    if(salontypess.contains("kids"))
//                        salontypess.remove("kids");
                }
                break;
            case R.id.chckbox4:
                if (chckBox4.isChecked()) {
                    homeservicesstatus = "1";
                    // salontypess.add("home services");

                } else {
                    homeservicesstatus = "0";
//                    if(salontypess.contains("home services"))
//                        salontypess.remove("home services");
                }
                break;

            case R.id.phone:
                phoneLogin();
                break;

        }
    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    plusimage.setVisibility(View.VISIBLE);
                    plusimage1.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //choose from camera
    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(RegisterActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                // Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    //Choose from gallery
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("profilePic/*");
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {

                // set image path
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                //  App.getAppPreferences().SaveString(RegisterActivity.this, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(UserimagePath, options);
                if (count == 2) {
                    // String path= App.getAppPreferences().GetString(BasicActivity.this,UserimagePath);
                    licenseimage.setImageBitmap(mBitmapInsurance);
                } else if (count == 1) {
//                    Bitmap mphoto = (Bitmap) data.getExtras().get("data");
                    salonimage.setImageBitmap(mBitmapInsurance);
                }
            }
        }


        if (requestCode == APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {
                Toast.makeText(this, loginResult.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                Toast.makeText(this, "Login Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                if (loginResult.getAccessToken() != null) {
//                    Toast.makeText(this, "Success" + loginResult.getAccessToken().getAccountId(), Toast.LENGTH_SHORT).show();
                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(Account account) {
                            phone.setText(account.getPhoneNumber().toString());
                            StrCountryCode = account.getPhoneNumber().getCountryCode();
                        }

                        @Override
                        public void onError(AccountKitError accountKitError) {

                        }
                    });

                } else {
                    Toast.makeText(this, loginResult.getAuthorizationCode(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                // UserimagePath = imagepath;
                if (count == 2) {
                    //licenseimage.setImageURI(uri);
                    Glide.with(RegisterActivity.this).load("file://" + UserimagePath).into(licenseimage);

                } else if (count == 1) {
                    //  Bitmap mphoto=(Bitmap)data.getExtras().get("data");
                    //  salonimage.setImageURI(uri);
                    Glide.with(RegisterActivity.this).load("file://" + UserimagePath).into(salonimage);
                }

//                App.getAppPreference().SaveString(RegisterActivity.this, ConstantData.IMAGEPATH, UserimagePath);
            }

        }
    }


    private void registeration() {
        salnonname1 = salonname.getText().toString();
        Str_License_Number = License_Number.getText().toString().trim();
        Str_salonnameArabic = salonnameArabic.getText().toString().trim();

        phone1 = phone.getText().toString();
        email1 = email.getText().toString();
        App.getSingletonPojo().setUseremail(email1);
//        flagcode = countrypicker.getSelectedCountryNameCode().toLowerCase();
        country = countrypicker.getSelectedCountryName();
        //motnostr = motno.getText().toString();
        password1 = password.getText().toString();
        confirmpasword1 = confirmpasword.getText().toString();
        licensepath = UserimagePath;
        App.getSingletonPojo().setLicensecopy(licensepath);
        salonphotopath = UserimagePath;
        App.getSingletonPojo().setSalonphotopath(salonphotopath);
//        for (int i = 0; i < salontypess.size(); i++) {
//            if (i == salontypess.size() - 1) {
//                builder.append(salontypess.get(i));
//            } else {
//                builder.append(salontypess.get(i) + ",");
//            }
//        }
//        salonTypestr = String.valueOf(builder);
        RequestBody flagcoderequestbody = RequestBody.create(MediaType.parse("text/plain"), StrCountryCode);
        RequestBody countryrequestbody = RequestBody.create(MediaType.parse("text/plain"), country);
        RequestBody salonnamerequestbody = RequestBody.create(MediaType.parse("text/plain"), salnonname1);
        RequestBody languagerequestbody = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody phonerequestbody = RequestBody.create(MediaType.parse("text/plain"), phone1);
        RequestBody emailrequestbody = RequestBody.create(MediaType.parse("text/plain"), email1);
        RequestBody passwordrequestbody = RequestBody.create(MediaType.parse("text/plain"), password1);
        RequestBody motnorequestbody = RequestBody.create(MediaType.parse("text/plain"), "");
        final RequestBody salontyperequestbody = RequestBody.create(MediaType.parse("text/plain"), salontypestatus);
        final RequestBody kidsstatusrequestbody = RequestBody.create(MediaType.parse("text/plain"), kidsstatus);
        final RequestBody homeservicesrequestbody = RequestBody.create(MediaType.parse("text/plain"), homeservicesstatus);
        RequestBody devicetyperequestbody = RequestBody.create(MediaType.parse("text/plain"), device_type);
        RequestBody regidrequestbody = RequestBody.create(MediaType.parse("text/plain"), FirebaseInstanceId.getInstance().getToken());
        RequestBody salonnameArabicBody = RequestBody.create(MediaType.parse("text/plain"), Str_salonnameArabic);
        RequestBody License_Numberbody = RequestBody.create(MediaType.parse("text/plain"), Str_License_Number);

        File file = new File(App.getSingletonPojo().getSalonphotopath());
        final RequestBody rf = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part salonLogo = MultipartBody.Part.createFormData("salonImage", file.getName(), rf);

        File file2 = new File(App.getSingletonPojo().getLicensecopy());
        final RequestBody rf2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
        MultipartBody.Part licenseimage = MultipartBody.Part.createFormData("licenseCopy", file.getName(), rf2);

        if (Commonutil.InternetCheck(RegisterActivity.this)) {
            viewModell.register(RegisterActivity.this, languagerequestbody, salonnamerequestbody, phonerequestbody, emailrequestbody, passwordrequestbody, motnorequestbody,
                    salontyperequestbody, kidsstatusrequestbody, regidrequestbody, devicetyperequestbody, licenseimage, salonLogo, homeservicesrequestbody, flagcoderequestbody, countryrequestbody, salonnameArabicBody, License_Numberbody).observe(this, new Observer<RegisterModelClass>() {
                @Override
                public void onChanged(@Nullable RegisterModelClass registerModelClass) {
                    if (registerModelClass.getSuccess().equalsIgnoreCase("1")) {
                       //String id = registerModelClass.getDetails().;

                        App.getAppPreferences().SaveString(ConstantData.USER_ID, registerModelClass.getDetails().getId());
                        App.getAppPreferences().SaveString(ConstantData.SALONIMAGE,registerModelClass.getDetails().getSalonImage());
                        App.getAppPreferences().SaveString(ConstantData.SALONNAME,registerModelClass.getDetails().getSalonName());
                        App.getAppPreferences().SaveString(ConstantData.EMAIL,registerModelClass.getDetails().getEmail());
                        App.getAppPreferences().SaveString(ConstantData.PHONE,registerModelClass.getDetails().getPhoneNumber());
                        //  App.getAppPreferences().Savestatus(ConstantData.MAN_WOMANSTATUS,registerModelClass.getDetails().getSalonType());
                        App.getAppPreferences().SaveString(ConstantData.SALONNAME_ARABIC,registerModelClass.getDetails().getSalonNameArbic());
                        dialogbox();

                    } else {
                        Toast.makeText(RegisterActivity.this, "" + registerModelClass.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        //lang1 = String.valueOf(lang.getSelectedItem());
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }

    private void dialogbox() {
        dialog.setContentView(R.layout.verificationdialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        ok = dialog.findViewById(R.id.ok);
        ok.setText(resources.getString(R.string.ok));
        TextView data = dialog.findViewById(R.id.textdata);
        data.setText(resources.getString(R.string.your_documents_are_verifying_nwe_will_notify_you_once_your_document_nverification_is_done_n_thankyou));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finishAffinity();
            }
        });

    }


    public void phoneLogin() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }
}
//    MultipartBody.Part[] imagesData = new MultipartBody.Part[App.getSingletonPojo().getPhotoslist().size()];
//        for (int i = 0; i < App.getSingletonPojo().getPhotoslist().size(); i++) {
//
//        File file1 = new File(App.getSingletonPojo().getPhotoslist().get(i));
//final RequestBody rf1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
//        imagesData[i] = MultipartBody.Part.createFormData("salonLogo", file.getName(), rf1);
//        }