package omninos.com.sai_salon.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import omninos.com.sai_salon.Adapters.SalonAdapter;
import omninos.com.sai_salon.R;

public class Salon_ServicesActivity extends AppCompatActivity {

   private RecyclerView rvsalon;
   private SalonAdapter adapter;
   private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_services);
        initId();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Salon_ServicesActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvsalon.setLayoutManager(linearLayoutManager);
        adapter= new SalonAdapter(this);
        rvsalon.setAdapter(adapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }



    private void initId() {
        rvsalon=findViewById(R.id.rvsalon);
        back=findViewById(R.id.back);
    }
}
