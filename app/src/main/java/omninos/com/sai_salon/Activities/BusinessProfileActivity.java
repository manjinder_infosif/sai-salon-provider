package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import omninos.com.sai_salon.Adapters.BusinessProfileAdapter;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.BusinessPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class BusinessProfileActivity extends AppCompatActivity implements View.OnClickListener {
 private  RecyclerView rv;
 private  ImageView editprofile,back,img;
 private Button services;
 private TextView tottalearning, totalcustomers,businesslink,businessid,address,salonnae,businessaction,tv1,tv2,tv3,tv4,idtxt;
 private RatingBar ratingBar;
 private ViewModelNext viewModelNext;
    Context context;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        context = LocaleHelper.setLocale(BusinessProfileActivity.this, App.getAppPreferences().getLanguage(BusinessProfileActivity.this));
        resources = context.getResources();
        initId();
        ChooseLanguage();
        BusinessProfileAdapter adapter=new BusinessProfileAdapter(this);
        rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        rv.setAdapter(adapter);
        apihit();

    }

    private void ChooseLanguage() {

        businessaction.setText(resources.getString(R.string.business_profile));
        tv1.setText(resources.getString(R.string.total_earning));
        tv2.setText(resources.getString(R.string.total_customers_monthly));
        tv3.setText(resources.getString(R.string.salon_rating));
        tv4.setText(resources.getString(R.string.business_id));

    }

    private void apihit() {
        viewModelNext.businessprofile(BusinessProfileActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(BusinessProfileActivity.this, new Observer<BusinessPojo>() {
            @Override
            public void onChanged(@Nullable BusinessPojo businessPojo) {
                if(businessPojo.getSuccess().equalsIgnoreCase("1")){
                    if(businessPojo.getDetails()!=null) {
                        tottalearning.setText(businessPojo.getDetails().getTotalEarnings()+ " KWD");
                        totalcustomers.setText(businessPojo.getDetails().getTotalCustomers());
                      //  businesslink.setText(businessPojo.getDetails().getBusinessLink());
                        businessid.setText(businessPojo.getDetails().getBusinessId());
                        ratingBar.setRating(Float.parseFloat(String.valueOf(businessPojo.getDetails().getSalonRating())));
                        if(App.getAppPreferences().getLanguage(BusinessProfileActivity.this).equalsIgnoreCase("ar")){
                            salonnae.setText(businessPojo.getDetails().getSalonNameArbic());
                        }
                        else {
                            salonnae.setText(businessPojo.getDetails().getSalonName());
                        }
                        address.setText(businessPojo.getDetails().getAddress());
                        App.getSingletonPojo().setLatitude(businessPojo.getDetails().getLatitude());
                        App.getSingletonPojo().setLongitude(businessPojo.getDetails().getLongitude());
                        Glide.with(BusinessProfileActivity.this).load(businessPojo.getDetails().getSalonImage()).into(img);

                    }
                }
            }
        });
    }


    private void initId() {
        rv= findViewById(R.id.rv);
        editprofile= findViewById(R.id.editprofile);
        editprofile.setOnClickListener(this);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        totalcustomers=findViewById(R.id.totalcustomer);
        tottalearning=findViewById(R.id.totalearning);
        businessid=findViewById(R.id.businessid);
        businesslink=findViewById(R.id.businesslink);
        ratingBar=findViewById(R.id.ratingbar);
        services=findViewById(R.id.services);
        services.setOnClickListener(this);
        address=findViewById(R.id.addres);
        salonnae=findViewById(R.id.salonnane);
        img=findViewById(R.id.img);
        businessaction=findViewById(R.id.businessaction);
        tv1=findViewById(R.id.tv1);
        tv2=findViewById(R.id.tv2);
        tv3=findViewById(R.id.tv3);
        tv4=findViewById(R.id.tv4);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editprofile:
                startActivity(new Intent(BusinessProfileActivity.this,EditProfile.class));
                break;
//            case R.id.services:
//                startActivity(new Intent(BusinessProfileActivity.this,Salon_ServicesActivity.class));
//                break;
            case R.id.back:
                onBackPressed();
                break;

        }

    }
}
