package omninos.com.sai_salon.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back;
    TextView actiontxt;
    Context context;
    Resources resources;
    private ImageButton fb,insta,watsapp,twitter,snap,utube,pintrst,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        fb=findViewById(R.id.facebook);
        fb.setOnClickListener(this);
        insta=findViewById(R.id.insta);
        insta.setOnClickListener(this);
        watsapp=findViewById(R.id.whatsapp);
        watsapp.setOnClickListener(this);
        actiontxt=findViewById(R.id.actiontxt);
        twitter=findViewById(R.id.tweet);
        twitter.setOnClickListener(this);
        snap=findViewById(R.id.snap);
        snap.setOnClickListener(this);
        utube=findViewById(R.id.youtube);
        utube.setOnClickListener(this);
        pintrst=findViewById(R.id.pinterest);
        pintrst.setOnClickListener(this);
        email=findViewById(R.id.email);
        email.setOnClickListener(this);
        context = LocaleHelper.setLocale(ShareActivity.this, App.getAppPreferences().getLanguage(ShareActivity.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        actiontxt.setText(resources.getString(R.string.share));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.facebook:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.facebook.katana");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Facebook First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.facebook.katana" )));
                }
                break;
            case R.id.insta:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.instagram.android");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Instagram First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.instagram.android" )));
                }
                break;
            case R.id.whatsapp:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);

                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install WatsApp First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp" )));

                }
                break;
            case R.id.tweet:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Saihttps://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.twitter.android");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Twitter First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android" )));

                }
                break;
            case R.id.snap:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.snapchat.android");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Snapchat First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.snapchat.android" )));
                }
                break;
            case R.id.youtube:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.google.android.youtube");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install youtube First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.youtube" )));
                }
                break;
            case R.id.pinterest:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.pinterest");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Pinterest First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pinterest" )));
                }
                break;
            case R.id.email:
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=omninos.com.sai_salon&hl=en_IN");
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.google.android.gm");
                    startActivity(sendIntent);
                }catch (Exception e){
                    Toast.makeText(ShareActivity.this, "Install Gmail First", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gm" )));

                }
                break;
        }

    }
}
