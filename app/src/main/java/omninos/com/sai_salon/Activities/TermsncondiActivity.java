package omninos.com.sai_salon.Activities;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class TermsncondiActivity extends AppCompatActivity implements View.OnClickListener {
   private ImageView back;
   private WebView webView;
   private TextView actiontxt;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsncondi);
        initId();
        context = LocaleHelper.setLocale(TermsncondiActivity.this, App.getAppPreferences().getLanguage(TermsncondiActivity.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        actiontxt.setText(resources.getString(R.string.terms_conditions));
    }

    private void initId() {
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(this);
        webView=findViewById(R.id.webview);
        actiontxt=findViewById(R.id.actiontxt);
        if(App.getAppPreferences().getLanguage(TermsncondiActivity.this).equalsIgnoreCase("ar")){
            webView.loadUrl("https://www.saisalon.com/admincp/index.php/api/user/termsAndConditionsArabic");
        }else {
            webView.loadUrl("https://www.saisalon.com/admincp/index.php/api/user/termsAndConditions");
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.back:
                onBackPressed();
                break;
        }
    }
}
