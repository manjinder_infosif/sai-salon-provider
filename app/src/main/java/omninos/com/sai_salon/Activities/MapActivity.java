package omninos.com.sai_salon.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    GoogleMap map;
    ImageView back;
    Switch switchbtn;
    Marker salonmarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(this);

        switchbtn=(Switch)findViewById(R.id.switchbtn);
        switchbtn.setOnCheckedChangeListener(MapActivity.this);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapHome);
       // assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        String salonLat = App.getSingletonPojo().getLatitude();
        String salonLan = App.getSingletonPojo().getLongitude();
        LatLng latLng = new LatLng(Double.parseDouble(salonLat), Double.parseDouble(salonLan));
        salonmarker = map.addMarker(new MarkerOptions().position(latLng).title("Salon").icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.logo,"Salon"))));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.2f));
    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId, String name) {

        View customMarkerView;
        customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.salon);
        //  TextView marrkertextview = customMarkerView.findViewById(R.id.username);
        markerImageView.setImageResource(resId);
        //  marrkertextview.setText(name);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.back:
                startActivity(new Intent(MapActivity.this,HomeActivity.class));
                break;

        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {

            switchbtn.setTrackDrawable(getResources().getDrawable(R.drawable.active_toggle));
           // search.setVisibility(View.VISIBLE);

        } else {
            switchbtn.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));
          //  search.setVisibility(View.GONE);
        }

    }
}
