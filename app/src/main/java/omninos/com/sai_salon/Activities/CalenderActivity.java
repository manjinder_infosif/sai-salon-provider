package omninos.com.sai_salon.Activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;

public class CalenderActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView back;
    MaterialCalendarView calendarView;
    TextView datetxt,daytxt,yeartxt;
     FrameLayout fl_calender;
    CaldroidFragment caldroidFragment;
    Date chosen_date = null;
    ColorDrawable blue;
    ColorDrawable white;
    Bundle args;
    Calendar cal;

    int count = 0;

    private List<String> dateList = new ArrayList<>();
   // private List<CustomTimeList>list=new ArrayList<>();
    String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        id();

        //alenderView();
    }

    private void id() {
        caldroidFragment = new CaldroidFragment();
         args = new Bundle();
        fl_calender=findViewById(R.id.fl_calender);
        cal = Calendar.getInstance();
        blue = new ColorDrawable(getResources().getColor(R.color.caldroid_sky_blue));
        white = new ColorDrawable(Color.parseColor("#ffffff"));

        calender();
    }

    private void calender() {
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.fl_calender, caldroidFragment);
        t.commit();

//        caldroidFragment.setCaldroidListener(new CaldroidListener() {
//            @Override
//            public void onSelectDate(Date date, View view) {
//                chosen_date = date;
//                SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
//                String targetdatevalue = targetFormat.format(date);
//
//                CustomTimeList customTimeList = new CustomTimeList();
////                CustomTimeList customTimeList1 = new CustomTimeList();
//                System.out.println("Dates: " + targetdatevalue);
//                if (!timeLists.contains(customTimeList)) {
//                    dateList.add(targetdatevalue);
//                    caldroidFragment.setBackgroundDrawableForDate(blue, date);
//                    customTimeList.setDateData(targetdatevalue);
//                    customTimeList.setStartTime("Start Time");
//                    customTimeList.setEndTime("End Time");
//                    timeLists.add(customTimeList);
//
//                } else {
//                    dateList.remove(targetdatevalue);
//                    caldroidFragment.setBackgroundDrawableForDate(white, date);
//                    timeLists.remove(customTimeList);
//                }
//
//                App.getSinltonPojo().setAvailableDates(timeLists);
//                caldroidFragment.refreshView();
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
        }

    }

//    private void calenderView() {
//        Calendar calendar = Calendar.getInstance();
//        int day = calendar.get(Calendar.DAY_OF_MONTH);
//        int mont = calendar.get(Calendar.MONTH);
//        int yr = calendar.get(Calendar.YEAR);
//        calendarView.state().edit().setMinimumDate(CalendarDay.from(yr, mont, day)).commit();
//
//        calendarView.setDateSelected(Calendar.getInstance().getTime(), true);
//
//        str = new SimpleDateFormat("yyyy-MM-dd").format(calendarView.getSelectedDate().getDate()).toString();
//        //  App.getSinltonPojo().setPatAppointDate(str);
//    //    GetTimeSlot(str);
//
//        String strmontYr = new SimpleDateFormat("MMMM,yyyy").format(calendarView.getSelectedDate().getDate()).toString();
//        yeartxt.setText(strmontYr);
//
//        String strdate = new SimpleDateFormat("dd").format(calendarView.getSelectedDate().getDate()).toString();
//        datetxt.setText(strdate);
//
//        String strday = new SimpleDateFormat("EEEE").format(calendarView.getSelectedDate().getDate()).toString();
//        daytxt.setText(strday);
//
//        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
//            @Override
//            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
//                str = new SimpleDateFormat("yyyy-MM-dd").format(date.getDate()).toString();
//                //Toast.makeText(SelectDateAppointActivity.this, "" + str, Toast.LENGTH_SHORT).show();
//                String strmontYr = new SimpleDateFormat("MMMM,yyyy").format(date.getDate()).toString();
//                yeartxt.setText(strmontYr);
//
//                String strdate = new SimpleDateFormat("dd").format(date.getDate()).toString();
//                datetxt.setText(strdate);
//
//                String strday = new SimpleDateFormat("EEEE").format(date.getDate()).toString();
//                daytxt.setText(strday);
//                //GetTimeSlot(str);
//
//
//                //   App.getSinltonPojo().setPatAppointDate(str);
//            }
//        });
//    }
}
