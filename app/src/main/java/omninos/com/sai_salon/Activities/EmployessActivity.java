package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Adapters.EmployessAdapter;
import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.Modelclass.EmployessList;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.AddEMployeepojo;
import omninos.com.sai_salon.PojoClasses.GetEmployeelist;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.Util.LocaleHelper;

public class EmployessActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back,img;
    private RecyclerView employeerv;
    private Button next, add;
    private EditText no_of_employes;
    private String no_employstr;
    private EmployessAdapter adapter;
    private List<EmployessList> employessList = new ArrayList<>();
    private File photoFile;
    private Uri uri;
    private  JSONArray jsonArray;
    ViewModelNext viewModelNext;
    private String UserimagePath;
    ImageView removeimg;
    EditText name, email, phone;
    TextView nametxt,emailtxt,phonetxt,employeeaction;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private MultipartBody.Part editImagePart;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employess);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        initId();
       // recycViews();
        context = LocaleHelper.setLocale(EmployessActivity.this, App.getAppPreferences().getLanguage(EmployessActivity.this));
        resources = context.getResources();
        ChangeLanguage();

    }

    private void ChangeLanguage() {
        nametxt.setText(resources.getString(R.string.nametxt));
        name.setHint(resources.getString(R.string.nametxt));
        phonetxt.setText(resources.getString(R.string.phone));
        phone.setHint(resources.getString(R.string.phone));
        emailtxt.setText(resources.getString(R.string.email));
        email.setHint(resources.getString(R.string.email));
        employeeaction.setText(resources.getString(R.string.employees));
        next.setText(resources.getString(R.string.add_employee));


    }


//    private void recycViews() {
//
//        adapter = new EmployessAdapter(this, employessList, new EmployessAdapter.Imageclick() {
//            @Override
//            public void onimageclick(int position) {
//                    OpenImageData();
//                   // App.getAppPreferences().SaveString(ConstantData.EMPLOYEEIMAGE_POSITION, String.valueOf(position));
//                App.getSingletonPojo().setEmployeimage_positon(position);
//            }
//        }, new EmployessAdapter.Remove() {
//            @Override
//            public void Cleardata(Object object) {
//                employessList.remove(object);
//                adapter.notifyDataSetChanged();
//            }
//        });
//        employeerv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        employeerv.setAdapter(adapter);
//        App.getSingletonPojo().setEmployeelist(employessList);
//    }

    private void initId() {
        back = findViewById(R.id.back);
      //  next = findViewById(R.id.next);
        next = findViewById(R.id.next);
        next.setOnClickListener(this);
        back.setOnClickListener(this);
        img = findViewById(R.id.img);
        nametxt=findViewById(R.id.nametxt);
        phonetxt=findViewById(R.id.phonetxt);
        emailtxt=findViewById(R.id.emailtxt);
        employeeaction=findViewById(R.id.employeeaction);
//        add = findViewById(R.id.add);
//        add.setOnClickListener(this);
      
        removeimg = findViewById(R.id.remove);
        removeimg.setOnClickListener(this);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        img = findViewById(R.id.img);
        img.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.next:
                apihit();
                break;
            case R.id.img:
                OpenImageData();
                break;
//                startActivity(new Intent(EmployessActivity.this,CheckoutActivity.class));
//               if(employessList.isEmpty()){
//                   Toast.makeText(this, "Enter atleast 1 employee", Toast.LENGTH_SHORT).show();
//               }
//               else{
//                   startActivity(new Intent(EmployessActivity.this,CheckoutActivity.class));
//               }
//                GetAllData();
//                for (EmployessList s:employessList){
//                    System.out.println("Data: " + s.getEmployeename());
//                    System.out.println("Data: " + s.getEmployeeemail());
//                    System.out.println("Data: " + s.getEmployeephone());
//                }
//        
        }

    }

    private void apihit() {
        String namestr=name.getText().toString();
        String emailstr=email.getText().toString();
        String phonestr=phone.getText().toString();

        RequestBody namebody=RequestBody.create(MediaType.parse("text/plain"),namestr );
        RequestBody emailbody=RequestBody.create(MediaType.parse("text/plain"), emailstr);
        RequestBody phonebody=RequestBody.create(MediaType.parse("text/plain"), phonestr);
        RequestBody providerbody=RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));

        if (UserimagePath != null) {
            File file = new File(UserimagePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            editImagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        } else {
            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
            editImagePart = MultipartBody.Part.createFormData("image", "", attachmentEmpty);
        }

        viewModelNext.addemployee(EmployessActivity.this,editImagePart,namebody,emailbody,phonebody,providerbody).observe(EmployessActivity.this, new Observer<AddEMployeepojo>() {
            @Override
            public void onChanged(@Nullable AddEMployeepojo addEMployeepojo) {
                if(addEMployeepojo.getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(EmployessActivity.this, ""+addEMployeepojo.getMessage(), Toast.LENGTH_SHORT).show();
                    //finish();
                    startActivity(new Intent(EmployessActivity.this, GetEmployessActivty.class));
                    finish();
                }
            }
        });

    }


    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EmployessActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(EmployessActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                // Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    //Choose from gallery
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("profilePic/*");
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                img.setImageURI(uri);
                //App.getAppPreferences().SaveString( ConstantData.EMPLOYEEIMAGE, UserimagePath);
//                employessList.get(App.getSingletonPojo().getEmployeimage_positon()).setEmployeeimage(UserimagePath);
//                adapter.notifyDataSetChanged();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {

                // set image path
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                // App.getAppPreferences().SaveString(activity, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
               
                img.setImageBitmap(mBitmapInsurance);
//                App.getAppPreferences().SaveString( ConstantData.EMPLOYEEIMAGE, UserimagePath);
//                employessList.get(App.getSingletonPojo().getEmployeimage_positon()).setEmployeeimage(UserimagePath);
//                adapter.notifyDataSetChanged();

            }
        }
    }
    private void GetAllData() {
        jsonArray = new JSONArray();
        for (EmployessList employessList : employessList) {
            JSONObject jsonObject = new JSONObject();
            RequestBody namebody=RequestBody.create(MediaType.parse("text/plain"), employessList.getEmployeename());
            RequestBody emailbody=RequestBody.create(MediaType.parse("text/plain"), employessList.getEmployeename());
            RequestBody phonebody=RequestBody.create(MediaType.parse("text/plain"), employessList.getEmployeename());
            RequestBody providerbody=RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));

            File file = new File(employessList.getEmployeeimage());
            final RequestBody rf = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), rf);

            try {
                jsonObject.put("Name", namebody);
                jsonObject.put("Email", emailbody);
                jsonObject.put("Number", phonebody);
                jsonObject.put("Image", image);

                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            System.out.println("Data: " + recyclerModel.getEmail());
//            System.out.println("Data: " + recyclerModel.getNumber());
        }

        System.out.println("Data Is: " + jsonArray);
    }

}
