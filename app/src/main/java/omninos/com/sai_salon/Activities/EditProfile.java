package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.EditProfilePojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class EditProfile extends AppCompatActivity implements View.OnClickListener {
    ImageView back,map;
    Button save;
    TextView name,phone,email,profileaction,tv1,tv2,tv3;
    String namestr,emailstr,phonestr;
    ViewModelNext viewModelNext;
    Context context;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
            viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        initId();
        context = LocaleHelper.setLocale(EditProfile.this, App.getAppPreferences().getLanguage(EditProfile.this));
        resources = context.getResources();

        ChangeLanguage();

    }

    private void ChangeLanguage() {
        profileaction.setText(resources.getString(R.string.profile));
        tv1.setText(resources.getString(R.string.business_name));
        tv2.setText(resources.getString(R.string.business_email));
        tv3.setText(resources.getString(R.string.business_phone_number));

    }

    private void initId() {
        back= findViewById(R.id.back);
        back.setOnClickListener(this);
        map=findViewById(R.id.map);
        map.setOnClickListener(this);
        name=findViewById(R.id.businessname);
        phone=findViewById(R.id.phone);
        email=findViewById(R.id.email);
        tv1=findViewById(R.id.tv1);
        tv2=findViewById(R.id.tv2);
        tv3=findViewById(R.id.tv3);
        profileaction=findViewById(R.id.profileaction);
//        save=findViewById(R.id.save);
//        save.setOnClickListener(this);
        if(App.getAppPreferences().getLanguage(EditProfile.this).equalsIgnoreCase("Ar")){
            name.setText(App.getAppPreferences().GetString(ConstantData.SALONNAME_ARABIC));

        }
        else {
            name.setText(App.getAppPreferences().GetString(ConstantData.SALONNAME));
        }
        phone.setText(App.getAppPreferences().GetString(ConstantData.PHONE));
        email.setText(App.getAppPreferences().GetString(ConstantData.EMAIL));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.map:
                startActivity(new Intent(EditProfile.this,MapActivity.class));
                break;
//            case R.id.save:
//               // apihit();
//                break;
        }
    }

//    private void apihit() {
//        namestr=name.getText().toString();
//        emailstr=email.getText().toString();
//        phonestr=phone.getText().toString();
//
//        viewModelNext.editprofile(EditProfile.this,App.getAppPreferences().GetString(ConstantData.USER_ID),namestr,emailstr,phonestr).observe(EditProfile.this, new Observer<EditProfilePojo>() {
//            @Override
//            public void onChanged(@Nullable EditProfilePojo editProfilePojo) {
//                if(editProfilePojo.getSuccess().equalsIgnoreCase("1")){
//                    Toast.makeText(EditProfile.this, ""+editProfilePojo.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
}
