package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.GetContactPojo;
import omninos.com.sai_salon.R;

public class ContactusActivity extends AppCompatActivity {
    ImageView back,img;
    TextView name,phone,email,address;
    ViewModelNext viewModelNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);

        back=(ImageView)findViewById(R.id.back);
        name=findViewById(R.id.name);
        phone=findViewById(R.id.phone);
        email=findViewById(R.id.email);
        address=findViewById(R.id.address);
        img=findViewById(R.id.img);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getdata();
    }
    private void getdata() {
        viewModelNext.getcontacts(ContactusActivity.this).observe(ContactusActivity.this, new Observer<GetContactPojo>() {
            @Override
            public void onChanged(@Nullable GetContactPojo getContactPojo) {
                if(getContactPojo.getSuccess().equalsIgnoreCase("1")){
                    name.setText(getContactPojo.getDetails().getUsername());
                    email.setText(getContactPojo.getDetails().getEmail());
                    phone.setText(getContactPojo.getDetails().getPhone());
                    address.setText(getContactPojo.getDetails().getAddress());
                    if(getContactPojo.getDetails().getImage()!=null) {
                        Glide.with(ContactusActivity.this).load(getContactPojo.getDetails().getImage()).into(img);
                    }
                    else{
                        img.setBackgroundResource(R.drawable.logo);
                    }
                }
            }
        });
    }
}
