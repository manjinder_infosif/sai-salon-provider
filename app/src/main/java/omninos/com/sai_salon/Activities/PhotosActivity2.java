package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Adapters.PhotosAdapter;
import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.ImagesPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.Util.LocaleHelper;

public class PhotosActivity2 extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView photosrv;
    private String UserimagePath;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private File photoFile;
    private Button next;
    private ImageView plus,back;
    private ViewModelNext viewModelNext;
    private TextView actiontext;
    private List<String> photolist=new ArrayList<>();
    private PhotosAdapter adapter;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos2);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        initId();
        recyview();
        context = LocaleHelper.setLocale(PhotosActivity2.this, App.getAppPreferences().getLanguage(PhotosActivity2.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        actiontext.setText(resources.getString(R.string.salon_images));
        next.setText(resources.getString(R.string.save_photos));
    }

    private void recyview() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        photosrv.setLayoutManager(gridLayoutManager);

    }

    private void initId() {
        photosrv = findViewById(R.id.photosrv);
        next = findViewById(R.id.next);
        plus=findViewById(R.id.plusimg);
        plus.setOnClickListener(this);
        next.setOnClickListener(this);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        actiontext=findViewById(R.id.actiontext);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.plusimg:
                OpenImageData();
                break;
            case R.id.next:
                    apihit();
                break;
            case R.id.back:
               startActivity(new Intent(PhotosActivity2.this,SalonimagesActivity.class));
                break;
        }

    }

    private void apihit() {
        if (photolist.isEmpty()) {
            Toast.makeText(this, "Click atleast one photo", Toast.LENGTH_SHORT).show();
        } else {
            MultipartBody.Part[] imagesData = new MultipartBody.Part[photolist.size()];
            for (int i = 0; i < photolist.size(); i++) {
                File file1 = new File(photolist.get(i));
                final RequestBody rf1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                imagesData[i] = MultipartBody.Part.createFormData("images[]", file1.getName(), rf1);
            }
            RequestBody provideridR=RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));

            viewModelNext.sendimagess(PhotosActivity2.this, provideridR,imagesData).observe(PhotosActivity2.this, new Observer<ImagesPojo>() {
                @Override
                public void onChanged(@Nullable ImagesPojo imagesPojo) {
                    if(imagesPojo.getSuccess().equalsIgnoreCase("1")){
                        Toast.makeText(PhotosActivity2.this, ""+imagesPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(PhotosActivity2.this,SalonimagesActivity.class));
                      // onBackPressed();
                       finish();

                    }
                    else  if(imagesPojo.getSuccess().equalsIgnoreCase("0")){
                        Toast.makeText(PhotosActivity2.this, "You can only upload 10 images"+ " , "+imagesPojo.getMessage(), Toast.LENGTH_SHORT).show();
                     //   startActivity(new Intent(PhotosActivity2.this,SalonimagesActivity.class));
                        finish();
                    }
                }
            });
        }

    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(PhotosActivity2.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //choose from camera
    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(PhotosActivity2.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                // Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    //Choose from gallery
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {

                // set image path
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                //   App.getAppPreferences().SaveString(PhotosActivity.this, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(UserimagePath, options);
                photolist.add(ImageUtil.compressImage(photoFile.getAbsolutePath()));
                adapter = new PhotosAdapter(this, photolist, new PhotosAdapter.Click() {
                    @Override
                    public void removephoto(int position) {
//                        OpenImageData();
                        photolist.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                photosrv.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                photolist.add(cursor.getString(column_index_data));
                adapter = new PhotosAdapter(this, photolist, new PhotosAdapter.Click() {
                    @Override
                    public void removephoto(int position) {
//                        OpenImageData();
                        photolist.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                photosrv.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

        }
    }
}
