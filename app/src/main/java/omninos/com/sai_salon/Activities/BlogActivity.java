package omninos.com.sai_salon.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import omninos.com.sai_salon.R;

public class BlogActivity extends AppCompatActivity {
    ImageView back;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        back=(ImageView)findViewById(R.id.back);
        webView=findViewById(R.id.webview);
        webView.loadUrl("https://www.saisalon.com/admincp/index.php/api/user/blog");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
