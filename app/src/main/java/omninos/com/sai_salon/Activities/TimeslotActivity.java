package omninos.com.sai_salon.Activities;

import android.app.TimePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import omninos.com.sai_salon.Adapters.Timeslotadapter;
import omninos.com.sai_salon.Adapters.Timeslotadapter2;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.EditTimeslotspojo;
import omninos.com.sai_salon.PojoClasses.NewTimeSlotpojo;
import omninos.com.sai_salon.PojoClasses.TimeslotPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class TimeslotActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView back;
    RecyclerView timerv;
    List<TimeslotPojo.Detail> list = new ArrayList<>();
    ViewModelNext viewModelNext;
    String format = "", time_str, status;
    Button Update;
    int count = 0;
    JSONArray jsonArray;
    String switchstatus;
    Timeslotadapter timeslotadapter;
    TextView timeslotaction,day,starttime,endtime;
    Timeslotadapter2 timeslotadapter2;
    List<TimeslotPojo.Detail> list1 = new ArrayList<>();
    Context context;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeslot);
        viewModelNext = ViewModelProviders.of(this).get(ViewModelNext.class);
        back = findViewById(R.id.back);
        Update = findViewById(R.id.update);
        Update.setOnClickListener(this);
        timerv = findViewById(R.id.timerv);
        timeslotaction=findViewById(R.id.timeslotaction);
        day=findViewById(R.id.day);
        starttime=findViewById(R.id.start);
        endtime=findViewById(R.id.end                                                 );
        timerv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        gettime();

        context = LocaleHelper.setLocale(TimeslotActivity.this, App.getAppPreferences().getLanguage(TimeslotActivity.this));
        resources = context.getResources();
        ChangeLanguage();



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void ChangeLanguage() {
        timeslotaction.setText(resources.getString(R.string.timeslot));
        day.setText(resources.getString(R.string.day));
        starttime.setText(resources.getString(R.string.start_time));
        endtime.setText(resources.getString(R.string.end_time));
        Update.setText(resources.getString(R.string.update_time));
    }

    private void gettime() {
        viewModelNext.gettime(this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(this, new Observer<TimeslotPojo>() {
            @Override
            public void onChanged(@Nullable TimeslotPojo timeslotPojo) {
                if (timeslotPojo.getSuccess().equalsIgnoreCase("1")) {
                    list = timeslotPojo.getDetails();

                    timeslotadapter = new Timeslotadapter(TimeslotActivity.this, list, new Timeslotadapter.StartTime() {
                        @Override
                        public void onstarttime(int position) {
                            count = 1;
                            PickTime(position);
                        }
                    }, new Timeslotadapter.EndTime() {
                        @Override
                        public void onendtime(int position) {
                            count = 2;
                            PickTime(position);
                        }
                    }, new Timeslotadapter.SwitchbtnClick() {
                        @Override
                        public void onswitchclick(int position, String status) {
                            if (status.equalsIgnoreCase("active")) {
                                switchstatus = "1";
                                list.get(position).setStatus("active");
                                // timeslotadapter.notifyDataSetChanged();
                            } else if (status.equalsIgnoreCase("inactive")) {
                                switchstatus = "0";
                                list.get(position).setStatus("inactive");
                                //timeslotadapter.notifyDataSetChanged();
                            }

                        }
                    });
                    timerv.setAdapter(timeslotadapter);
                    timeslotadapter.notifyDataSetChanged();
                } else {
//                    viewModelNext.getnewtime(TimeslotActivity.this).observe(TimeslotActivity.this, new Observer<TimeslotPojo>() {
//                        @Override
//                        public void onChanged(@Nullable TimeslotPojo newTimeSlotpojo) {
//                            if (newTimeSlotpojo.getSuccess().equalsIgnoreCase("1")) {
//                                list = newTimeSlotpojo.getDetails();
//                                timeslotadapter = new Timeslotadapter(TimeslotActivity.this, newTimeSlotpojo.getDetails(), new Timeslotadapter.StartTime() {
//                                    @Override
//                                    public void onstarttime(int position) {
//                                        count = 1;
//                                        PickTime(position);
//                                    }
//                                }, new Timeslotadapter.EndTime() {
//                                    @Override
//                                    public void onendtime(int position) {
//                                        count = 2;
//                                        PickTime(position);
//                                    }
//                                }, new Timeslotadapter.SwitchbtnClick() {
//                                    @Override
//                                    public void onswitchclick(int position, String status) {
//                                        if (status.equalsIgnoreCase("active")) {
//                                            switchstatus = "1";
//                                            list.get(position).setStatus("active");
//                                            // timeslotadapter.notifyDataSetChanged();
//                                        } else if (status.equalsIgnoreCase("inactive")) {
//                                            switchstatus = "0";
//                                            list.get(position).setStatus("inactive");
//                                            //timeslotadapter.notifyDataSetChanged();
//                                        }
//                                    }
//                                });
//                                timerv.setAdapter(timeslotadapter);
//                                timeslotadapter.notifyDataSetChanged();
//                            }
//                        }
//                    });
                }


            }
        });
    }


    private void PickTime(final int position) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

//                        if (hourOfDay == 0) {
//                            hourOfDay += 12;
//                            format = "AM";
//                        } else if (hourOfDay == 12) {
//                            format = "PM";
//                        } else if (hourOfDay > 12) {
//                            hourOfDay -= 12;
//                            format = "PM";
//                        } else {
//                            format = "AM";
//                        }
//
                        time_str = hourOfDay + ":" + minute + ":00";
                        if (time_str != null) {
                            //  writetimetv.setText(time_str);
                            if (count == 1) {
                                list.get(position).setStartTime(time_str);
                                timeslotadapter.notifyDataSetChanged();
                            } else if (count == 2) {
                                list.get(position).setEndTime(time_str);
                                timeslotadapter.notifyDataSetChanged();
                            }


                        }
                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update:

                checklist();
                break;
        }

    }

    private void checklist() {
        jsonArray = new JSONArray();
        for (TimeslotPojo.Detail detail : list) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", detail.getId());
                jsonObject.put("startTime", detail.getStartTime());
                jsonObject.put("endTime", detail.getEndTime());
                jsonObject.put("status", detail.getStatus());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            System.out.println("Data: " + detail.getEndTime());
//            System.out.println("Data: " + detail.getStatus());


        }

        System.out.println("Data: " + jsonArray);
        apihit();
    }

    private void apihit() {
        viewModelNext.edittimeget(TimeslotActivity.this, jsonArray, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(TimeslotActivity.this, new Observer<EditTimeslotspojo>() {
            @Override
            public void onChanged(@Nullable EditTimeslotspojo editTimeslotspojo) {
                if (editTimeslotspojo.getSuccess().equalsIgnoreCase("1")) {
                    timeslotadapter.notifyDataSetChanged();
                    gettime();
                    timeslotadapter.notifyDataSetChanged();
                    Toast.makeText(TimeslotActivity.this, "" + editTimeslotspojo.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
