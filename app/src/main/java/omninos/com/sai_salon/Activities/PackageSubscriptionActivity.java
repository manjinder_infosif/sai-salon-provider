package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.PackagesubcripAdapter;
import omninos.com.sai_salon.PojoClasses.Packagespojoo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.Commonutil;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class PackageSubscriptionActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView packagerv;
    private ImageView back;
    private Button register;
    private List<Packagespojoo> packagesList = new ArrayList<>();
    private List<Packagespojoo.Detail> detailList = new ArrayList<>();
    private ViewModell viewModell;
    private PackagesubcripAdapter adapter;
    private TextView packagesaction;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_subscription);
        viewModell = ViewModelProviders.of(this).get(ViewModell.class);
        initid();
        context = LocaleHelper.setLocale(PackageSubscriptionActivity.this, App.getAppPreferences().getLanguage(PackageSubscriptionActivity.this));
        resources = context.getResources();
        ChangeLanguage();
        recycview();
        display();
    }

    private void ChangeLanguage() {
        packagesaction.setText(resources.getString(R.string.packages));
    }

    private void recycview() {
        //adapter = new PackagesubcripAdapter(this, detailList);
        packagerv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }

    private void initid() {
        packagerv = findViewById(R.id.packagerv);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        packagesaction=findViewById(R.id.packagesaction);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
//            case R.id.register:
//                if(!packagesList.isEmpty()) {
//                    startActivity(new Intent(PackageSubscriptionActivity.this, HomeActivity.class));
//                }
//                else{
//                    Toast.makeText(this, "Emptylist", Toast.LENGTH_SHORT).show();
//                }
//                break;
        }
    }

    private void display() {
        if (Commonutil.InternetCheck(PackageSubscriptionActivity.this)) {
            viewModell.packageget(PackageSubscriptionActivity.this).observe(this, new Observer<Packagespojoo>() {
                @Override
                public void onChanged(@Nullable Packagespojoo packagespojoo) {
                    if (packagespojoo.getSuccess().equalsIgnoreCase("1")) {
                        detailList = packagespojoo.getDetails();

                        adapter = new PackagesubcripAdapter(PackageSubscriptionActivity.this, detailList, new PackagesubcripAdapter.ChoosePackage() {
                            @Override
                            public void Choose(int position) {
                                App.getSingletonPojo().setPackageid(detailList.get(position).getId());
                                App.getAppPreferences().SaveString(ConstantData.PACKAGEID,detailList.get(position).getId());
                                startActivity(new Intent(PackageSubscriptionActivity.this, CheckoutActivity.class));
                            }
                        });
                        packagerv.setAdapter(adapter);
                      //  Toast.makeText(PackageSubscriptionActivity.this, "" + packagespojoo.getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                      //  Toast.makeText(PackageSubscriptionActivity.this, "" + packagespojoo.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            Toast.makeText(this, "Check your connection", Toast.LENGTH_SHORT).show();
        }
    }

}
