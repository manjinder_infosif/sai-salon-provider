package omninos.com.sai_salon.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.Toast;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;

public class SelectLangActivity extends AppCompatActivity implements View.OnClickListener {
   private Button next;
   private Button english, arabic;
   private int count = 0;
   private ImageView back;
   private String data,status;
    private String selectedlanguage,Move;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_lang);

        initId();
        status="0";

    }


    private void initId() {
        english = findViewById(R.id.engbtn);
        english.setOnClickListener(this);
        arabic = findViewById(R.id.arabicbtn);
        arabic.setOnClickListener(this);
        next = findViewById(R.id.nxt);
        next.setOnClickListener(this);
        back=findViewById(R.id.back1);
        back.setOnClickListener(this);
                //data = getIntent().getStringExtra("Type");
//        if (data.equalsIgnoreCase("1")) {
//            next.setText("Next");
//            next.setOnClickListener(this);
//        } else if (data.equalsIgnoreCase("2")) {
//            next.setText("Select");
//            back.setVisibility(View.VISIBLE);
//            next.setOnClickListener(this);
       // }//next.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nxt:
                App.getAppPreferences().saveLanguage(SelectLangActivity.this, selectedlanguage);
                    Intent intent=new Intent(SelectLangActivity.this, LoginActivity.class);
                   // intent.putExtra("Langstatus",status);
                    startActivity(intent);
                    finish();

                break;

            case R.id.engbtn:
                status ="0";
                selectedlanguage = "en";
                english.setBackgroundResource(R.drawable.langbtn1);
                arabic.setBackgroundResource(R.drawable.lngbtn2);
                App.getAppPreferences().SaveString(ConstantData.LANG_STATUS,status);
                break;

            case R.id.arabicbtn:
                status="1";
                selectedlanguage = "ar";
                english.setBackgroundResource(R.drawable.lngbtn2);
                arabic.setBackgroundResource(R.drawable.langbtn1);
                App.getAppPreferences().SaveString(ConstantData.LANG_STATUS,status);
                break;
            case R.id.back1:

                break;

        }

    }
}
