package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Adapters.ChatAdapter;
import omninos.com.sai_salon.Modelclass.ChatModel;
import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.PojoClasses.ConversationMessagepojo;
import omninos.com.sai_salon.PojoClasses.GetAdminIdPojo;
import omninos.com.sai_salon.PojoClasses.SendMessagePojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class ChatApplication extends AppCompatActivity implements View.OnClickListener {
    RecyclerView chatrv;
    ImageView send,back,attachment;
    ChatAdapter adapter;
    private EditText input_msg;
    OffersViewModel offersViewModel;
    Timer timer;
    TimerTask timerTask;
    TextView chattx;
    String adminid;
    List<ConversationMessagepojo.MessageDetail> models=new ArrayList<>();
    Context context;
    Resources resources;
    MultipartBody.Part editImagePart;
    private String UserimagePath;
    private File photoFile;
    private Uri uri;
    private static final int GALLERY_REQUEST = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_application);

        initId();
        context = LocaleHelper.setLocale(ChatApplication.this, App.getAppPreferences().getLanguage(ChatApplication.this));
        resources = context.getResources();
        ChangeLanguage();
        setrecycle();
       // getadminid();
        
    }

    private void ChangeLanguage() {
        chattx.setText(resources.getString(R.string.chat));
        input_msg.setHint(resources.getString(R.string.type_a_message));
    }

//    private void getadminid() {
//        offersViewModel.getadminid(ChatApplication.this).observe(ChatApplication.this, new Observer<GetAdminIdPojo>() {
//            @Override
//            public void onChanged(@Nullable GetAdminIdPojo getAdminIdPojo) {
//                if (getAdminIdPojo.getSuccess().equalsIgnoreCase("1")) {
//                    adminid = getAdminIdPojo.getDetails().getId();
//                    startTimer();
//                }
//            }
//        });
//    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
        timerTask.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    private void startTimer() {

        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 2000, 5000); //
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                getConversationApi();

            }
        };
    }

    private void getConversationApi() {
        offersViewModel.converstaionmsg(ChatApplication.this, App.getAppPreferences().GetString(ConstantData.USER_ID), "1", "2").observe(ChatApplication.this, new Observer<ConversationMessagepojo>() {
            @Override
            public void onChanged(@Nullable ConversationMessagepojo conversationMessagepojo) {
                if (conversationMessagepojo.getSuccess().equalsIgnoreCase("1")) {
                    adapter = new ChatAdapter(ChatApplication.this, conversationMessagepojo.getMessageDetails());
                    int newMsgPosition = conversationMessagepojo.getMessageDetails().size() - 1;
                    adapter.notifyItemInserted(newMsgPosition);
                    chatrv.scrollToPosition(newMsgPosition);
                    chatrv.setAdapter(adapter);
                }

            }
        });
    }

    private void setrecycle() {

        chatrv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

    }

    private void initId() {
        offersViewModel= ViewModelProviders.of(ChatApplication.this).get(OffersViewModel.class);
        chatrv=findViewById(R.id.chatrv);
        send=findViewById(R.id.send);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        send.setOnClickListener(this);
        input_msg=findViewById(R.id.input_msg);
        chattx=findViewById(R.id.chattxt);
        attachment=findViewById(R.id.attachment);
        attachment.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.send:
                if (input_msg.getText().toString().isEmpty()){
                    Toast.makeText(context, "Enter something", Toast.LENGTH_SHORT).show();
                }else {
                    sendMessage(input_msg.getText().toString(),"1","");
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.attachment:
                galleryIntent();
                break;

        }

    }
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("profilePic/*");
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                //imagesalon.setImageURI(uri);
                sendMessage("","2",UserimagePath);
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            }
        }
    }


//    private void sendMessage(String ) {
//
//            offersViewModel.sendingmessage(ChatApplication.this, App.getAppPreferences().GetString(ConstantData.USER_ID), adminid, "provider", "2", data).observe(ChatApplication.this, new Observer<SendMessagePojo>() {
//                @Override
//                public void onChanged(@Nullable SendMessagePojo sendMessagePojo) {
//                    if (sendMessagePojo.getSuccess().equalsIgnoreCase("1")) {
//                        input_msg.setText("");
//
//                    }
//                }
//            });
//
//    }
private void sendMessage(String s, String text,String path) {


    RequestBody UseridR = RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));
    RequestBody adminidR = RequestBody.create(MediaType.parse("text/plain"), "1");
    RequestBody TypeR = RequestBody.create(MediaType.parse("text/plain"), "provider");
    RequestBody chattypeR = RequestBody.create(MediaType.parse("text/plain"), "2");
    RequestBody msgR = RequestBody.create(MediaType.parse("text/plain"), s);
    RequestBody msgType=RequestBody.create(MediaType.parse("text/plain"),text);


    if (!path.isEmpty()) {
        File file = new File(path);
        final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        editImagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
    } else {
        RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
        editImagePart = MultipartBody.Part.createFormData("image", "", attachmentEmpty);
    }


//        if (data.isEmpty()) {
//            Toast.makeText(this, "Enter Message", Toast.LENGTH_SHORT).show();
//        }
////
//        else {
    offersViewModel.sendingmessage(ChatApplication.this,UseridR, adminidR, TypeR, chattypeR, msgR,editImagePart,msgType).observe(ChatApplication.this, new Observer<SendMessagePojo>() {
        @Override
        public void onChanged(@Nullable SendMessagePojo sendMessagePojo) {
            if (sendMessagePojo.getSuccess().equalsIgnoreCase("1")) {
                input_msg.setText("");

            }
        }
    });
//        }
}
    }

