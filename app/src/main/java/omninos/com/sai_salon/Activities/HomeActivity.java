package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import omninos.com.sai_salon.Adapters.HomePageMainAdapter;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.OnlineOfflinestatus;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.AppPreferences;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    ImageView square,salonimage;
    TabLayout tabLayout;
    Switch switchbtn;
    TextView salonname;
    String status="0";
    ViewModelNext viewModelNext;
    TextView langtext;
    private DrawerLayout drawer;
    private RelativeLayout manageaddress, employees,timeslot,salonimages,logout, blog,home,lang,profile,homeservice,booking,notification,packages,contactus,About,terms,rating,share,holiday,cstm_services;
    private TextView homeaction,navi_hometv,navi_profiletv,navi_addtv,navi_servicestv,navi_timetv,navi_imagestv,navi_offertv,navi_employeetv,navi_bookingtv,navi_notifitv,navi_ratingstv,navi_customertv,navi_termstv,navi_abouttv,navi_sharetv,navi_logouttv;
    Context context;
    Resources resources;
    String statuss,selectedlanguage;
    ViewPager viewPager;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        context = LocaleHelper.setLocale(HomeActivity.this, App.getAppPreferences().getLanguage(HomeActivity.this));
        resources = context.getResources();


            initId();
            setUps();
            navigId();
        ChangeLanguage();

        if(App.getAppPreferences().getLanguage(HomeActivity.this).equalsIgnoreCase("en")){
            langtext.setText(resources.getString(R.string.arabiclang));
            //App.getAppPreferences().saveLanguage(HomeActivity.this,"ar");
            salonname.setText(App.getAppPreferences().GetString(ConstantData.SALONNAME));
        }

        else if (App.getAppPreferences().getLanguage(HomeActivity.this).equalsIgnoreCase("ar")) {
            langtext.setText(resources.getString(R.string.englishlng));
            salonname.setText(App.getAppPreferences().GetString(ConstantData.SALONNAME_ARABIC));
//
        }
            }

    private void ChangeLanguage() {
        homeaction.setText(resources.getString(R.string.home));
        navi_hometv.setText(resources.getString(R.string.home));
        navi_profiletv.setText(resources.getString(R.string.profile));
        navi_addtv.setText(resources.getString(R.string.manage_n_address));
        navi_servicestv.setText(resources.getString(R.string.services));
        navi_timetv.setText(resources.getString(R.string.timeslot));
        navi_imagestv.setText(resources.getString(R.string.salon_n_images));
        navi_offertv.setText(resources.getString(R.string.special_offers));
        navi_employeetv.setText(resources.getString(R.string.employees));
        navi_bookingtv.setText(resources.getString(R.string.booking));
        navi_notifitv.setText(resources.getString(R.string.notifications));
        navi_ratingstv.setText(resources.getString(R.string.ratings));
        navi_customertv.setText(resources.getString(R.string.customer_service));
        navi_termstv.setText(resources.getString(R.string.terms_conditions));
        navi_abouttv.setText(resources.getString(R.string.aboutname));
        navi_sharetv.setText(resources.getString(R.string.share));
        navi_logouttv.setText(resources.getString(R.string.logout));


    }

    private void checkstatus() {
        viewModelNext.getstatus(HomeActivity.this,App.getAppPreferences().GetString(ConstantData.USER_ID),status).observe(HomeActivity.this, new Observer<OnlineOfflinestatus>() {
            @Override
            public void onChanged(@Nullable OnlineOfflinestatus onlineOfflinestatus) {
                if(onlineOfflinestatus.getSuccess().equalsIgnoreCase("1")){

                   // Toast.makeText(HomeActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void navigId() {
        homeaction=findViewById(R.id.homeaction);
        salonimage=findViewById(R.id.salonimage);
        salonname=findViewById(R.id.salonname);
        Glide.with(HomeActivity.this).load(App.getAppPreferences().GetString(ConstantData.SALONIMAGE)).into(salonimage);

        booking=findViewById(R.id.booking);
        booking.setOnClickListener(this);
        About=findViewById(R.id.About);
        About.setOnClickListener(this);
        notification=findViewById(R.id.notification);
        notification.setOnClickListener(this);
        homeservice=findViewById(R.id.homeservice);
        homeservice.setOnClickListener(this);
        packages=findViewById(R.id.packages);
        packages.setOnClickListener(this);
        langtext=findViewById(R.id.langtxt);
        contactus=findViewById(R.id.contactus);
        contactus.setOnClickListener(this);
        terms=findViewById(R.id.terms);
        terms.setOnClickListener(this);
        share=findViewById(R.id.share);
        share.setOnClickListener(this);
        rating=findViewById(R.id.rating);
        rating.setOnClickListener(this);
        holiday=findViewById(R.id.holidays);
        holiday.setOnClickListener(this);
        cstm_services=findViewById(R.id.cstom_services);
        cstm_services.setOnClickListener(this);
        profile=findViewById(R.id.profile);
        profile.setOnClickListener(this);
        lang=findViewById(R.id.lang);
        lang.setOnClickListener(this);
        home=findViewById(R.id.home);
        home.setOnClickListener(this);
        blog=findViewById(R.id.blog);
        blog.setOnClickListener(this);
        logout=findViewById(R.id.Logout);
        logout.setOnClickListener(this);
        employees=findViewById(R.id.employees);
        employees.setOnClickListener(this);
        salonimages=findViewById(R.id.salonimages);
        salonimages.setOnClickListener(this);
        timeslot=findViewById(R.id.timeslot);
        timeslot.setOnClickListener(this);
        manageaddress=findViewById(R.id.manageaddress);
        manageaddress.setOnClickListener(this);
        navi_hometv=findViewById(R.id.navi_hometv);
        navi_profiletv=findViewById(R.id.navi_profiletv);
        navi_addtv=findViewById(R.id.navi_addresstv);
        navi_imagestv=findViewById(R.id.navi_imagestv);
        navi_servicestv=findViewById(R.id.navi_servicestv);
        navi_timetv=findViewById(R.id.navi_timeslottv);
        navi_offertv=findViewById(R.id.navi_offerstv);
        navi_employeetv=findViewById(R.id.navi_employeetv);
        navi_bookingtv=findViewById(R.id.navi_bookingtv);
        navi_notifitv=findViewById(R.id.navi_notitv);
        navi_ratingstv=findViewById(R.id.navi_ratingtv);
        navi_customertv=findViewById(R.id.navi_customertv);
        navi_termstv=findViewById(R.id.navi_termstv);
        navi_abouttv=findViewById(R.id.navi_abouttv);
        navi_sharetv=findViewById(R.id.navi_sharetv);
        navi_logouttv=findViewById(R.id.navi_logouttv);
    }


    private void setUps() {
        tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.pendingjobs)));
        tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.upcomingjobs)));

        HomePageMainAdapter adapter=new HomePageMainAdapter(this,getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
           @Override
           public void onTabSelected(TabLayout.Tab tab) {
               viewPager.setCurrentItem(tab.getPosition());
               Log.d( "onTabSelected: ", String.valueOf(tab.getPosition()));
           }

           @Override
           public void onTabUnselected(TabLayout.Tab tab) {

           }

           @Override
           public void onTabReselected(TabLayout.Tab tab) {

           }
       });
    }

    private void initId() {
        square=findViewById(R.id.square);
        square.setOnClickListener(this);
        builder=new AlertDialog.Builder(this);
        drawer=findViewById(R.id.drawer);
        switchbtn=findViewById(R.id.switchbtn);
        switchbtn.setOnCheckedChangeListener(HomeActivity.this);
        if(App.getAppPreferences().GetString(ConstantData.STATUS).equalsIgnoreCase("0")){
            switchbtn.setChecked(true);
        }
        else{
            switchbtn.setChecked(false);
        }
        viewPager=(ViewPager)findViewById(R.id.viewpager);
        tabLayout=(TabLayout)findViewById(R.id.tablayout);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.square:
                drawer.openDrawer(Gravity.START);
                break;
            case R.id.booking:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this, BookingActivity.class));
                break;

            case R.id.contactus:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,ContactusActivity.class));
                break;

            case R.id.homeservice:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,HomeservicesActivity.class));
                break;
            case R.id.notification:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,NotificationActivity.class));
                break;
            case R.id.packages:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,GetSpecialOfferActivity.class));
                break;
            case R.id.About:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,AboutActivity.class));
                break;
            case R.id.terms:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,TermsncondiActivity.class));
                break;
            case R.id.rating:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,RatingsActivity.class));
                break;
            case R.id.holidays:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this, CalenderActivity.class));
                break;
            case R.id.share:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,ShareActivity.class));
                break;
            case R.id.cstom_services:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,ChatApplication.class));
                break;
            case R.id.profile:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,BusinessProfileActivity.class));
                break;
            case R.id.home:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,HomeActivity.class));
                break;
            case R.id.blog:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(HomeActivity.this,BlogActivity.class));
                break;
            case R.id.Logout:
                drawer.closeDrawer(GravityCompat.START);
                App.getAppPreferences().Logout();
                startActivity(new Intent(HomeActivity.this,LoginActivity.class));
                finishAffinity();
                break;
            case R.id.lang:


//                Intent i=new Intent(HomeActivity.this,SelectLangActivity.class);
//                i.putExtra("Type","2");
//                startActivity(i);
                // startActivity(new Intent(HomeActivity.this,SelectLangActivity.class));
                dialogOpen();

                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.timeslot:
                startActivity(new Intent(HomeActivity.this,TimeslotActivity.class));
                break;
            case R.id.salonimages:
                startActivity(new Intent(HomeActivity.this,SalonimagesActivity.class));
                break;
            case R.id.employees:
                startActivity(new Intent(HomeActivity.this,GetEmployessActivty.class));
                break;
            case R.id.manageaddress:
                startActivity(new Intent(HomeActivity.this,ManageAddressActivity.class));
                break;


        }

    }

    private void dialogOpen() {
        builder. setTitle("").setMessage(resources.getString(R.string.changelangtxt))
                .setCancelable(false).setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(App.getAppPreferences().getLanguage(HomeActivity.this).equalsIgnoreCase("en")){
                    langtext.setText(resources.getString(R.string.arabiclang));
                    App.getAppPreferences().saveLanguage(HomeActivity.this,"ar");
                    startActivity(new Intent(HomeActivity.this,HomeActivity.class));
                    finishAffinity();
                }

                else if (App.getAppPreferences().getLanguage(HomeActivity.this).equalsIgnoreCase("ar")) {
                    langtext.setText(resources.getString(R.string.englishlng));
                    App.getAppPreferences().saveLanguage(HomeActivity.this,"en");
                    startActivity(new Intent(HomeActivity.this,HomeActivity.class));
                    finishAffinity();

                }

            }
        }).setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.show();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {

            switchbtn.setTrackDrawable(getResources().getDrawable(R.drawable.active_toggle));
            status="0";
            App.getAppPreferences().SaveString(ConstantData.STATUS,status);

            checkstatus();
            // search.setVisibility(View.VISIBLE);

        } else {
            switchbtn.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));
            status="1";
            App.getAppPreferences().SaveString(ConstantData.STATUS,status);
            checkstatus();
            //  search.setVisibility(View.GONE);
        }


    }

}
