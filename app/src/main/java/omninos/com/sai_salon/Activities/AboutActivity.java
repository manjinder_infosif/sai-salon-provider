package omninos.com.sai_salon.Activities;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class AboutActivity extends AppCompatActivity {
   private ImageView back;
   private WebView webView;
   TextView actiontxt;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        back=findViewById(R.id.back);
        webView=findViewById(R.id.webview);
        actiontxt=findViewById(R.id.actiontxt);
        context = LocaleHelper.setLocale(AboutActivity.this, App.getAppPreferences().getLanguage(AboutActivity.this));
        resources = context.getResources();
        ChangeLanguage();
        if(App.getAppPreferences().getLanguage(AboutActivity.this).equalsIgnoreCase("ar")){
            webView.loadUrl("https://www.saisalon.com/admincp/index.php/api/user/aboutUsArabic");
        }
        else {
            webView.loadUrl("https://www.saisalon.com/admincp/index.php/api/user/aboutUs");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }

    private void ChangeLanguage() {
        actiontxt.setText(resources.getString(R.string.aboutname));
    }
}
