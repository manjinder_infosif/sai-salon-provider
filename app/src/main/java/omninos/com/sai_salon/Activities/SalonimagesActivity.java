package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Adapters.PhotosAdapter;
import omninos.com.sai_salon.Adapters.SalonImagesAdapter;
import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.DeletePojo;
import omninos.com.sai_salon.PojoClasses.SalonimagesPojo;
import omninos.com.sai_salon.PojoClasses.UpdatePhotoPojooo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.Util.LocaleHelper;

public class SalonimagesActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView salonimgesrv;
    ImageView back;
    Dialog dialog;
    Button addphotos;
    SalonImagesAdapter adapter;
    ViewModelNext viewModelNext;
    private String UserimagePath;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private File photoFile;
    AlertDialog.Builder builder;
    MultipartBody.Part editImagePart;
    TextView salonimagestext;
    List<SalonimagesPojo.Detail>list=new ArrayList<>();
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salonimages);
        viewModelNext= ViewModelProviders.of(this).get(ViewModelNext.class);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        salonimagestext=findViewById(R.id.salonimagesaction);
        builder = new AlertDialog.Builder(this);
        salonimgesrv=findViewById(R.id.salonimgesrv);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,3,GridLayoutManager.VERTICAL,false);
        salonimgesrv.setLayoutManager(gridLayoutManager);
        addphotos=findViewById(R.id.addphotos);
        addphotos.setOnClickListener(this);
        getImages();
        context = LocaleHelper.setLocale(SalonimagesActivity.this, App.getAppPreferences().getLanguage(SalonimagesActivity.this));
        resources = context.getResources();
        ChangeLanguage();

    }

    private void ChangeLanguage() {
        salonimagestext.setText(resources.getString(R.string.salon_images));
        addphotos.setText(resources.getString(R.string.add_photos));
    }

    private void getImages() {
        viewModelNext.getimages(this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(this, new Observer<SalonimagesPojo>() {
            @Override
            public void onChanged(@Nullable final SalonimagesPojo salonimagesPojo) {
                if(salonimagesPojo.getSuccess().equalsIgnoreCase("1")){
                    list=salonimagesPojo.getDetails();
                    adapter=new SalonImagesAdapter(SalonimagesActivity.this, salonimagesPojo.getDetails(), new SalonImagesAdapter.OnClick() {
                        @Override
                        public void onImageclick(int position) {
                            String id = salonimagesPojo.getDetails().get(position).getId();
                            App.getSingletonPojo().setPhotoid(id);
                            OpenImageData();
                        }
                    }, new SalonImagesAdapter.Removephoto() {
                        @Override
                        public void remove(int position) {
                            String id = salonimagesPojo.getDetails().get(position).getId();
                            App.getSingletonPojo().setPhotoid(id);
                            Opendialog(position);
                        }
                    });
                    salonimgesrv.setAdapter(adapter);
                }
            }
        });
    }

    private void Opendialog(final int position) {
        builder. setTitle("").setMessage(resources.getString(R.string.deletephoto))
                .setCancelable(false).setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                   delete(position);


            }
        }).setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.show();

    }

    private void delete(final int position) {
        viewModelNext.deletephots(SalonimagesActivity.this,App.getSingletonPojo().getPhotoid()).observe(SalonimagesActivity.this, new Observer<DeletePojo>() {
            @Override
            public void onChanged(@Nullable DeletePojo deletePojo) {
                if(deletePojo.getSuccess().equalsIgnoreCase("1")){

                    list.remove(list.get(position));
                    adapter.notifyDataSetChanged();
                    getImages();
                    adapter.notifyDataSetChanged();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
               // startActivity(new Intent(SalonimagesActivity.this,SalonimagesActivity.class));
            break;
            case R.id.addphotos:
                startActivity(new Intent(SalonimagesActivity.this,PhotosActivity2.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SalonimagesActivity.this,HomeActivity.class));
    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(SalonimagesActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //choose from camera
    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(SalonimagesActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                // Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    //Choose from gallery
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {

                // set image path
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                //   App.getAppPreferences().SaveString(PhotosActivity.this, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(UserimagePath, options);
               // list.add(ImageUtil.compressImage(photoFile.getAbsolutePath()));
                updateImage(UserimagePath);
                salonimgesrv.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }
        }
    }

    private void updateImage(String UserimagePath) {
        RequestBody idbody=RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getPhotoid());

        if (UserimagePath != null) {
            File file = new File(UserimagePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            editImagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        } else {
            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
            editImagePart = MultipartBody.Part.createFormData("image", "", attachmentEmpty);
        }
        viewModelNext.updatephotos(SalonimagesActivity.this,idbody,editImagePart).observe(SalonimagesActivity.this, new Observer<UpdatePhotoPojooo>() {
            @Override
            public void onChanged(@Nullable UpdatePhotoPojooo updatePhotoPojooo) {
                if(updatePhotoPojooo.getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(SalonimagesActivity.this, ""+updatePhotoPojooo.getMessage(), Toast.LENGTH_SHORT).show();
                }
                getImages();
                adapter.notifyDataSetChanged();
            }
        });



    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
               // photolist.add(cursor.getString(column_index_data));
                updateImage(UserimagePath);
                salonimgesrv.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

        }
    }
}
