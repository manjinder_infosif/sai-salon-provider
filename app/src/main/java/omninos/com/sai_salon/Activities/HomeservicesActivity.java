package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.HomeServicesAdapter;
import omninos.com.sai_salon.MyViewModels.NewViewModel;
import omninos.com.sai_salon.MyViewModels.ServicesViewModel;
import omninos.com.sai_salon.PojoClasses.DeletePojo;
import omninos.com.sai_salon.PojoClasses.DeleteServicepojo;
import omninos.com.sai_salon.PojoClasses.GetHomeServiceListModel;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class HomeservicesActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private HomeServicesAdapter homeAdapter;
    private Button addnew;
    private TextView servicesaction;
    private ImageView back;
    private ServicesViewModel viewModel;
    NewViewModel newViewModel;
    AlertDialog.Builder builder;
    TextView text;
    Context context;
    Resources resources;
    private List<GetHomeServiceListModel.Detail> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeservices);

        viewModel = ViewModelProviders.of(this).get(ServicesViewModel.class);
        newViewModel=ViewModelProviders.of(this).get(NewViewModel.class);
        initId();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeservicesActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        context = LocaleHelper.setLocale(HomeservicesActivity.this, App.getAppPreferences().getLanguage(HomeservicesActivity.this));
        resources = context.getResources();
        ChangeLanguage();

    }

    private void ChangeLanguage() {
        servicesaction.setText(resources.getString(R.string.services));
        addnew.setText(resources.getString(R.string.add_more_services));
        text.setText(resources.getString(R.string.add_services_first));
    }

    private void getList() {

        if (list != null) {
            list.clear();
        }
        viewModel.getServices(HomeservicesActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(HomeservicesActivity.this, new Observer<GetHomeServiceListModel>() {
            @Override
            public void onChanged(@Nullable GetHomeServiceListModel getHomeServiceListModel) {
                if (getHomeServiceListModel.getSuccess().equalsIgnoreCase("1")) {

                    list = getHomeServiceListModel.getDetails();


                    homeAdapter = new HomeServicesAdapter(HomeservicesActivity.this, list, new HomeServicesAdapter.RemoveService() {
                        @Override
                        public void remove(int position) {
                            String serviceid=list.get(position).getId();
                            OpenDialog(serviceid,position);

                        }
                    });
                    recyclerView.setAdapter(homeAdapter);
                    text.setVisibility(View.GONE);

                } else {
                    recyclerView.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void OpenDialog(final String serviceid, final int position) {
        builder. setTitle("").setMessage(resources.getString(R.string.deleteservicestxt))
                .setCancelable(false).setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                deleteoffer(serviceid,position);


            }
        }).setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.show();
    }

    private void deleteoffer(String serviceid, final int position) {
        newViewModel.deleteservices(HomeservicesActivity.this,serviceid).observe(HomeservicesActivity.this, new Observer<DeleteServicepojo>() {
            @Override
            public void onChanged(@Nullable DeleteServicepojo deletePojo) {
                if(deletePojo.getSuccess().equalsIgnoreCase("1")){
                    list.remove(list.get(position));
                    homeAdapter.notifyDataSetChanged();
                    getList();
                    homeAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void initId() {
        recyclerView = findViewById(R.id.recyl);
        addnew = (Button) findViewById(R.id.addbtn_services);
        addnew.setOnClickListener(this);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        builder=new AlertDialog.Builder(this);
        text=findViewById(R.id.text);
        servicesaction=findViewById(R.id.srvicesaction);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addbtn_services:
                startActivity(new Intent(HomeservicesActivity.this, AddnewActivity.class));
                break;
            case R.id.back:
                onBackPressed();
                break;


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getList();

    }
}
