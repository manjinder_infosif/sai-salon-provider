package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.PojoClasses.AcceptRejectPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class AcceptRejectActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back, image;
    private Button accept,reject,done;
    String status;
    private TextView name, name2, email, appointmnt, address, services;
    private OffersViewModel offersViewModel;
    Context context;
    Resources resources;
    TextView nametv,emailtv,servicestv,appointtv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_reject);
        offersViewModel = ViewModelProviders.of(this).get(OffersViewModel.class);
        id();

        context = LocaleHelper.setLocale(AcceptRejectActivity.this, App.getAppPreferences().getLanguage(AcceptRejectActivity.this));
        resources = context.getResources();
        chooselang();
        if (getIntent().getStringExtra("From").toString().equalsIgnoreCase("Notification")) {

            Glide.with(AcceptRejectActivity.this).load(getIntent().getStringExtra("userImage")).into(image);
            name.setText(getIntent().getStringExtra("username"));
            name2.setText(getIntent().getStringExtra("username"));
            email.setText(getIntent().getStringExtra("usereamil"));
            appointmnt.setText(getIntent().getStringExtra("appointmentDate") + " , " + getIntent().getStringExtra("appointmenttime"));
            address.setText(getIntent().getStringExtra("userAddress"));
            services.setText(getIntent().getStringExtra("services"));
        } else {
            Glide.with(AcceptRejectActivity.this).load(getIntent().getStringExtra("userImage")).into(image);
            name.setText(getIntent().getStringExtra("username"));
            name2.setText(getIntent().getStringExtra("username"));
            email.setText(getIntent().getStringExtra("usereamil"));
            appointmnt.setText(getIntent().getStringExtra("appointmentDate") + " , " + getIntent().getStringExtra("appointmenttime"));
            address.setText(getIntent().getStringExtra("userAddress"));
            services.setText(getIntent().getStringExtra("services"));
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (App.getSingletonPojo().getAcceptrejectstatus() != null) {
            if (App.getSingletonPojo().getAcceptrejectstatus().equalsIgnoreCase("1") ) {
                accept.setVisibility(View.GONE);
                reject.setVisibility(View.GONE);
                done.setVisibility(View.VISIBLE);
            } else if (App.getSingletonPojo().getAcceptrejectstatus().equalsIgnoreCase("2")){

                accept.setVisibility(View.GONE);
                reject.setVisibility(View.GONE);
                done.setVisibility(View.GONE);
            }
            else
            {
                accept.setVisibility(View.VISIBLE);
                reject.setVisibility(View.VISIBLE);
                done.setVisibility(View.GONE);
            }
        }
        else{
            accept.setVisibility(View.VISIBLE);
            reject.setVisibility(View.VISIBLE);
            done.setVisibility(View.GONE);
        }
    }

    private void id() {
        back = findViewById(R.id.back);
        image = findViewById(R.id.image);
        name = findViewById(R.id.name);
        name2 = findViewById(R.id.name2);
        email = findViewById(R.id.email);
        appointmnt = findViewById(R.id.appointmnt);
        address = findViewById(R.id.address);
        services = findViewById(R.id.services);
        accept = findViewById(R.id.accept);
        accept.setOnClickListener(this);
        done = findViewById(R.id.done);
        done.setOnClickListener(this);
        reject = findViewById(R.id.reject);
        reject.setOnClickListener(this);
        nametv=findViewById(R.id.nametv);
        emailtv=findViewById(R.id.emailtv);
        servicestv=findViewById(R.id.servicetv);
        appointtv=findViewById(R.id.apoointtv);
        accept.setVisibility(View.VISIBLE);
        reject.setVisibility(View.VISIBLE);
        done.setVisibility(View.GONE);
    }

    private void chooselang() {
        nametv.setText(resources.getString(R.string.nametxt));
        emailtv.setText(resources.getString(R.string.email));
        servicestv.setText(resources.getString(R.string.services));
        appointtv.setText(resources.getString(R.string.appointment));
        accept.setText(resources.getString(R.string.accept));
        reject.setText(resources.getString(R.string.reject));
        done.setText(resources.getString(R.string.done));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reject:
                status="2";
                acceptreject();
                break;
            case R.id.accept:
                status="1";
                acceptreject();
                break;
            case R.id.done:
                status="3";
                acceptreject();
                break;

        }

    }

    private void acceptreject() {
        offersViewModel.getacpetrejectstatus(AcceptRejectActivity.this,status, App.getSingletonPojo().getBookingid()).observe(AcceptRejectActivity.this, new Observer<AcceptRejectPojo>() {
            @Override
            public void onChanged(@Nullable AcceptRejectPojo acceptRejectPojo) {
                if(acceptRejectPojo.getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(AcceptRejectActivity.this, ""+acceptRejectPojo.getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AcceptRejectActivity.this,HomeActivity.class));
                    finishAffinity();
                }
            }
        });
    }
}
