package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.RatingAdapter;
import omninos.com.sai_salon.MyViewModels.ServicesViewModel;
import omninos.com.sai_salon.PojoClasses.ShowRatingPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class RatingsActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView ratingrv;
    private ImageView back;
    ServicesViewModel viewModel;
    TextView ratingtxt,text;
    private List<ShowRatingPojo.Detail>list=new ArrayList<>();
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        viewModel= ViewModelProviders.of(this).get(ServicesViewModel.class);
        initID();
        context = LocaleHelper.setLocale(RatingsActivity.this, App.getAppPreferences().getLanguage(RatingsActivity.this));
        resources = context.getResources();
        ChangeLanguage();

        ratingrv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        viewModel.getratingofsalon(RatingsActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(this, new Observer<ShowRatingPojo>() {
            @Override
            public void onChanged(@Nullable ShowRatingPojo showRatingPojo) {
                if(showRatingPojo.getSuccess().equalsIgnoreCase("1")){
                    if(showRatingPojo.getDetails()!=null){
                        RatingAdapter adapter=new RatingAdapter(RatingsActivity.this,showRatingPojo.getDetails());
                        ratingrv.setAdapter(adapter);
                        text.setVisibility(View.GONE);
                    }
                }
                else{
                    text.setVisibility(View.VISIBLE);
                    ratingrv.setVisibility(View.GONE);
                }


            }
        });



    }

    private void ChangeLanguage() {
        ratingtxt.setText(resources.getString(R.string.ratings));
        text.setText(resources.getString(R.string.no_ratings));
    }

    private void initID() {
        ratingrv=findViewById(R.id.ratingrv);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);
        ratingtxt=findViewById(R.id.ratingtxt);
        text=findViewById(R.id.text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
               onBackPressed();
               break;
        }

    }
}
