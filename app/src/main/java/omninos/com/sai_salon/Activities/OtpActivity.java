//package ankii.mehra.com.sai_salon.Activities;
//
//import android.app.Activity;
//import android.arch.lifecycle.Observer;
//import android.arch.lifecycle.ViewModel;
//import android.arch.lifecycle.ViewModelProviders;
//import android.content.Context;
//import android.content.Intent;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import ankii.mehra.com.sai_salon.PojoClasses.OtpPojoClass;
//import ankii.mehra.com.sai_salon.R;
//import ankii.mehra.com.sai_salon.Util.App;
//import ankii.mehra.com.sai_salon.Util.ConstantData;
//import ankii.mehra.com.sai_salon.MyViewModels.ViewModell;
//
//public class OtpActivity extends AppCompatActivity {
//    Button otpbtn;
//    StringBuilder otpstr;
//    EditText num1, num2, num3, num4;
//    String ed1,ed2,ed3,ed4;
//    ViewModell viewModell;
//    Activity activity;
//    String userid;
//    String otptxt;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_otp);
//        initViews();
//
//        otpstr=new StringBuilder();
//
//        viewModell= ViewModelProviders.of(this).get(ViewModell.class);
//
//        otpbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                otpverify();
//
//
//            }
//        });
//
//    }
//
//    private void otpverify() {
//
//        otpstr.append(ed1+ed2+ed3+ed4);
//        otptxt=otpstr.toString();
////        int otpnum= Integer.parseInt(otptxt);
//      //  userid= App.getAppPreferences().GetString(OtpActivity.this, ConstantData.USER_ID);
//        String id1=App.getSingletonPojo().getuserId();
//        Toast.makeText(this, ""+otptxt, Toast.LENGTH_SHORT).show();
//
////        viewModell.otpverify(OtpActivity.this,id1,otptxt).observe(OtpActivity.this, new Observer<OtpPojoClass>() {
////            @Override
////            public void onChanged(@Nullable OtpPojoClass otpPojoClass) {
////                otpstr.delete(0,otpstr.length());
////                if(otpPojoClass.getSuccess().equalsIgnoreCase("1")){
////
////                    startActivity(new Intent(OtpActivity.this,Welcomscrn.class));
////
////                }
////                else{
////                    Toast.makeText(OtpActivity.this, ""+otpPojoClass.getMessage(), Toast.LENGTH_SHORT).show();
////                }
////
////            }
////        });
////    }
//
//    TextWatcher generalTextWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            if (num1.getText().toString().length() == 1) {
//                ed1 = num1.getText().toString().trim();
//                if (!ed1.equalsIgnoreCase("")) {
//                    num2.requestFocus();
//                }
//
//            }  if (num2.getText().toString().length() == 1) {
//                ed2 = num2.getText().toString().trim();
//                if (!ed2.equalsIgnoreCase("")) {
//                    num3.requestFocus();
//                }
//
//            } if (num3.getText().toString().length() ==1) {
//                ed3 = num3.getText().toString().trim();
//                if (!ed3.equalsIgnoreCase("")) {
//                    num4.requestFocus();
//                }
//            }  if (num4.getText().toString().length() == 1) {
//                ed4 = num4.getText().toString().trim();
//                if (!ed4.equalsIgnoreCase("")) {
//                    hideKeyboard(OtpActivity.this);
//                }
//
//            }
//        }
//    };
//
//    private void hideKeyboard(Activity activity) {
//        View view=activity.findViewById(android.R.id.content);
//        if(view!=null){
//            InputMethodManager imm=(InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
//        }
//
//    }
//
//    private void initViews() {
//        num1 = findViewById(R.id.firstNumberVerify);
//        num2 = findViewById(R.id.secondNumberVerify);
//        num3 = findViewById(R.id.thirdNumberVerify);
//        num4 = findViewById(R.id.forthNumberVerify);
//        num1.addTextChangedListener(generalTextWatcher);
//        num2.addTextChangedListener(generalTextWatcher);
//        num3.addTextChangedListener(generalTextWatcher);
//        num4.addTextChangedListener(generalTextWatcher);
//        otpbtn=(Button)findViewById(R.id.btn_submit);
//    }
//}
