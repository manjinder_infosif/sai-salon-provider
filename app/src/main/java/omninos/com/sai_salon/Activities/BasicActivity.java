package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.io.File;
import java.util.Calendar;

import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.MyViewModels.ViewModell;

public class BasicActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private ImageView back;
    private Button next;
    private String UserimagePath;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private File photoFile;
    private ViewModell viewModell;
    private AwesomeValidation awesomeValidation;
    private int hour, minutes1, count = 0, c2 = 0, value = 0;
    private ImageView uploadimage;
    private TextView license;
    private EditText motno, name, surname, email, location;
    private String motno1, name1, surname1, email1, location1, licensepath, salonphotopath, montxt1open, montxt1close, tuestxt2open, tuestxt2close, wedtxt3open, wedtxt3close, thurstxt4open, thurstxt4close, fritxt5open, fritxt5close, satext6open, satext6close, suntext7open, suntext7close;
    private TextView openingtime, closingtime, txt1open, txt1close, txt2open, txt2close, txt3open, txt3close, txt4open, txt4close, txt5open, txt5close, txt6open, txt6close, txt7open, txt7close;
    private Switch switchbtn1, switchbtn2, switchbtn3, switchbtn4, switchbtn5, switchbtn6, switchbtn7;

    private String format = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        initId();
        viewModell = ViewModelProviders.of(this).get(ViewModell.class);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.name, "[a-zA-Z\\s]+", R.string.err_name);
        awesomeValidation.addValidation(this, R.id.surname, "[a-zA-Z\\s]+", R.string.err_name);
        awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        saveData();

    }


    private void initId() {
        uploadimage = findViewById(R.id.uploadimage);
        uploadimage.setOnClickListener(this);
        motno = findViewById(R.id.motno);
        name = findViewById(R.id.name);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        location = findViewById(R.id.location);
        next = findViewById(R.id.next);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        license = findViewById(R.id.license);
        license.setOnClickListener(this);
        openingtime = findViewById(R.id.openingtime);
        closingtime = findViewById(R.id.closingtime);
        switchbtn1 = findViewById(R.id.switchbtn1);
        switchbtn2 = findViewById(R.id.switchbtn2);
        switchbtn3 = findViewById(R.id.switchbtn3);
        switchbtn4 = findViewById(R.id.switchbtn4);
        switchbtn5 = findViewById(R.id.switchbtn5);
        switchbtn6 = findViewById(R.id.switchbtn6);
        txt1open = findViewById(R.id.txt1open);
        txt1close = findViewById(R.id.txt1close);
        switchbtn7 = findViewById(R.id.switchbtn7);
        switchbtn1.setOnCheckedChangeListener(this);
        switchbtn2.setOnCheckedChangeListener(this);
        switchbtn3.setOnCheckedChangeListener(this);
        switchbtn4.setOnCheckedChangeListener(this);
        switchbtn5.setOnCheckedChangeListener(this);
        switchbtn6.setOnCheckedChangeListener(this);
        switchbtn7.setOnCheckedChangeListener(this);
        txt2open = findViewById(R.id.txt2open);
        txt2close = findViewById(R.id.txt2close);
        txt3open = findViewById(R.id.txt3open);
        txt3close = findViewById(R.id.txt3close);
        txt4open = findViewById(R.id.txt4open);
        txt4close = findViewById(R.id.txt4close);
        txt5open = findViewById(R.id.txt5open);
        txt5close = findViewById(R.id.txt5close);
        txt6open = findViewById(R.id.txt6open);
        txt6close = findViewById(R.id.txt6close);
        txt7open = findViewById(R.id.txt7open);
        txt7close = findViewById(R.id.txt7close);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.next:
                if (switchbtn1.isChecked() || switchbtn2.isChecked() || switchbtn3.isChecked() || switchbtn4.isChecked() || switchbtn5.isChecked() || switchbtn6.isChecked() || switchbtn7.isChecked()) {
                    providerinfo();
                } else {
                    Toast.makeText(this, "Click any switch button", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.license:
                count = 2;
                OpenImageData();
                break;
            case R.id.uploadimage:
                count = 1;
                OpenImageData();
                break;
            case R.id.txt1open:
                value = 1;
                PickTime();
                break;
            case R.id.txt1close:
                value = 2;
                PickTime();
                break;
            case R.id.txt2open:
                value = 3;
                PickTime();
                break;
            case R.id.txt2close:
                value = 4;
                PickTime();
                break;
            case R.id.txt3open:
                value = 5;
                PickTime();
                break;
            case R.id.txt3close:
                value = 6;
                PickTime();
                break;
            case R.id.txt4open:
                value = 7;
                PickTime();
                break;
            case R.id.txt4close:
                value = 8;
                PickTime();
                break;
            case R.id.txt5open:
                value = 9;
                PickTime();
                break;
            case R.id.txt5close:
                value = 10;
                PickTime();
                break;
            case R.id.txt6open:
                value = 11;
                PickTime();
                break;
            case R.id.txt6close:
                value = 12;
                PickTime();
                break;
            case R.id.txt7open:
                value = 13;
                PickTime();
                break;
            case R.id.txt7close:
                value = 14;
                PickTime();
                break;

        }
    }


    private void PickTime() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (hourOfDay == 0) {
                            hourOfDay += 12;
                            format = "AM";
                        } else if (hourOfDay == 12) {
                            format = "PM";
                        } else if (hourOfDay > 12) {
                            hourOfDay -= 12;
                            format = "PM";
                        } else {
                            format = "AM";
                        }


                        if (value == 1) {
                            txt1open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 2) {
                            txt1close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 3) {
                            txt2open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 4) {
                            txt2close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 5) {
                            txt3open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 6) {
                            txt3close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 7) {
                            txt4open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 8) {
                            txt4close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 9) {
                            txt5open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 10) {
                            txt5close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 11) {
                            txt6open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 12) {
                            txt6close.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 13) {
                            txt7open.setText(hourOfDay + ":" + minute + " " + format);
                        } else if (value == 14) {
                            txt7close.setText(hourOfDay + ":" + minute + " " + format);
                        }


                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switchbtn1:
                c2 = 1;
                if (isChecked) {
                    txt1open.setText("10:00 AM");
                    txt1close.setText("10:00 PM");
                    txt1open.setOnClickListener(this);
                    txt1close.setOnClickListener(this);
                } else {
                    txt1open.setText("Off");
                    txt1close.setText("Off");
                    txt1open.setOnClickListener(null);
                    txt1close.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn2:
                c2 = 2;
                if (isChecked) {
                    txt2open.setText("10:00 AM");
                    txt2close.setText("10:00 PM");
                    txt2close.setOnClickListener(this);
                    txt2open.setOnClickListener(this);
                } else {
                    txt2open.setText("Off");
                    txt2close.setText("Off");
                    txt2close.setOnClickListener(null);
                    txt2open.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn3:
                c2 = 3;
                if (isChecked) {
                    txt3open.setText("10:00 AM");
                    txt3close.setText("10:00 PM");
                    txt3close.setOnClickListener(this);
                    txt3open.setOnClickListener(this);
                } else {
                    txt3open.setText("Off");
                    txt3close.setText("Off");
                    txt3close.setOnClickListener(null);
                    txt3open.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn4:
                c2 = 4;
                if (isChecked) {
                    txt4open.setText("10:00 AM");
                    txt4close.setText("10:00 PM");
                    txt4close.setOnClickListener(this);
                    txt4open.setOnClickListener(this);
                } else {
                    txt4open.setText("Off");
                    txt4close.setText("Off");
                    txt4close.setOnClickListener(null);
                    txt4open.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn5:
                c2 = 5;
                if (isChecked) {
                    txt5open.setText("10:00 AM");
                    txt5close.setText("10:00 PM");
                    txt5close.setOnClickListener(this);
                    txt5open.setOnClickListener(this);
                } else {
                    txt5open.setText("Off");
                    txt5close.setText("Off");
                    txt5close.setOnClickListener(null);
                    txt5open.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn6:
                c2 = 6;
                if (isChecked) {
                    txt6open.setText("10:00 AM");
                    txt6close.setText("10:00 PM");
                    txt6close.setOnClickListener(this);
                    txt6open.setOnClickListener(this);
                } else {
                    txt6open.setText("Off");
                    txt6close.setText("Off");
                    txt6close.setOnClickListener(null);
                    txt6open.setOnClickListener(null);
                }
                break;
            case R.id.switchbtn7:
                c2 = 7;
                if (isChecked) {
                    txt7open.setText("10:00 AM");
                    txt7close.setText("10:00 PM");
                    txt7close.setOnClickListener(this);
                    txt7open.setOnClickListener(this);
                } else {
                    txt7close.setText("Off");
                    txt7open.setText("Off");
                    txt7close.setOnClickListener(null);
                    txt7open.setOnClickListener(null);
                }
                break;
        }
    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(BasicActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //choose from camera
    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(BasicActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                // Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    //Choose from gallery
    private void galleryIntent() {
        //gallery intent
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("profilePic/*");
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {

                // set image path
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                App.getAppPreferences().SaveString(ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(UserimagePath, options);
                if (count == 2) {
                    // String path= App.getAppPreferences().GetString(BasicActivity.this,UserimagePath);
                    license.setText(UserimagePath);
                } else if (count == 1) {
//                    Bitmap mphoto = (Bitmap) data.getExtras().get("data");
                    uploadimage.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                // UserimagePath = imagepath;
                if (count == 2) {
                    license.setText(UserimagePath);
                } else if (count == 1) {
                    //  Bitmap mphoto=(Bitmap)data.getExtras().get("data");
                    uploadimage.setImageURI(uri);
                }
//                App.getAppPreference().SaveString(RegisterActivity.this, ConstantData.IMAGEPATH, UserimagePath);
//                //set image preview on Image View
//
            }

        }
    }

    private void saveData() {
        salonphotopath = license.getText().toString();
        App.getSingletonPojo().setSalonphotopath(salonphotopath);
        motno1 = motno.getText().toString();
        App.getSingletonPojo().setMotno(motno1);
        name1 = name.getText().toString();
        App.getSingletonPojo().setName(name1);
        surname1 = surname.getText().toString();
        App.getSingletonPojo().setSurname(surname1);
        email1 = email.getText().toString();
        App.getSingletonPojo().setUseremail(email1);
        location1 = location.getText().toString();
        App.getSingletonPojo().setLocation(location1);
        licensepath = UserimagePath;
        App.getSingletonPojo().setLicensecopy(licensepath);
        montxt1open = txt1open.getText().toString();
//        App.getSingletonPojo().setMondayopen(montxt1open);
//        montxt1close = txt1close.getText().toString();
//        App.getSingletonPojo().setMondayclose(montxt1close);
//        tuestxt2open = txt2open.getText().toString();
//        App.getSingletonPojo().setTuesdayopen(tuestxt2open);
//        tuestxt2close = txt2close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(tuestxt2close);
//        wedtxt3open = txt3open.getText().toString();
//        App.getSingletonPojo().setTuesdayopen(wedtxt3open);
//        wedtxt3close = txt3close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(wedtxt3close);
//        thurstxt4open = txt4open.getText().toString();
//        App.getSingletonPojo().setTuesdayopen(thurstxt4open);
//        thurstxt4close = txt4close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(thurstxt4close);
//        fritxt5open = txt5open.getText().toString();
//        App.getSingletonPojo().setTuesdayopen(fritxt5open);
//        fritxt5close = txt5close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(fritxt5close);
//        satext6open = txt6open.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(satext6open);
//        satext6close = txt6close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(satext6close);
//        suntext7open = txt7open.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(suntext7open);
//        suntext7close = txt7close.getText().toString();
//        App.getSingletonPojo().setTuesdayclose(suntext7close);


    }

    private void providerinfo() {
        if (awesomeValidation.validate()) {
            startActivity(new Intent(BasicActivity.this, ServicessActivity.class));

        } else {
            Toast.makeText(this, "Please enter all credentials", Toast.LENGTH_SHORT).show();
        }
    }

}






























































































































































































































//    private void registeration() {
//
//        File file=new File(UserimagePath);
//        businessname=name.getText().toString();
//        passwd=password.getText().toString();
//        mail=email.getText().toString();
//        motno=motnumber.getText().toString();
//        confirmpaswd=confirmpasword.getText().toString();
//        phonee=phone.getText().toString();
//
//        RequestBody requestBody=RequestBody.create(MediaType.parse("multipart/form-data"),file);
//        MultipartBody.Part image=MultipartBody.Part.createFormData("image",file.getName(),requestBody);
//        RequestBody businessname1=RequestBody.create(MediaType.parse("text/plain"),businessname);
//        RequestBody passwd1=RequestBody.create(MediaType.parse("text/plain"),passwd);
//        RequestBody mail1=RequestBody.create(MediaType.parse("text/plain"),mail);
//        RequestBody motno1=RequestBody.create(MediaType.parse("text/plain"),motno);
//        //  RequestBody confirmpaswd1=RequestBody.create(MediaType.parse("text/plain"),confirmpaswd);
//        RequestBody phone1=RequestBody.create(MediaType.parse("text/plain"),phonee);
//        RequestBody devicetype1=RequestBody.create(MediaType.parse("text/plain"),device_type);
//        RequestBody regid=RequestBody.create(MediaType.parse("text/plain"),reg_id);
//        RequestBody logintype=RequestBody.create(MediaType.parse("text/plain"),login_type);
//
//        if(awesomeValidation.validate()) {
//            viewModell.register(BasicActivity.this,businessname1,mail1,phone1,passwd1,image,motno1
//                    ,devicetype1,regid,logintype)
//                    .observe(BasicActivity.this, new Observer<RegisterModelClass>() {
//
//                        @Override
//                        public void onChanged(@Nullable RegisterModelClass registerModelClass) {
//                            if(registerModelClass.getSuccess().equalsIgnoreCase("1")){
//                                //  App.getAppPreferences().SaveString(RegisterActivity.this, ConstantData.USER_ID,registerModelClass.getDetails().getId());
//                                App.getSingletonPojo().setuserId(registerModelClass.getDetails().getRegId());
//                                Toast.makeText(BasicActivity.this, ""+registerModelClass.getMessage(), Toast.LENGTH_SHORT).show();
//                                Toast.makeText(BasicActivity.this, ""+registerModelClass.getDetails().getOtp(), Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(BasicActivity.this,OtpActivity.class));
//                            }
//                            else{
//                                Toast.makeText(BasicActivity.this, ""+registerModelClass.getMessage(), Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//                    });
//
//        }
//        else{
//            Toast.makeText(this, "Invalid", Toast.LENGTH_SHORT).show();
//        }
//
//
//    }



