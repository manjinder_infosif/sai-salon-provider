package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.ServicesAdapter;
import omninos.com.sai_salon.PojoClasses.ServicesListpojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.Commonutil;
import omninos.com.sai_salon.MyViewModels.ViewModell;

public class ServicessActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView rvservices;
    private ImageView back;
    private Button next;
    private ViewModell viewModell;

    private List<ServicesListpojo>serviceslist=new ArrayList<>();
    private List<ServicesListpojo.Detail>detailList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicess);
        initId();
        viewModell= ViewModelProviders.of(this).get(ViewModell.class);
        recyview();

        App.getSingletonPojo().setServiceslist(serviceslist);
        display();
    }

    private void recyview() {

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,3,GridLayoutManager.VERTICAL,false);
        rvservices.setLayoutManager(gridLayoutManager);

    }

    private void initId() {
        rvservices=findViewById(R.id.rvservices);
        back=findViewById(R.id.back);
        next=findViewById(R.id.next);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.next:
                startActivity(new Intent(ServicessActivity.this,PhotosActivity2.class));
       //         display();

                break;
        }

    }

    private void display() {

        if (Commonutil.InternetCheck(ServicessActivity.this)) {
            viewModell.getservices(ServicessActivity.this).observe(ServicessActivity.this, new Observer<ServicesListpojo>() {
                @Override
                public void onChanged(@Nullable ServicesListpojo servicesListpojo) {
                    if (servicesListpojo.getSuccess().equalsIgnoreCase("1")) {
                        //    int count = servicesListpojo.getDetails().size();

                        int size = servicesListpojo.getDetails().size();
                        for (int i = 0; i < size; i++) {
                            ServicesListpojo model = new ServicesListpojo();
                            ServicesListpojo.Detail detail = new ServicesListpojo.Detail();
                            detail.setId(servicesListpojo.getDetails().get(i).getId());
                            detail.setImage(servicesListpojo.getDetails().get(i).getImage());
                            detail.setTitle(servicesListpojo.getDetails().get(i).getTitle());

                            detailList.add(detail);
                            model.setDetails(detailList);
                            serviceslist.add(model);

                        }
                        ServicesAdapter adapter = new ServicesAdapter(ServicessActivity.this, serviceslist);
                        rvservices.setAdapter(adapter);
                      //  Toast.makeText(ServicessActivity.this, "" + servicesListpojo.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ServicessActivity.this, "" + servicesListpojo.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }
        else{
            Toast.makeText(this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


}
