package omninos.com.sai_salon.Activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import omninos.com.sai_salon.Adapters.ViewPagerAdapter;
import omninos.com.sai_salon.R;
import me.relex.circleindicator.CircleIndicator;

public class WalkThroughActivity extends AppCompatActivity implements View.OnClickListener {
   private int[] layouts = { R.layout.fragment_walk_through2, R.layout.fragment_walk_through3, R.layout.fragment_walk_through4};
   private ViewPager viewPager;
   private CircleIndicator indicator;
   private ViewPagerAdapter mAdapter;
   private TextView leftTv, rightTv;
   private Timer timer;
   private Button getStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_through);
        initViews();
        mAdapter = new ViewPagerAdapter(layouts, WalkThroughActivity.this);
        viewPager.setAdapter(mAdapter);
        getStarted.setOnClickListener(this);
        indicator.setViewPager(viewPager);
        mAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPager.post(new Runnable() {
                    @Override
                    public void run() {
                        if(viewPager.getCurrentItem() == layouts.length-1){
                            viewPager.setCurrentItem(0);
                        }
                        else {
                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                        }
                    }
                });
            }
        };

        timer = new Timer();
        timer.schedule(timerTask,3000,3000);
    }

    private void initViews() {
        viewPager = findViewById(R.id.view_pager);
        indicator = findViewById(R.id.circle_indicator);
        getStarted=findViewById(R.id.getStarted);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.getStarted:
                Intent i=new Intent(WalkThroughActivity.this,SelectLangActivity.class);
                i.putExtra("Type","1");
                startActivity(i);

                //startActivity(new Intent(WalkThroughActivity.this,SelectLangActivity.class));
                finish();
        }

    }
}
