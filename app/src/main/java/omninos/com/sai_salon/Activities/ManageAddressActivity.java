package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import omninos.com.sai_salon.MyViewModels.NewViewModel;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.AddAddressPojo;
import omninos.com.sai_salon.PojoClasses.CheckaddresssPojo;
import omninos.com.sai_salon.PojoClasses.GetCityPojo;
import omninos.com.sai_salon.PojoClasses.GetCountryListPojo;
import omninos.com.sai_salon.PojoClasses.GetStatePojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class ManageAddressActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    ImageView back;
    GoogleMap map;
    Spinner countryspinner, statespinner, cityspinner;
    EditText pincode, address;
    String pincode_str, address_str, countrynameS;
    Button add_address;
    ViewModelNext viewModelNext;
    NewViewModel newViewModel;
    TextView manageaction,countrytv,statetv,citytv,pincodetv,addresstv;
    String countryId_str, stateid_str, cityid_str;
    String countryName, stateName, cityName;
    List<String> countryid = new ArrayList<>();
    List<String> countrynames = new ArrayList<>();
    List<String> statename = new ArrayList<>();
    List<String> stateId = new ArrayList<>();
    List<String> cityid = new ArrayList<>();
    List<String> cityname = new ArrayList<>();
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_address);
        viewModelNext = ViewModelProviders.of(this).get(ViewModelNext.class);
        newViewModel=ViewModelProviders.of(this).get(NewViewModel.class);
        id();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapHome);
        // assert mapFragment != null;
        mapFragment.getMapAsync(this);

        context = LocaleHelper.setLocale(ManageAddressActivity.this, App.getAppPreferences().getLanguage(ManageAddressActivity.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        addresstv.setText(resources.getString(R.string.address));
        pincodetv.setText(resources.getString(R.string.pincode));
        citytv.setText(resources.getString(R.string.city));
        statetv.setText(resources.getString(R.string.state));
        countrytv.setText(resources.getString(R.string.country));
        manageaction.setText(resources.getString(R.string.manage_address));
    }


    private void id() {
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        countryspinner = findViewById(R.id.countryspinner);
        statespinner = findViewById(R.id.statespinner);
        cityspinner = findViewById(R.id.cityspinner);
        pincode = findViewById(R.id.pincode);
        add_address = findViewById(R.id.addaddress);
        add_address.setOnClickListener(this);
        address = findViewById(R.id.address);
        manageaction=findViewById(R.id.manageaction);
        countrytv=findViewById(R.id.countrytv);
        statetv=findViewById(R.id.statetv);
        citytv=findViewById(R.id.citytv);
        pincodetv=findViewById(R.id.pincodetv);
        addresstv=findViewById(R.id.addresstv);

        getCountries();

    }

    private void getCountries() {
        if (countryid != null) {
            countryid.clear();
        }
        if (countrynames != null) {
            countrynames.clear();
        }
        viewModelNext.getcountry(ManageAddressActivity.this).observe(ManageAddressActivity.this, new Observer<GetCountryListPojo>() {
            @Override
            public void onChanged(@Nullable GetCountryListPojo getCountryListPojo) {
                if (getCountryListPojo.getSuccess().equalsIgnoreCase("1")) {
                    for (int i = 0; i < getCountryListPojo.getDetails().size(); i++) {
                        if (App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")) {
                            countrynames.add(getCountryListPojo.getDetails().get(i).getCountryArabicName());
                            countryid.add(getCountryListPojo.getDetails().get(i).getId());
                        }
                        else{
                            countrynames.add(getCountryListPojo.getDetails().get(i).getName());
                            countryid.add(getCountryListPojo.getDetails().get(i).getId());
                        }
                    }


                    ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(ManageAddressActivity.this, R.layout.spinner_item, countrynames); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    countryspinner.setAdapter(spinnerArrayAdapter1);

                    countryspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface = ResourcesCompat.getFont(ManageAddressActivity.this, R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);
                            countrynameS = parent.getItemAtPosition(position).toString();
                            countryId_str = countryid.get(position);
                            countryName = parent.getItemAtPosition(position).toString();

                            getstate();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });
    }

    private void getstate() {
        if (stateId != null) {
            stateId.clear();
        }
        if (statename != null) {
            statename.clear();
        }
        viewModelNext.getstates(ManageAddressActivity.this, countryId_str).observe(ManageAddressActivity.this, new Observer<GetStatePojo>() {
            @Override
            public void onChanged(@Nullable GetStatePojo getStatePojo) {
                if (getStatePojo.getSuccess().equalsIgnoreCase("1")) {
                    for (int i = 0; i < getStatePojo.getDetails().size(); i++) {
                        if (App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")) {
                            statename.add(getStatePojo.getDetails().get(i).getStateArabicName());
                            stateId.add(getStatePojo.getDetails().get(i).getId());
                        } else {
                            statename.add(getStatePojo.getDetails().get(i).getName());
                            stateId.add(getStatePojo.getDetails().get(i).getId());
                        }
                    }
                    ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(ManageAddressActivity.this, R.layout.spinner_item, statename); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    statespinner.setAdapter(spinnerArrayAdapter1);
                    statespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface = ResourcesCompat.getFont(ManageAddressActivity.this, R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);

                                stateName = parent.getItemAtPosition(position).toString();
                                stateid_str = stateId.get(position);

                            getCities();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
            }
        });
    }

    private void getCities() {
        if (cityid != null) {
            cityid.clear();

        }
        if (cityname != null) {
            cityname.clear();
        }
        viewModelNext.getcity(ManageAddressActivity.this, stateid_str).observe(ManageAddressActivity.this, new Observer<GetCityPojo>() {
            @Override
            public void onChanged(@Nullable GetCityPojo getCityPojo) {
                if (getCityPojo.getSuccess().equalsIgnoreCase("1")) {
                    for (int i = 0; i < getCityPojo.getDetails().size(); i++) {
                        if (App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")) {
                            cityname.add(getCityPojo.getDetails().get(i).getCityArabicName());
                            cityid.add(getCityPojo.getDetails().get(i).getId());
                        }
                        else{
                            cityname.add(getCityPojo.getDetails().get(i).getName());
                            cityid.add(getCityPojo.getDetails().get(i).getId());
                        }
                    }
                    ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(ManageAddressActivity.this, R.layout.spinner_item, cityname); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    cityspinner.setAdapter(spinnerArrayAdapter1);
                    cityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface = ResourcesCompat.getFont(ManageAddressActivity.this, R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);

                            cityid_str = cityid.get(position);

                            LatLng latLng=getLocationFromAddress(ManageAddressActivity.this, parent.getItemAtPosition(position).toString() + ","+stateName+","+countryName);


                            MarkerOptions markerOptions = new MarkerOptions();
                            markerOptions.position(latLng);
                            markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                            map.clear();
                            map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                            map.addMarker(markerOptions);
                            float zoomLevel = 16.0f; //This goes up to 21
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
                            // Create location object
//                            Location location = new Location(latLng.latitude, latLng.longitude);
//                             add location to SQLite database
//                            locationsDB.insert(location);
                            getdetails();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.addaddress:
                adddata();
                break;
        }
    }

    private void adddata() {
        address_str = address.getText().toString();
        pincode_str = pincode.getText().toString();
        if (address_str.isEmpty()) {
            Toast.makeText(this, "Enter Valid address", Toast.LENGTH_SHORT).show();
        } else if (pincode_str.isEmpty()) {
            Toast.makeText(this, "Enter Pincode", Toast.LENGTH_SHORT).show();
        } else {
            newViewModel.add_addres(ManageAddressActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID), address_str, cityid_str, pincode_str, stateid_str, countryId_str).observe(ManageAddressActivity.this, new Observer<AddAddressPojo>() {
                @Override
                public void onChanged(@Nullable AddAddressPojo addAddressPojo) {
                    if (addAddressPojo.getSuccess().equalsIgnoreCase("1")) {
                        Toast.makeText(ManageAddressActivity.this, "" + addAddressPojo.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }


    private void getdetails() {
        newViewModel.checkaddress(ManageAddressActivity.this,App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(ManageAddressActivity.this, new Observer<CheckaddresssPojo>() {
            @Override
            public void onChanged(@Nullable CheckaddresssPojo checkaddresssPojo) {
                if(checkaddresssPojo.getSuccess().equalsIgnoreCase("1")){
                    add_address.setText(resources.getString(R.string.updateadress));
                    address.setText(checkaddresssPojo.getDetails().getAddress());
                    pincode.setText(checkaddresssPojo.getDetails().getPin());
                   // countryspinner.setSelection(0, Boolean.parseBoolean(checkaddresssPojo.getDetails().getCountryid().toString()));


                    for (int j = 0; j < countrynames.size(); j++) {
                        if(App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")) {
                            if (countrynames.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getCountryArabicName())) {
                                countryspinner.setSelection(j, true);
                                String S_countryname = countryspinner.getSelectedItem().toString();
                            }
                        }
                        else{
                            if (countrynames.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getCountryName())) {
                                countryspinner.setSelection(j, true);
                                String S_countryname = countryspinner.getSelectedItem().toString();
                            }
                        }
                    }


                    for (int j = 0; j < statename.size(); j++) {
                        if(App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")){
                            if (statename.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getStateNameArabic())) {
                                statespinner.setSelection(j, true);
                                String S_statename = statespinner.getSelectedItem().toString();
                            }
                        }
                        else {
                            if (statename.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getStateName())) {
                                statespinner.setSelection(j, true);
                                String S_statename = statespinner.getSelectedItem().toString();
                            }
                        }
                    }
                    for (int j = 0; j < cityname.size(); j++) {
                        if(App.getAppPreferences().getLanguage(ManageAddressActivity.this).equalsIgnoreCase("ar")) {
                            if (cityname.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getCityNameArabic())) {
                                cityspinner.setSelection(j, true);
                                String S_cityname = statespinner.getSelectedItem().toString();
                            }
                        }
                        else{
                            if (cityname.get(j).equalsIgnoreCase(checkaddresssPojo.getDetails().getCityName())) {
                                cityspinner.setSelection(j, true);
                                String S_cityname = statespinner.getSelectedItem().toString();
                            }
                        }
                    }
                }
                else{
                    add_address.setText(resources.getString(R.string.add_address));
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
            System.out.println("Address: "+location.getLatitude());
            System.out.println("Address: "+location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }
}
