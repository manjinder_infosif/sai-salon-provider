package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Adapters.PackageAdapter1;
import omninos.com.sai_salon.Adapters.PackageServiceAdapter;
import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.MyViewModels.ServicesViewModel;
import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.GetServiceIDPojo;
import omninos.com.sai_salon.PojoClasses.GetServicesnamelist;
import omninos.com.sai_salon.PojoClasses.ProviderSpecialOffersPojo;
import omninos.com.sai_salon.PojoClasses.SalonSpecialOfferspojo;
import omninos.com.sai_salon.PojoClasses.Servicestypesspojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import omninos.com.sai_salon.Util.LocaleHelper;

public class PackageActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private RecyclerView rv1;
    private ImageView leftarrow;
    private OffersViewModel offersViewModel;
    Spinner servicetypesp, servicelocationsp, servicenamesp;
    EditText offertitle, offerprice, Arabictitleoffer, offerpercent;
    String offertitle_str, offerprice_str, salonTypeStatus, salonlocation, servicesstr, serviceID, arabicoffertitle_str, pricestr, offerpercent_str;
    ImageView image;
    ServicesViewModel viewModel;
    ViewModelNext viewModelNext;
    CheckBox checkbox2, checkbox1;
    Button addoffers;
    PackageServiceAdapter adapter;
    RecyclerView servicesrv;
    List<ProviderSpecialOffersPojo.Detail> details = new ArrayList<>();
    List<String> services = new ArrayList<>();
    List<String> serviceId = new ArrayList<>();
    List<String> servicetype = new ArrayList<>();
    List<String> servicesname = new ArrayList<>();
    List<String> servicesssId = new ArrayList<>();
    private String CategoryPositionId, UserimagePath = "";
    private File photoFile;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    String Ids, startdatestr = "", enddatestr = "";
    int count = 0;
    TextView startdate, enddate, price;
    TextView actiontext, servicetypetxt, locationtxt, nametxt, pricetxt, engoffertxt, arabicoffertxt, percnttxt, starttxt, endtxt, imagetxt, offerpricetv;
    private StringBuilder stringBuilder;
    private MultipartBody.Part editImagePart;
    Context context;
    Resources resources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);
        offersViewModel = ViewModelProviders.of(this).get(OffersViewModel.class);
        viewModel = ViewModelProviders.of(this).get(ServicesViewModel.class);
        viewModelNext = ViewModelProviders.of(this).get(ViewModelNext.class);

        initId();
        context = LocaleHelper.setLocale(PackageActivity.this, App.getAppPreferences().getLanguage(PackageActivity.this));
        resources = context.getResources();
        ChangeLanguage();

        //rv1.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        //  getoffers();


        //  LoadList("salon");


        getservicetype();


    }

    private void ChangeLanguage() {
        actiontext.setText(resources.getString(R.string.add_special_offers));
        servicetypetxt.setText(resources.getString(R.string.service_type));
        locationtxt.setText(resources.getString(R.string.service_location));
        nametxt.setText(resources.getString(R.string.service_name));
        pricetxt.setText(resources.getString(R.string.total_price));
        engoffertxt.setText(resources.getString(R.string.english_offer_name));
        arabicoffertxt.setText(resources.getString(R.string.arabic_offer_name));
        percnttxt.setText(resources.getString(R.string.offer_percentage_upto_100));
        starttxt.setText(resources.getString(R.string.offer_start_date));
        endtxt.setText(resources.getString(R.string.offer_end_date));
        imagetxt.setText(resources.getString(R.string.offerimage));
        addoffers.setText(resources.getString(R.string.add_offers));
        offerpricetv.setText(resources.getString(R.string.offer_price));

    }

    private void getdata(final String salonTypeStatus) {


        List<String> myArraySpinner = new ArrayList<String>();
//
        myArraySpinner.add(resources.getString(R.string.Salon));
        myArraySpinner.add(resources.getString(R.string.home));
        //  myArraySpinner.add(resources.getString(R.string.both));

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PackageActivity.this, R.layout.spinner_item, myArraySpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicelocationsp.setAdapter(arrayAdapter);
//
//        LoadList(servicelocationsp.getSelectedItem().toString(), salonTypeStatus);
        if (App.getAppPreferences().getLanguage(PackageActivity.this).equalsIgnoreCase("ar")) {
            salonlocation = "salon";
        } else {
            salonlocation = servicelocationsp.getSelectedItem().toString();
        }


        servicelocationsp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Typeface typeface = ResourcesCompat.getFont(PackageActivity.this, R.font.raleway_medium);
                ((TextView) view).setTypeface(typeface);
                if (App.getAppPreferences().getLanguage(PackageActivity.this).equalsIgnoreCase("ar")) {
                    if (position == 0) {
                        salonlocation = "salon";
                    } else if (position == 1) {
                        salonlocation = "home";
                    }
                } else {
                    salonlocation = parent.getItemAtPosition(position).toString();
                }
                LoadList(salonlocation, salonTypeStatus);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

//                LoadList(salonlocation);
            }

        });

    }

    private void getservicetype() {
        viewModel.getservicestypes(PackageActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(PackageActivity.this, new Observer<Servicestypesspojo>() {
            @Override
            public void onChanged(@Nullable Servicestypesspojo servicestypesspojo) {
                if (servicestypesspojo.getSuccess().equalsIgnoreCase("1")) {
                    if (servicestypesspojo.getDetails().getKidsStatus().equalsIgnoreCase("1")) {
                        servicetype.add(resources.getString(R.string.kids));
                    }
                    if (servicestypesspojo.getDetails().getManStatus().equalsIgnoreCase("1")) {
                        servicetype.add(resources.getString(R.string.men));
                    }
                    if (servicestypesspojo.getDetails().getWomanStatus().equalsIgnoreCase("1")) {
                        servicetype.add(resources.getString(R.string.women));
                    }


                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PackageActivity.this, R.layout.spinner_item, servicetype);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    servicetypesp.setAdapter(arrayAdapter);
                    servicetypesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface = ResourcesCompat.getFont(PackageActivity.this, R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);
                            if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.men))) {
                                salonTypeStatus = "2";

                                getdata(salonTypeStatus);

                                // LoadList("2");
                            } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.women))) {
                                salonTypeStatus = "1";

                                getdata(salonTypeStatus);

                                //  LoadList("1");
                            } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.kids))) {
                                salonTypeStatus = "3";

                                // LoadList("3");

                                getdata(salonTypeStatus);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }


            }
        });


    }

    private void LoadList(String salonlocation, String salonTypeStatus) {
        if (services != null) {
            services.clear();
        }
        if (servicesssId != null) {
            servicesssId.clear();
        }
        if (servicesname != null) {
            servicesname.clear();
        }
        if (servicesssId != null) {
            servicesssId.clear();
        }
        viewModelNext.getserviceslist(PackageActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID), salonTypeStatus, salonlocation).observe(PackageActivity.this, new Observer<GetServicesnamelist>() {
            @Override
            public void onChanged(@Nullable final GetServicesnamelist getServicesnamelist) {
                if (getServicesnamelist.getSuccess().equalsIgnoreCase("1")) {
                    if (services != null) {
                        services.clear();
                    }
                    if (servicesname != null) {
                        servicesname.clear();
                    }
                    for (int i = 0; i < getServicesnamelist.getHolidays().size(); i++) {
                        if (App.getAppPreferences().getLanguage(PackageActivity.this).equalsIgnoreCase("ar")) {
                            services.add(getServicesnamelist.getHolidays().get(i).getArabicTitle());
                            serviceId.add(getServicesnamelist.getHolidays().get(i).getId());
                        } else {
                            services.add(getServicesnamelist.getHolidays().get(i).getTitle());
                            serviceId.add(getServicesnamelist.getHolidays().get(i).getId());
                        }
                    }

                    ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(PackageActivity.this, R.layout.spinner_item, services);
                    arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    servicenamesp.setAdapter(arrayAdapter1);
                    servicenamesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface = ResourcesCompat.getFont(PackageActivity.this, R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);

                            servicesstr = parent.getItemAtPosition(position).toString();
                            serviceID = serviceId.get(position);


                            if (servicesname.contains(position)) {
                                Toast.makeText(PackageActivity.this, "Already exist", Toast.LENGTH_SHORT).show();
                            } else {
                                servicesname.add(servicesstr);
                                servicesssId.add(serviceID);
                            }

                            getIds();

                            adapter = new PackageServiceAdapter(PackageActivity.this, servicesname, new PackageServiceAdapter.OnCancelClick() {
                                @Override
                                public void onclick(int position) {
                                    servicesname.remove(servicesname.get(position));
                                    servicesssId.remove(servicesssId.get(position));
                                    adapter.notifyDataSetChanged();
                                    getIds();
                                    adapter.notifyDataSetChanged();
                                }
                            });
                            servicesrv.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    Toast.makeText(PackageActivity.this, "No Services available", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getIds() {
        try {
            stringBuilder = new StringBuilder();
            for (String s : servicesssId) {
                stringBuilder.append(s + ",");
            }
            int size1 = stringBuilder.length();
            stringBuilder.deleteCharAt(size1 - 1);
            System.out.println("Data Is: " + stringBuilder);
            Ids = stringBuilder.toString();
            System.out.println("Data Sys: " + Ids);
            getservicetotalprices();


        } catch (Exception e) {
            price.setText("0");
            pricestr = "0";
            stringBuilder = new StringBuilder();
        }
    }

    private void getservicetotalprices() {
        viewModelNext.getservieid(PackageActivity.this, Ids).observe(PackageActivity.this, new Observer<GetServiceIDPojo>() {
            @Override
            public void onChanged(@Nullable GetServiceIDPojo getServiceIDPojo) {
                if (getServiceIDPojo.getSuccess().equalsIgnoreCase("1")) {
                    pricestr = String.valueOf(getServiceIDPojo.getTotalPrice());
                    price.setText(pricestr);
                }
                else{
                    price.setText(" ");
                }
            }
        });
    }

//    private void getoffers() {
//        offersViewModel.getoffers(this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(PackageActivity.this, new Observer<ProviderSpecialOffersPojo>() {
//            @Override
//            public void onChanged(@Nullable ProviderSpecialOffersPojo providerSpecialOffersPojo) {
//                if(providerSpecialOffersPojo.getSuccess().equalsIgnoreCase("1")){
//                    PackageAdapter1 adapter1=new PackageAdapter1(PackageActivity.this,providerSpecialOffersPojo.getDetails());
//                    rv1.setAdapter(adapter1);
//                  //  Toast.makeText(PackageActivity.this, ""+providerSpecialOffersPojo.getDetails(), Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Toast.makeText(PackageActivity.this, ""+providerSpecialOffersPojo.getDetails(), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//    }

    private void initId() {
        // rv1=findViewById(R.id.rv1);

        leftarrow = findViewById(R.id.arrow);
        leftarrow.setOnClickListener(this);
        image = findViewById(R.id.image);
        image.setOnClickListener(this);
        servicelocationsp = findViewById(R.id.servicelocationspinner);
        servicenamesp = findViewById(R.id.servicenamespinner);
        servicetypesp = findViewById(R.id.servicetypespinner);
        offertitle = findViewById(R.id.titleoffer);
        offerprice = findViewById(R.id.offerprice);
        addoffers = findViewById(R.id.addoffers);
        addoffers.setOnClickListener(this);
        servicesrv = findViewById(R.id.servicesrv);
        startdate = findViewById(R.id.startdate);
        startdate.setOnClickListener(this);
        enddate = findViewById(R.id.enddate);
        price = findViewById(R.id.price);
        enddate.setOnClickListener(this);
        Arabictitleoffer = findViewById(R.id.Arabictitleoffer);
        actiontext = findViewById(R.id.actiontext);
        servicetypetxt = findViewById(R.id.servicetypetxt);
        locationtxt = findViewById(R.id.serviceloctext);
        nametxt = findViewById(R.id.servicenametxt);
        pricetxt = findViewById(R.id.pricetxt);
        engoffertxt = findViewById(R.id.englishoffertxt);
        arabicoffertxt = findViewById(R.id.arabicoffertxt);
        percnttxt = findViewById(R.id.offerpercenttext);
        starttxt = findViewById(R.id.startdatetxt);
        endtxt = findViewById(R.id.enddatetxt);
        offerpercent = findViewById(R.id.offerpercent);
        checkbox2 = findViewById(R.id.checkbox2);
        checkbox1 = findViewById(R.id.checkbox1);
        checkbox1.setOnCheckedChangeListener(this);
        checkbox2.setOnCheckedChangeListener(this);
        imagetxt = findViewById(R.id.imagetxt);
        offerpricetv = findViewById(R.id.offerpricetv);
        servicesrv.setLayoutManager(new LinearLayoutManager(PackageActivity.this, LinearLayoutManager.HORIZONTAL, false));
        //setups();


    }

//    private void setups() {
//        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    checkbox1.setEnabled(false);
//                    offerpercent.setFocusable(false);
//                    offerprice.setFocusable(true);
//                }
//                else{
//                    checkbox1.setEnabled(true);
//                    offerpercent.setFocusable(true);
//                }
//            }
//        });
//
//        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    checkbox2.setEnabled(false);
//                    offerprice.setFocusable(false);
//                    offerpercent.setFocusable(true);
//                }
//                else{
//                    checkbox2.setEnabled(true);
//                    offerpercent.setFocusable(true);
//                }
//            }
//        });
//
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.arrow:
                onBackPressed();
                break;
            case R.id.addoffers:
//                getIds();
                apihit();
                break;
            case R.id.image:
                OpenImageData();
                break;
            case R.id.startdate:
                count = 1;
                PickDate();
                break;
            case R.id.enddate:
                count = 2;
                PickDate();
                break;
        }

    }


    private void PickDate() {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                PackageActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);


                String date_str = sdf.format(mcurrentDate.getTime());
                if (date_str != null) {
                    if (count == 1) {
                        startdatestr = date_str;
                        startdate.setText(startdatestr);
                    } else if (count == 2) {
                        enddatestr = date_str;
                        enddate.setText(enddatestr);
                    }
                }


            }
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.setTitle("Select Date for Booking");
        mDatePicker.show();
    }

    private void apihit() {
        offertitle_str = offertitle.getText().toString();
        offerprice_str = offerprice.getText().toString();
        arabicoffertitle_str = Arabictitleoffer.getText().toString();
        offerpercent_str = offerpercent.getText().toString();

        if (startdatestr.isEmpty()) {
            Toast.makeText(context, "choose offer start date", Toast.LENGTH_SHORT).show();
        } else if (enddatestr.isEmpty()) {
            Toast.makeText(context, "choose offer end date", Toast.LENGTH_SHORT).show();
        } else {
            RequestBody pricebody = RequestBody.create(MediaType.parse("text/plain"), offerprice_str);
            RequestBody titlebody = RequestBody.create(MediaType.parse("text/plain"), offertitle_str);
            RequestBody arabictitlebody = RequestBody.create(MediaType.parse("text/plain"), arabicoffertitle_str);
            RequestBody servicenamebody = RequestBody.create(MediaType.parse("text/plain"), Ids);
            RequestBody servicetypebody = RequestBody.create(MediaType.parse("text/plain"), salonTypeStatus);
            RequestBody servicelocbody = RequestBody.create(MediaType.parse("text/plain"), salonlocation);
            RequestBody startdatebody = RequestBody.create(MediaType.parse("text/plain"), startdatestr);
            RequestBody enddatebody = RequestBody.create(MediaType.parse("text/plain"), enddatestr);
            RequestBody totalpricebody = RequestBody.create(MediaType.parse("text/plain"), pricestr);
            RequestBody percentbody = RequestBody.create(MediaType.parse("text/plain"), offerpercent_str);

            RequestBody providerbody = RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));

            if (UserimagePath != null) {
                File file = new File(UserimagePath);
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                editImagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            } else {
                RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
                editImagePart = MultipartBody.Part.createFormData("image", "", attachmentEmpty);
            }

            viewModelNext.getspecialoffers(PackageActivity.this, providerbody, servicenamebody, servicetypebody, servicelocbody, titlebody, percentbody, editImagePart, startdatebody, enddatebody, arabictitlebody, totalpricebody, pricebody).observe(PackageActivity.this, new Observer<SalonSpecialOfferspojo>() {
                @Override
                public void onChanged(@Nullable SalonSpecialOfferspojo salonSpecialOfferspojo) {
                    if (salonSpecialOfferspojo.getSuccess().equalsIgnoreCase("1")) {

                        Toast.makeText(PackageActivity.this, "" + salonSpecialOfferspojo.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(PackageActivity.this, GetSpecialOfferActivity.class));
                    }
                }
            });
        }
    }


    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(PackageActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(PackageActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
//                App.getAppPreference().SaveString(activity, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
//                userImage.setImageBitmap(mBitmapInsurance);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotated = Bitmap.createBitmap(mBitmapInsurance, 0, 0, mBitmapInsurance.getWidth(), mBitmapInsurance.getHeight(),
                        matrix, true);

                if (Build.VERSION.SDK_INT > 25 && Build.VERSION.SDK_INT < 27) {
                    // Do something for lollipop and above versions
                    image.setImageBitmap(rotated);
                } else {
                    // do something for phones running an SDK before lollipop
                    image.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
//                App.getAppPreference().SaveString(activity, ConstantData.IMAGEPATH, UserimagePath);
                Glide.with(PackageActivity.this).load("file://" + UserimagePath).into(image);
            }

        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (checkbox2.isChecked()) {
            checkbox1.setEnabled(false);
            offerpercent.setFocusable(false);
            offerpercent.setText("");
            offerprice.setCursorVisible(false);
            offerprice.setFocusableInTouchMode(true);

        } else if (checkbox1.isChecked()) {
            checkbox2.setEnabled(false);
            offerprice.setFocusable(false);
            offerprice.setFocusableInTouchMode(false);
            offerpercent.setCursorVisible(false);
            offerpercent.setFocusableInTouchMode(true);
            offerprice.setText("");
        } else {
            checkbox2.setEnabled(true);
            checkbox1.setEnabled(true);
            offerpercent.setFocusableInTouchMode(false);
            offerprice.setFocusableInTouchMode(false);
            offerpercent.setFocusable(false);
            offerprice.setFocusable(false);
        }
    }
}
