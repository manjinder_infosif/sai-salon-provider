package omninos.com.sai_salon.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class Welcomscrn extends AppCompatActivity {
    private Button done;
    private TextView tv2,tv3,tv4,welcometxt;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcomscrn);
        done=(Button)findViewById(R.id.done);
        tv2=findViewById(R.id.tv2);
        tv3=findViewById(R.id.tv3);
        tv4=findViewById(R.id.tv4);
        welcometxt=findViewById(R.id.welcometxt);
        context = LocaleHelper.setLocale(Welcomscrn.this, App.getAppPreferences().getLanguage(Welcomscrn.this));
        resources = context.getResources();
        changelanguage();

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Welcomscrn.this,HomeActivity.class));
                finishAffinity();
            }
        });
    }

    private void changelanguage() {
        done.setText(resources.getString(R.string.done));
        welcometxt.setText(resources.getString(R.string.welcome));
        tv2.setText(resources.getString(R.string.lines));
        tv3.setText(resources.getString(R.string.congratulations));
        tv4.setText(resources.getString(R.string.your_account_has_been_successfully_verified));
    }
}
