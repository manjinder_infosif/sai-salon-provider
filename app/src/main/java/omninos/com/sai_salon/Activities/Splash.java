package omninos.com.sai_salon.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;

public class Splash extends AppCompatActivity {
    private Boolean sentToSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getpermissions();
    }

    void getpermissions() {
        try {
            if (ActivityCompat.checkSelfPermission(Splash.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE + Manifest.permission.CAMERA + Manifest.permission.READ_EXTERNAL_STORAGE + Manifest.permission.ACCESS_FINE_LOCATION + Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Splash.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            } else {

                initControls();
//
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initControls() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (App.getAppPreferences().GetString(ConstantData.TOKEN).equalsIgnoreCase("1")){
                    Intent i = new Intent(Splash.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(Splash.this, WalkThroughActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                boolean writeStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean readStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean camera = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean locationfine = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                boolean locationcoarse = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && camera && writeStorage && readStorage && locationfine && locationcoarse) {
                    initControls();

                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                    builder.setTitle("Permissions");
                    builder.setMessage("Storage Permissions are Required");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //send to settings
                            Toast.makeText(Splash.this, "Go to Settings to Grant the Storage Permissions and restart application", Toast.LENGTH_LONG).show();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", Splash.this.getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                            .create()
                            .show();
                } else {
                    Toast.makeText(Splash.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(Splash.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
                }
                break;
        }
    }
}


