package omninos.com.sai_salon.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import omninos.com.sai_salon.R;

public class WebViewActivity extends AppCompatActivity {

    ImageView back;
    WebView webView;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        back=findViewById(R.id.back);
        webView=findViewById(R.id.webview);
        title=findViewById(R.id.title);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WebViewActivity.this,HomeActivity.class));
               // finishAffinity();
            }
        });

            webView.loadUrl(getIntent().getStringExtra("Link"));
           // title.setText(getIntent().getStringExtra("Title"));

    }
}
