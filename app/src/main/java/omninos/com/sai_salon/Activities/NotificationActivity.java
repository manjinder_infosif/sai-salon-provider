package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.NotificationAdapter;
import omninos.com.sai_salon.MyViewModels.OffersViewModel;
import omninos.com.sai_salon.PojoClasses.NotificationPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class NotificationActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private NotificationAdapter adapter;
    private ImageView back;
    TextView notification,text;
    private OffersViewModel offersViewModel;
    private List<NotificationPojo.Detail>list=new ArrayList<>();
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        offersViewModel= ViewModelProviders.of(NotificationActivity.this).get(OffersViewModel.class);
        recyclerView = findViewById(R.id.recy);
        back=(ImageView)findViewById(R.id.back);
        text=findViewById(R.id.text);
        notification=findViewById(R.id.notification);
        context = LocaleHelper.setLocale(NotificationActivity.this, App.getAppPreferences().getLanguage(NotificationActivity.this));
        resources = context.getResources();
        ChangeLanguage();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);



        offersViewModel.getnotify(NotificationActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(this, new Observer<NotificationPojo>() {
            @Override
            public void onChanged(@Nullable NotificationPojo notificationPojo) {
                if(notificationPojo.getSuccess().equalsIgnoreCase("1")){
                    adapter = new NotificationAdapter(NotificationActivity.this,notificationPojo.getDetails());
                    recyclerView.setAdapter(adapter);
                    text.setVisibility(View.GONE);
                    Toast.makeText(NotificationActivity.this, ""+notificationPojo.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else{
                    text.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        });


    }

    private void ChangeLanguage() {
        notification.setText(resources.getString(R.string.notifications));
        text.setText(resources.getString(R.string.no_notifications));

    }
}
