package omninos.com.sai_salon.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import omninos.com.sai_salon.BuildConfig;
import omninos.com.sai_salon.MyViewModels.ServicesViewModel;
import omninos.com.sai_salon.PojoClasses.GetCategoryModel;
import omninos.com.sai_salon.PojoClasses.Servicestypesspojo;
import omninos.com.sai_salon.PojoClasses.UpdateAddserviesPojo;
import omninos.com.sai_salon.PojoClasses.UpdateServiceslistPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.ImageUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.sai_salon.Util.LocaleHelper;

public class AddnewActivity extends AppCompatActivity implements View.OnClickListener {

    private Dialog dialog;
    private Button addService,updateservice;
    private ImageView back, salonImage;
    private Spinner spinner,serviceslocationspinner,servicetypespinner;
    private List<String> category = new ArrayList<>();
    private List<String> categoryId = new ArrayList<>();
    private List<String>servicestypes=new ArrayList<>();
    private List<String>servicestypesId=new ArrayList<>();
    private TextView actiontxt,typetxt,categorytxt,englishnametxt,arabicnametxt,pricetxt,timetxt,loctxt;
    private String S_category,S_servicelocation;
    private ServicesViewModel viewModel;
    private String CategoryPositionId, UserimagePath = "";
    private File photoFile;
    private static final int GALLERY_REQUEST = 101;
    private static final int CAMERA_REQUEST = 102;
    private Uri uri;
    private EditText serviceName, actualPrice, salePrice, Arabicname,servicetime;
    private String name, acprice, SPrice, Desc,salonTypeStatus,arbicname_str,servicetime_str,status,serviceid;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnew);

        viewModel = ViewModelProviders.of(this).get(ServicesViewModel.class);

        initView();
        context = LocaleHelper.setLocale(AddnewActivity.this, App.getAppPreferences().getLanguage(AddnewActivity.this));
        resources = context.getResources();
        ChangeLanguage();
        SetUps();

        serviceid=getIntent().getStringExtra("serviceid");
        status=getIntent().getStringExtra("status");
        if(status!=null) {
            if (status.equalsIgnoreCase("1")) {
              //  addService.setText("Update Services");
                updateservice.setVisibility(View.VISIBLE);
                addService.setVisibility(View.GONE);
                getfilledservices();
            } else {
                //addService.setText("Add Services");
                addService.setVisibility(View.VISIBLE);
                updateservice.setVisibility(View.GONE);
            }
        }

    }

    private void ChangeLanguage() {
        actiontxt.setText(resources.getString(R.string.add_more_services));
        typetxt.setText(resources.getString(R.string.service_type));
        categorytxt.setText(resources.getString(R.string.choose_category));
        englishnametxt.setText(resources.getString(R.string.english_service_name));
        serviceName.setHint(resources.getString(R.string.english_service_name));
        arabicnametxt.setText(resources.getString(R.string.arabic_service_name));
        Arabicname.setHint(resources.getString(R.string.arabic_service_name));
        pricetxt.setText(resources.getString(R.string.service_price));
        timetxt.setText(resources.getString(R.string.servicetime));
        loctxt.setText(resources.getString(R.string.service_location));
        addService.setText(resources.getString(R.string.add_services));
        updateservice.setText(resources.getString(R.string.update_services));

    }

    private void getfilledservices() {
        viewModel.updateservice(AddnewActivity.this,serviceid,App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(AddnewActivity.this, new Observer<UpdateAddserviesPojo>() {
            @Override
            public void onChanged(@Nullable UpdateAddserviesPojo updateAddserviesPojo) {
                if(updateAddserviesPojo.getSuccess().equalsIgnoreCase("1")){
                    //servicetypespinner.setT
                            serviceName.setText(updateAddserviesPojo.getDetails().getTitle());
                            actualPrice.setText(updateAddserviesPojo.getDetails().getSalePrice());
                            servicetime.setText(updateAddserviesPojo.getDetails().getServiceTime());
                            Arabicname.setText(updateAddserviesPojo.getDetails().getArabicTitle());
//                            if(updateAddserviesPojo.getDetails().getServiceType().equalsIgnoreCase("1")) {
//
//                           //     servicestypes.add("Women");
//                                servicetypespinner.setSelection(0,Boolean.parseBoolean(servicetypespinner.setPrompt("Women")));
//                            }
//
//                            else if(updateAddserviesPojo.getDetails().getServiceType().equalsIgnoreCase("2")){
//                                servicetypespinner.setPrompt("Men");
//                                servicestypes.add("Men");
//                            }
//                            else if(updateAddserviesPojo.getDetails().getServiceType().equalsIgnoreCase("3")){
//                                servicetypespinner.setPrompt("Kids");
//                                servicestypes.add("Kids");
//
//                            }

                }
            }
        });
    }

    private void initView() {

        addService = (Button) findViewById(R.id.addservice);
        back = findViewById(R.id.back);
        spinner = findViewById(R.id.spinner);
        serviceslocationspinner=findViewById(R.id.servicelocationspinner);
        serviceName = findViewById(R.id.serviceName);
        actualPrice = findViewById(R.id.actualPrice);
       // salePrice = findViewById(R.id.salePrice);
        servicetime = findViewById(R.id.servicetime);
        Arabicname=findViewById(R.id.ArabicName);
        salonImage = findViewById(R.id.salonImage);
        updateservice=findViewById(R.id.updateservice);
        updateservice.setOnClickListener(this);
        servicetypespinner=findViewById(R.id.servicetypespinner);
        actiontxt=findViewById(R.id.actiontext);
        typetxt=findViewById(R.id.typetxt);
        categorytxt=findViewById(R.id.categorytxt);
        englishnametxt=findViewById(R.id.englishnametxt);
        arabicnametxt=findViewById(R.id.arabicnamextx);
        pricetxt=findViewById(R.id.pricetxt);
        timetxt=findViewById(R.id.timetxt);
        loctxt=findViewById(R.id.loctxt);

    }

    private void SetUps() {
        back.setOnClickListener(this);
        dialog = new Dialog(this);
        addService.setOnClickListener(this);
        salonImage.setOnClickListener(this);

        getservicesList();
        getserviceLocation();
    }

    private void LoadList(String data) {
        category.add("-Select Category-");
        GetList(data);
       // servicestypes.add("-Select services");

    }

    private void getserviceLocation() {
        List<String> myArraySpinner = new ArrayList<String>();

        myArraySpinner.add(resources.getString(R.string.Salon));
        myArraySpinner.add(resources.getString(R.string.home));
        myArraySpinner.add(resources.getString(R.string.both));

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddnewActivity.this, R.layout.spinner_item, myArraySpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceslocationspinner.setAdapter(arrayAdapter);

       serviceslocationspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               Typeface typeface= ResourcesCompat.getFont(AddnewActivity.this,R.font.raleway_medium);
               ((TextView) view).setTypeface(typeface);
               if (App.getAppPreferences().getLanguage(AddnewActivity.this).equalsIgnoreCase("ar")){
                   if (position==0){
                       S_servicelocation="salon";
                   }else if (position==1){
                       S_servicelocation="home";
                   }
                   else if(position==2){
                       S_servicelocation="both";
                   }
               }else {
                   S_servicelocation = parent.getItemAtPosition(position).toString();
               }

           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
    }

    private void getservicesList() {

        viewModel.getservicestypes(AddnewActivity.this,App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(AddnewActivity.this, new Observer<Servicestypesspojo>() {
            @Override
            public void onChanged(@Nullable Servicestypesspojo servicestypesspojo) {
                if(servicestypesspojo.getSuccess().equalsIgnoreCase("1")) {
//                    servicestypes.add(servicestypesspojo.getDetails().getHomeServiceStatus());
                    if (servicestypesspojo.getDetails().getKidsStatus().equalsIgnoreCase("1")){
                        servicestypes.add(resources.getString(R.string.kids));
                    }

                    if (servicestypesspojo.getDetails().getManStatus().equalsIgnoreCase("1")){
                        servicestypes.add(resources.getString(R.string.men));
                    }
                    if (servicestypesspojo.getDetails().getWomanStatus().equalsIgnoreCase("1")){
                        servicestypes.add(resources.getString(R.string.women));
                    }
//                    servicestypes.add(servicestypesspojo.getDetails().getKidsStatus());
//                    servicestypes.add(servicestypesspojo.getDetails().getManStatus());
//                    servicestypes.add(servicestypesspojo.getDetails().getWomanStatus());
                }
                    ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(AddnewActivity.this, R.layout.spinner_item,servicestypes); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    servicetypespinner.setAdapter(spinnerArrayAdapter1);
                    servicetypespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface= ResourcesCompat.getFont(AddnewActivity.this,R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);

                            if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.men))){
                                salonTypeStatus="2";

                                LoadList("2");
                            }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.women))){
                                salonTypeStatus="1";

                                LoadList("1");
                            }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase(resources.getString(R.string.kids))) {
                                salonTypeStatus = "3";
                                LoadList("3");
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


            }
        });
    }

    private void GetList(String data) {
        if (category != null) {
            category.clear();
        }
        if (categoryId != null) {
            categoryId.clear();
        }
        viewModel.getCategoryModelLiveData(AddnewActivity.this, data).observe(AddnewActivity.this, new Observer<GetCategoryModel>() {
            @Override
            public void onChanged(@Nullable GetCategoryModel getCategoryModel) {
                if (getCategoryModel.getSuccess().equalsIgnoreCase("1")) {
                    for (int i = 0; i < getCategoryModel.getDetails().size(); i++) {
                        if (App.getAppPreferences().getLanguage(AddnewActivity.this).equalsIgnoreCase("ar")) {
                            category.add(getCategoryModel.getDetails().get(i).getArabicTitle());
                            categoryId.add(getCategoryModel.getDetails().get(i).getId());
                        } else {
                            category.add(getCategoryModel.getDetails().get(i).getTitle());
                            categoryId.add(getCategoryModel.getDetails().get(i).getId());
                        }
                    }


                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(AddnewActivity.this, R.layout.spinner_item, category); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override

                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Typeface typeface= ResourcesCompat.getFont(AddnewActivity.this,R.font.raleway_medium);
                            ((TextView) view).setTypeface(typeface);

                            S_category = parent.getItemAtPosition(position).toString();
                            CategoryPositionId = categoryId.get(position);

                        }
                        @Override

                        public void onNothingSelected(AdapterView<?> parent) {
                                                    }

                    });

                } else {

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addservice:
//                dialogbox();
                AddService();
//                Toast.makeText(this, CategoryPositionId, Toast.LENGTH_SHORT).show();
                break;
            case R.id.back:
                onBackPressed();
                break;

            case R.id.salonImage:
                OpenImageData();
                break;
            case R.id.updateservice:
                UpdateService();
        }
    }

    private void UpdateService() {
        name = serviceName.getText().toString();
        acprice = actualPrice.getText().toString();
        // SPrice = salePrice.getText().toString();
        // Desc = descriptionText.getText().toString();
        arbicname_str=Arabicname.getText().toString();
        servicetime_str=servicetime.getText().toString();
        viewModel.getupdatedservice(AddnewActivity.this,salonTypeStatus,App.getAppPreferences().GetString(ConstantData.USER_ID),S_servicelocation,CategoryPositionId,name,arbicname_str,acprice,servicetime_str,serviceid).observe(AddnewActivity.this, new Observer<UpdateServiceslistPojo>() {
            @Override
            public void onChanged(@Nullable UpdateServiceslistPojo updateServiceslistPojo) {
                if(updateServiceslistPojo.getSuccess().equalsIgnoreCase("1")){
                    Toast.makeText(AddnewActivity.this, ""+updateServiceslistPojo.getMessage(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(AddnewActivity.this,HomeservicesActivity.class));

                }
            }
        });
    }

    private void AddService() {
        name = serviceName.getText().toString();
        acprice = actualPrice.getText().toString();
       // SPrice = salePrice.getText().toString();
       // Desc = descriptionText.getText().toString();
        arbicname_str=Arabicname.getText().toString();
        servicetime_str=servicetime.getText().toString();

         if (name.isEmpty()) {
            Toast.makeText(this, "Enter service name", Toast.LENGTH_SHORT).show();
        } else if (acprice.isEmpty()) {
            Toast.makeText(this, "Enter actual price", Toast.LENGTH_SHORT).show();
        } else if (arbicname_str.isEmpty()) {
            Toast.makeText(this, "Enter arabic name", Toast.LENGTH_SHORT).show();
        } else if (servicetime_str.isEmpty()) {
            Toast.makeText(this, "Enter Service Time", Toast.LENGTH_SHORT).show();
        } else if (acprice.isEmpty()) {
            Toast.makeText(this, "Enter Sale price ", Toast.LENGTH_SHORT).show();
        } else {
//
//            RequestBody providerIdbody = RequestBody.create(MediaType.parse("text/plain"), App.getAppPreferences().GetString(ConstantData.USER_ID));
//            RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), CategoryPositionId);
//            RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), name);
//            RequestBody actualPriceBody = RequestBody.create(MediaType.parse("text/plain"), acprice);
//            RequestBody arbicnamebody = RequestBody.create(MediaType.parse("text/plain"), arbicname_str);
//            RequestBody servicetimebody = RequestBody.create(MediaType.parse("text/plain"), servicetime_str);
//            RequestBody servicestypbody=RequestBody.create(MediaType.parse("text/plain"),salonTypeStatus);
//            RequestBody serviceLocationbody=RequestBody.create(MediaType.parse("text/plain"),S_servicelocation);
//            File file = new File(UserimagePath);
//            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//            MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), body);

            viewModel.addnewService(AddnewActivity.this, salonTypeStatus,App.getAppPreferences().GetString(ConstantData.USER_ID),S_servicelocation,CategoryPositionId,name,arbicname_str,acprice,servicetime_str).observe(AddnewActivity.this, new Observer<Map>() {
                @Override
                public void onChanged(@Nullable Map map) {
                    if (map.get("success").equals("1")) {
                        dialogbox();
                    } else {

                    }
                }
            });
        }
    }

    public void dialogbox() {
        dialog.setContentView(R.layout.success_msg);
        Button okButton = dialog.findViewById(R.id.okButton);
        TextView  text=dialog.findViewById(R.id.text);
        okButton.setText(resources.getString(R.string.ok));
        text.setText(resources.getString(R.string.your_service_has_been_successfully_added));
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        dialog.show();
    }


    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AddnewActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(AddnewActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
//                App.getAppPreference().SaveString(activity, ConstantData.IMAGEPATH, ImageUtil.compressImage(photoFile.getAbsolutePath()));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
//                userImage.setImageBitmap(mBitmapInsurance);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotated = Bitmap.createBitmap(mBitmapInsurance, 0, 0, mBitmapInsurance.getWidth(), mBitmapInsurance.getHeight(),
                        matrix, true);

                if (Build.VERSION.SDK_INT > 25 && Build.VERSION.SDK_INT < 27) {
                    // Do something for lollipop and above versions
                    salonImage.setImageBitmap(rotated);
                } else {
                    // do something for phones running an SDK before lollipop
                    salonImage.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
//                App.getAppPreference().SaveString(activity, ConstantData.IMAGEPATH, UserimagePath);
                Glide.with(AddnewActivity.this).load("file://" + UserimagePath).into(salonImage);
            }

        }
    }

}

