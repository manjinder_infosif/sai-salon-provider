package omninos.com.sai_salon.Activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import omninos.com.sai_salon.R;

public class MessageActivity extends AppCompatActivity {
    ImageView image,back;
    TextView messgae,title,adminmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        back=findViewById(R.id.back);
        image=findViewById(R.id.image);
        messgae=findViewById(R.id.message);
        title=findViewById(R.id.title);
        adminmsg=findViewById(R.id.adminmsg);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(MessageActivity.this,HomeActivity.class));
               // finishAffinity();
            }
        });

        Glide.with(this).load(getIntent().getStringExtra("Image")).into(image);
        messgae.setText(getIntent().getStringExtra("Subtitle"));
        adminmsg.setText(getIntent().getStringExtra("Message"));
       // title.setText(getIntent().getStringExtra("Title"));
    }
}
