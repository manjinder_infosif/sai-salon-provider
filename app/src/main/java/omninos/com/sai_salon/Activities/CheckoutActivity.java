package omninos.com.sai_salon.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.session.MediaSession;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import omninos.com.sai_salon.MyViewModels.ViewModelNext;
import omninos.com.sai_salon.PojoClasses.GetCoupondetails;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.Commonutil;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.Util.LocaleHelper;

public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back;
    private Button register, debit, credit, apply;
    private TextView textcard,textdata,tv2,tv3,tv1;
    private ViewModell viewModell;
    private int count=0;
    private StringBuilder builder;
    Dialog dialog,dialog1;
    Context context;
    Resources resources;
    Button ok,okk;

    TextView applycoupontxt,cardpaymnttxt,checkoutaction;
    private CardView card1,card2,cardpaymnt1,cardpymt2;
    private ViewModelNext viewModelNext;
    private EditText cardno, monthedit, yearedit, CVV, coupon;
    private String cardoptionstr, cardstr, monthstr, yearstr, cvvstr, cardexpiry, couponstr;
    private int currentMonth,currentYear,cardYear,cardMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        viewModelNext=ViewModelProviders.of(this).get(ViewModelNext.class);
        builder = new StringBuilder();
        viewModell = ViewModelProviders.of(CheckoutActivity.this).get(ViewModell.class);
        initID();
        context = LocaleHelper.setLocale(CheckoutActivity.this, App.getAppPreferences().getLanguage(CheckoutActivity.this));
        resources = context.getResources();
        changelanguage();

    }

    private void changelanguage() {
        checkoutaction.setText(resources.getString(R.string.checkout));
        applycoupontxt.setText(resources.getString(R.string.apply_coupon));
        cardpaymnttxt.setText(resources.getString(R.string.card_payment));
        register.setText(resources.getString(R.string.submit));
        apply.setText(resources.getString(R.string.apply));
        debit.setText(resources.getString(R.string.debit_card));
        credit.setText(resources.getString(R.string.credit_card));
    }

    private void applyCoupon() {
        couponstr=coupon.getText().toString();
        if(couponstr.isEmpty()){
            Toast.makeText(this, "Write Coupon name", Toast.LENGTH_SHORT).show();
        }
        else{
            viewModelNext.getcoupon(CheckoutActivity.this,App.getAppPreferences().GetString(ConstantData.USER_ID),couponstr,App.getAppPreferences().GetString(ConstantData.PACKAGEID)).observe(CheckoutActivity.this, new Observer<GetCoupondetails>() {
                @Override
                public void onChanged(@Nullable GetCoupondetails getCoupondetails) {
                    if(getCoupondetails.getSuccess().equalsIgnoreCase("1")){
                        dialog();
                    }
                    else{
                        dialogerror();
                    }
                }


            });
        }
    }

    private void dialogerror() {
        dialog=new Dialog(this);
        dialog.setContentView(R.layout.errorcoupondialog);
        tv3=dialog.findViewById(R.id.tv3);
        tv1=dialog.findViewById(R.id.tv1);
        ok=dialog.findViewById(R.id.ok);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tv3.setText(resources.getString(R.string.your_coupon_does_nt_match_n_please_try_again));
        tv1.setText(resources.getString(R.string.ooops));
        ok.setText(resources.getString(R.string.ok));

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialog() {
        dialog1=new Dialog(this);
        dialog1.setContentView(R.layout.successlayout);
        tv2=dialog1.findViewById(R.id.tv2);
        textdata=dialog1.findViewById(R.id.textdata);
        okk=dialog1.findViewById(R.id.okk);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tv2.setText(resources.getString(R.string.your_coupon_matches));
        textdata.setText(resources.getString(R.string.hurrah));

        okk.setText(resources.getString(R.string.ok));
        okk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckoutActivity.this,HomeActivity.class));
                dialog1.dismiss();
                finishAffinity();
            }
        });
        dialog1.show();
    }


    private void initID() {

        back = findViewById(R.id.back);

        register = findViewById(R.id.register);
        back.setOnClickListener(this);
        register.setOnClickListener(this);
        debit = findViewById(R.id.debit);
        credit = findViewById(R.id.credit);
        textcard = findViewById(R.id.textcardd);
        debit.setOnClickListener(this);
        credit.setOnClickListener(this);
        cardno = findViewById(R.id.card);
        monthedit = findViewById(R.id.month);
        yearedit = findViewById(R.id.year);
        monthedit.setOnClickListener(this);
        yearedit.setOnClickListener(this);
        CVV = findViewById(R.id.cvv);

        coupon = findViewById(R.id.coupon);
        apply = findViewById(R.id.apply);
        apply.setOnClickListener(this);
        card1=findViewById(R.id.card1);
        card1.setOnClickListener(this);
        checkoutaction=findViewById(R.id.chkoutaction);
        applycoupontxt=findViewById(R.id.applycoupontxt);
        cardpaymnttxt=findViewById(R.id.cardpaymnttxt);
        card2=findViewById(R.id.card2);
        cardpaymnt1=findViewById(R.id.cardpaymt);
        cardpaymnt1.setOnClickListener(this);
        cardpymt2=findViewById(R.id.cardpymt2);
        cardpymt2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.register:
                registerr();
                // startActivity(new Intent(CheckoutActivity.this,HomeActivity.class));
                break;
            case R.id.debit:
                debit.setBackgroundResource(R.drawable.btnback);
                debit.setTextColor(Color.parseColor("#ffffff"));
                credit.setBackgroundResource(R.drawable.btn1);
                credit.setTextColor(Color.parseColor("#000000"));
                textcard.setText("Use Debit Card");
                cardoptionstr = "debit";
                break;
            case R.id.credit:
                credit.setBackgroundResource(R.drawable.btnback);
                credit.setTextColor(Color.parseColor("#ffffff"));
                textcard.setText("Use Credit Card");
                cardoptionstr = "credit";
                debit.setBackgroundResource(R.drawable.btn1);
                debit.setTextColor(Color.parseColor("#000000"));
            case R.id.apply:
                applyCoupon();
                break;
            case R.id.month:
                count=1;
                PickDate();
                break;
            case R.id.year:
                count=2;
                PickDate();
                break;
            case R.id.card1:
                card2.setVisibility(View.VISIBLE);
                cardpymt2.setVisibility(View.GONE);
                break;
            case R.id.cardpaymt:
                cardpymt2.setVisibility(View.VISIBLE);
                card2.setVisibility(View.GONE);
                break;
        }

    }

    private void PickDate() {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        currentMonth = mMonth;
        currentYear = mYear;
        DatePickerDialog mDatePicker = new DatePickerDialog(
                CheckoutActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH, selectedday);
                cardMonth = selectedmonth + 1;
                cardYear = selectedyear;
                SimpleDateFormat sdf = new SimpleDateFormat("MM", Locale.US);

                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy", Locale.US);

                if(count==1) {
                    monthedit.setText(sdf.format(mcurrentDate.getTime()));
                }
                if(count==2) {
                    yearedit.setText(sdf1.format(mcurrentDate.getTime()));
                }
            }
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.setTitle("Choose Expirey date");
        mDatePicker.show();
    }

    private void registerr() {
        cardstr = cardno.getText().toString();
//        monthstr = monthedit.getText().toString();
//        yearstr = yearedit.getText().toString();
        cvvstr = CVV.getText().toString();
        cardexpiry = String.valueOf(builder.append(monthstr + yearstr));
        App.getSingletonPojo().setCardcvv(cvvstr);
        App.getSingletonPojo().setCardno(cardstr);
        App.getSingletonPojo().setCardexpiry(cardexpiry);
        App.getSingletonPojo().setCoupon(couponstr);
        App.getSingletonPojo().setCardtype(cardoptionstr);
        String userid = App.getAppPreferences().GetString(ConstantData.USER_ID);
        String packageid = App.getSingletonPojo().getPackageid();
        if (cardstr.isEmpty()) {
            Toast.makeText(this, "Enter Card Number", Toast.LENGTH_SHORT).show();
        } else if (cvvstr.isEmpty()) {
            Toast.makeText(this, "Enter Card CVV", Toast.LENGTH_SHORT).show();
        } else if (currentYear <= cardYear) {
            if (currentYear == cardYear) {
                if (currentMonth > cardMonth) {
                    Toast.makeText(CheckoutActivity.this, "Enter valid exp month", Toast.LENGTH_SHORT).show();
                } else {
                    Verifycard(cardstr, cardMonth, cardYear, cvvstr);
                }
            } else {
                Verifycard(cardstr, cardMonth, cardYear, cvvstr);
            }
        } else {
            Toast.makeText(CheckoutActivity.this, "Enter valid exp Year", Toast.LENGTH_SHORT).show();
        }

    }

        public void Verifycard(String cardNumber, int cardExpMonth, int cardExpYear, String cardCVC) {
            Commonutil.showProgress(CheckoutActivity.this);
            Card card = new Card(
                    cardNumber,
                    cardExpMonth,
                    cardExpYear,
                    cardCVC
            );

            card.validateNumber();
            card.validateCVC();

            if (card != null) {
                Stripe stripe = new Stripe(this, "pk_test_BvAnXwzQ41NgEnMpB7x74pH2");
                stripe.createToken(card, new TokenCallback() {
                    @Override
                    public void onError(Exception error) {
                        Commonutil.dismiss();
                        Toast.makeText(CheckoutActivity.this, "Error " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(Token token) {
                        Commonutil.dismiss();
                        Toast.makeText(CheckoutActivity.this, "Success " + token.getId(), Toast.LENGTH_SHORT).show();
                        Log.d("onSuccess: ", token.getId());
                    }


                });
            }
        }
    }


//
// {
//         viewModell.getpackagesdetail(CheckoutActivity.this, packageid, userid).observe(this, new Observer<Map>() {
//@Override
//public void onChanged(@Nullable Map map) {
//        if (map.get("success").equals("1")) {
//        App.getAppPreferences().SaveString(ConstantData.TOKEN, "1");
//        startActivity(new Intent(CheckoutActivity.this, HomeActivity.class));
//        Toast.makeText(CheckoutActivity.this, "Successful", Toast.LENGTH_SHORT).show();
//        }
//        }
//        });
//
//        }


//        MultipartBody.Part logo;
//
////        String[] employitems=App.getSingletonPojo().getEmployeelist().toArray(new String[App.getSingletonPojo().getEmployeelist().size()]);
//
//        RequestBody[] employitems = new RequestBody[App.getSingletonPojo().getEmployeelist().size()];
//        for (int i = 0; i < App.getSingletonPojo().getEmployeelist().size(); i++) {
////            employitems[i]=App.getSingletonPojo().getEmployeelist().get(i).getEmployeename();
//            RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getEmployeelist().get(i).getEmployeename());
//            employitems[i] = requestBody;
//        }
//
//        RequestBody[] serviceBody = new RequestBody[App.getSingletonPojo().getServicesid().size()];
//        for (int i = 0; i < App.getSingletonPojo().getServiceslist().size(); i++) {
//            RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getServicesid().get(i));
//            serviceBody[i] = requestBody;
//        }
//
//        File file = new File(App.getSingletonPojo().getSalonphotopath());
//        final RequestBody rf = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        logo = MultipartBody.Part.createFormData("salonLogo", file.getName(), rf);
//
//        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getuserId());
//        RequestBody motNumber = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getMotno());
//        RequestBody licensecopy = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getLicensecopy());
//        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getName());
//        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getUseremail());
//        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getAddress());
//        RequestBody location = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getLocation());
//        RequestBody sundaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getSaturdayopen());
//        RequestBody sundayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getSundayclose());
//        RequestBody mondaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getMondayopen());
//        RequestBody mondayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getMondayclose());
//        RequestBody tuesdaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getTuesdayopen());
//        RequestBody tuesdayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getTuesdayclose());
//        RequestBody wednesdaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getWednesdayopen());
//        RequestBody wednesdayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getWednesdayclose());
//        RequestBody thurdaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getThursdayopen());
//        RequestBody thurdayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getThursdayclose());
//        RequestBody fridaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getFridayopen());
//        RequestBody fridayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getFridayclose());
//        RequestBody saturdaystart = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getSaturdayopen());
//        RequestBody saturdayclose = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getSaturdayclose());
////        RequestBody images[]=RequestBody.;
//        RequestBody cardType = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getCardtype());
//        RequestBody cardNumber = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getCardno());
//        RequestBody cardexpiry = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getCardexpiry());
//        RequestBody cvv = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getCardcvv());
//        RequestBody coupon = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getCoupon());
//        RequestBody surname = RequestBody.create(MediaType.parse("text/plain"), App.getSingletonPojo().getSurname());
//
//        MultipartBody.Part[] imagesData = new MultipartBody.Part[App.getSingletonPojo().getPhotoslist().size()];
//        for (int i = 0; i < App.getSingletonPojo().getPhotoslist().size(); i++) {
//
//            File file1 = new File(App.getSingletonPojo().getPhotoslist().get(i));
//            final RequestBody rf1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
//            imagesData[i] = MultipartBody.Part.createFormData("salonLogo", file.getName(), rf1);
//        }
//
////        if (Commonutil.InternetCheck(CheckoutActivity.this)) {
////            viewModell.getinfo(this,userId,motNumber,licensecopy,name,email,address,location,serviceBody,sundaystart,sundayclose,mondaystart,mondayclose,tuesdaystart,tuesdayclose,
////                    wednesdaystart,wednesdayclose,thurdaystart,thurdayclose,fridaystart,fridayclose,saturdaystart,saturdayclose,logo,imagesData,employitems,cardType,cardNumber,cardexpiry,cvv,coupon,surname).observe(this, new Observer<ProviderinfoPojo>() {
////                @Override
////                public void onChanged(@Nullable ProviderinfoPojo providerinfoPojo) {
////
////                    //if(providerinfoPojo.getSucc)
////
////
////                }
////            });
//
//        }


//}
