package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.GetSpecialoffersAdapter;
import omninos.com.sai_salon.MyViewModels.NewViewModel;
import omninos.com.sai_salon.PojoClasses.DeleteOfferPojo;
import omninos.com.sai_salon.PojoClasses.DeletePojo;
import omninos.com.sai_salon.PojoClasses.GetOffersListPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class GetSpecialOfferActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView back;
    Button add;
    RecyclerView offersrv;
    NewViewModel viewModel;
    AlertDialog.Builder builder;
    TextView text,offerstext;
    GetSpecialoffersAdapter adapter;
    List<GetOffersListPojo.Detail>list=new ArrayList<>();
    Context context;
    Resources resources;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_special_offer);
        viewModel= ViewModelProviders.of(this).get(NewViewModel.class);
        id();
        setview();
        context = LocaleHelper.setLocale(GetSpecialOfferActivity.this, App.getAppPreferences().getLanguage(GetSpecialOfferActivity.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        offerstext.setText(resources.getString(R.string.special_offers));
        add.setText(resources.getString(R.string.add_more_special_offers));
        text.setText(resources.getString(R.string.add_offers_first));
    }

    private void setview() {
        if(list!=null){
            list.clear();
        }
        viewModel.getofferlist(GetSpecialOfferActivity.this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(GetSpecialOfferActivity.this, new Observer<GetOffersListPojo>() {
            @Override
            public void onChanged(@Nullable final GetOffersListPojo getOffersListPojo) {
                if(getOffersListPojo.getSuccess().equalsIgnoreCase("1")){
                    list=getOffersListPojo.getDetails();
                    adapter=new GetSpecialoffersAdapter(GetSpecialOfferActivity.this, getOffersListPojo.getDetails(), new GetSpecialoffersAdapter.DeleteOffer() {
                        @Override
                        public void delete(final int position) {
                            String id=getOffersListPojo.getDetails().get(position).getId();
                              Opendialog(id,position);


                        }
                    });
                    offersrv.setAdapter(adapter);
                    text.setVisibility(View.GONE);
                }
                else{
                    offersrv.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void Opendialog(final String id, final int position) {
        builder. setTitle("").setMessage(resources.getString(R.string.deleteoffertxt))
                .setCancelable(false).setPositiveButton(resources.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                deleteoffer(id,position);


            }
        }).setNegativeButton(resources.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.show();

    }

    private void deleteoffer(String id, final int position) {
        viewModel.deleteoffer(GetSpecialOfferActivity.this,id).observe(GetSpecialOfferActivity.this, new Observer<DeleteOfferPojo>() {
            @Override
            public void onChanged(@Nullable DeleteOfferPojo deletePojo) {
                if(deletePojo.getSuccess().equalsIgnoreCase("1")){
                    list.remove(list.get(position));
                    adapter.notifyDataSetChanged();
                    setview();
                    adapter.notifyDataSetChanged();

                }
            }
        });
    }

    private void id() {
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        add = findViewById(R.id.add);
        add.setOnClickListener(this);
        text=findViewById(R.id.text);
        offerstext=findViewById(R.id.offerstext);
        builder=new AlertDialog.Builder(this);
        offersrv=findViewById(R.id.offersrv);
        offersrv.setLayoutManager(new LinearLayoutManager(GetSpecialOfferActivity.this,LinearLayoutManager.VERTICAL,false));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                startActivity(new Intent(GetSpecialOfferActivity.this,HomeActivity.class));
            break;
            case R.id. add:
                startActivity(new Intent(GetSpecialOfferActivity.this,PackageActivity.class));
                break;

        }

    }
}
