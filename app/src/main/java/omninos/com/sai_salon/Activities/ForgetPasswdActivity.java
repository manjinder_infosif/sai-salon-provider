package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.PojoClasses.ForgetPasswordPojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.LocaleHelper;

public class ForgetPasswdActivity extends AppCompatActivity implements View.OnClickListener {

    Button chnage;
    EditText newpassswd,confirmpasswd;
    String newpasswdstr,confirmpassstr;
    ViewModell viewModell;
    Context context;
    TextView tv1,tv2;
    Resources resources;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_passwd);
        viewModell= ViewModelProviders.of(this).get(ViewModell.class);
        chnage=findViewById(R.id.chnage);
        newpassswd=findViewById(R.id.passwdnew);
        confirmpasswd=findViewById(R.id.passwdconfirm);
        tv1=findViewById(R.id.tv1);
        tv2=findViewById(R.id.tv2);
        chnage.setOnClickListener(this);
        context = LocaleHelper.setLocale(ForgetPasswdActivity.this, App.getAppPreferences().getLanguage(ForgetPasswdActivity.this));
        resources = context.getResources();
        ChangeLanguage();
    }

    private void ChangeLanguage() {
        tv1.setText(resources.getString(R.string.new_password));
        tv2.setText(resources.getString(R.string.confirm_password));
        chnage.setText(resources.getString(R.string.change));
    }

    private void validate() {
        newpasswdstr=newpassswd.getText().toString();
        confirmpassstr=confirmpasswd.getText().toString();
        if(newpasswdstr.isEmpty()||confirmpassstr.isEmpty()){
            Toast.makeText(this, "Enter password", Toast.LENGTH_SHORT).show();
        }
        else if(!confirmpassstr.matches(newpasswdstr)){
            Toast.makeText(this, "Password doesnot match", Toast.LENGTH_SHORT).show();
        }
        else{
            viewModell.getupdatepassswd(this,App.getSingletonPojo().getPhoneNumber(),newpasswdstr).observe(ForgetPasswdActivity.this, new Observer<ForgetPasswordPojo>() {
                @Override
                public void onChanged(@Nullable ForgetPasswordPojo forgetPasswordPojo) {
                    if(forgetPasswordPojo.getSuccess().equalsIgnoreCase("1")){
                        startActivity(new Intent(ForgetPasswdActivity.this,LoginActivity.class));
                        Toast.makeText(ForgetPasswdActivity.this, ""+forgetPasswordPojo.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        Toast.makeText(ForgetPasswdActivity.this, ""+forgetPasswordPojo.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
            });


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.chnage:
                validate();
                break;

        }
    }
}
