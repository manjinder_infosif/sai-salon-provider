package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.PojoClasses.Loginpojo;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.Commonutil;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.MyViewModels.ViewModell;
import omninos.com.sai_salon.Util.LocaleHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView register,forgetpassword,loginaction,donthaveacctv,emailtv,passwdtv;
    private LinearLayout reglay;
    private Button Login;
    private ViewModell viewModell;
    private ImageView back;
    private URL fbProfilePicture;
    private GoogleSignInClient mgoogleSignInClient;
    private int RC_SIGN_IN = 100;
    public static int APP_REQUEST_CODE = 99;
    private List<String>salontypestatus=new ArrayList<>();
    private EditText email, password;
    private String emailstr, passwordstr, device_type = "android", reg_id = "",man_womanstatus,kidestatus,homestatus;
    Context context;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initId();
        viewModell = ViewModelProviders.of(LoginActivity.this).get(ViewModell.class);
        register.setPaintFlags(register.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        context = LocaleHelper.setLocale(LoginActivity.this, App.getAppPreferences().getLanguage(LoginActivity.this));
        resources = context.getResources();

        ChangeLanguage();
    }

    private void ChangeLanguage() {
        loginaction.setText(resources.getString(R.string.login));
        emailtv.setText(resources.getString(R.string.email));
        email.setHint(resources.getString(R.string.email));
        passwdtv.setText(resources.getString(R.string.password));
        password.setHint(resources.getString(R.string.password));
        forgetpassword.setText(resources.getString(R.string.forgot_password));
        Login.setText(resources.getString(R.string.login));
        donthaveacctv.setText(resources.getString(R.string.don_t_have_an_account));
        register.setText(resources.getString(R.string.register));
    }

    private void initId() {
        FirebaseApp.initializeApp(LoginActivity.this);
        register = findViewById(R.id.register);
        reglay = findViewById(R.id.reglay);
        reglay.setOnClickListener(this);
        Login = findViewById(R.id.btnlogin);
        Login.setOnClickListener(this);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        forgetpassword=findViewById(R.id.forgetPassword);
        forgetpassword.setOnClickListener(this);
        emailtv=findViewById(R.id.emailtv);
        passwdtv=findViewById(R.id.passedtv);
        loginaction=findViewById(R.id.loginaction);
        donthaveacctv=findViewById(R.id.donthaveacctv);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reglay:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
            case R.id.btnlogin:
                login();
                break;
            case R.id.forgetPassword:
                CheckPhone();
                break;
        }

    }
    private void CheckPhone() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if (requestCode == APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {
                Toast.makeText(this, loginResult.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                Toast.makeText(this, "Login Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                if (loginResult.getAccessToken() != null) {
//                    Toast.makeText(this, "Success" + loginResult.getAccessToken().getAccountId(), Toast.LENGTH_SHORT).show();
                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(Account account) {
                            String phone=account.getPhoneNumber().toString();
                            App.getSingletonPojo().setPhoneNumber(phone);
                            startActivity(new Intent (LoginActivity.this,ForgetPasswdActivity.class));

                            String StrCountryCode = account.getPhoneNumber().getCountryCode();

                        }

                        @Override
                        public void onError(AccountKitError accountKitError) {

                        }
                    });

                } else {
                    Toast.makeText(this, loginResult.getAuthorizationCode(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }
    private void login() {

        emailstr = email.getText().toString();
        passwordstr = password.getText().toString();

        if (Commonutil.InternetCheck(LoginActivity.this)) {
            viewModell.getlogin(LoginActivity.this, emailstr, passwordstr, FirebaseInstanceId.getInstance().getToken(), device_type).observe(LoginActivity.this, new Observer<Loginpojo>() {
                @Override
                public void onChanged(@Nullable Loginpojo loginpojo) {
                    if (loginpojo.getSuccess().equalsIgnoreCase("1")) {

                        String packageid = loginpojo.getDetails().getPackageId();
                        String activeStatus = loginpojo.getDetails().getActiveStatus();

                        if (activeStatus.equalsIgnoreCase("1")) {
                            if (packageid.equalsIgnoreCase("0")) {
                                if (loginpojo.getDetails().getSalonType().equalsIgnoreCase("0")){
                                    salontypestatus.add("Woman");
                                }else if (loginpojo.getDetails().getSalonType().equalsIgnoreCase("1")){
                                    salontypestatus.add("Man");
                                }
                                if (loginpojo.getDetails().getKidsStatus().equalsIgnoreCase("1")){
                                    salontypestatus.add("Kids");
                                }
                                else{
                                    salontypestatus.remove("Kids");
                                }
                                if(loginpojo.getDetails().getHomeServiceStatus().equalsIgnoreCase("1")){
                                    salontypestatus.add("HomeServices");
                                }
                                App.getAppPreferences().setSalonlist(salontypestatus);
                                App.getAppPreferences().SaveString(ConstantData.USER_ID, loginpojo.getDetails().getId());
                                App.getAppPreferences().SaveString(ConstantData.EMAIL,loginpojo.getDetails().getEmail());
                                App.getAppPreferences().SaveString(ConstantData.SALONNAME,loginpojo.getDetails().getSalonName());
                                App.getAppPreferences().SaveString(ConstantData.PHONE,loginpojo.getDetails().getPhoneNumber());
                                App.getAppPreferences().SaveString(ConstantData.SALONIMAGE,loginpojo.getDetails().getSalonImage());
                                //loginpojo.getDetails().getRegId();
                                App.getAppPreferences().SaveString(ConstantData.SALONNAME_ARABIC,loginpojo.getDetails().getSalonNameArbic());
                                startActivity(new Intent(LoginActivity.this, PackageSubscriptionActivity.class));
                                finishAffinity();
                            } else {
                                App.getAppPreferences().SaveString(ConstantData.EMAIL,loginpojo.getDetails().getEmail());
                                App.getAppPreferences().SaveString(ConstantData.SALONNAME,loginpojo.getDetails().getSalonName());
                                App.getAppPreferences().SaveString(ConstantData.PHONE,loginpojo.getDetails().getPhoneNumber());
                                App.getAppPreferences().SaveString(ConstantData.SALONIMAGE,loginpojo.getDetails().getSalonImage());
                                App.getAppPreferences().SaveString(ConstantData.USER_ID, loginpojo.getDetails().getId());
                                App.getAppPreferences().SaveString(ConstantData.TOKEN, "1");
                                App.getAppPreferences().SaveString(ConstantData.SALONNAME_ARABIC,loginpojo.getDetails().getSalonNameArbic());
                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                finishAffinity();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, "Documents are not verifed", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(LoginActivity.this, "" + loginpojo.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }
}
