package omninos.com.sai_salon.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.sai_salon.Adapters.GetEmployeesAdapter;
import omninos.com.sai_salon.MyViewModels.NewViewModel;
import omninos.com.sai_salon.PojoClasses.DeleteEmployeepojo;
import omninos.com.sai_salon.PojoClasses.GetEmployeelist;
import omninos.com.sai_salon.R;
import omninos.com.sai_salon.Util.App;
import omninos.com.sai_salon.Util.ConstantData;
import omninos.com.sai_salon.Util.LocaleHelper;

public class GetEmployessActivty extends AppCompatActivity implements View.OnClickListener {
    Button add;
    ImageView back;
    RecyclerView employeesrv;
    NewViewModel viewModel;
    GetEmployeesAdapter adapter;
    TextView actiontxt;
    List<GetEmployeelist.Detail> list = new ArrayList<>();
    Context context;
    TextView teext;
    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_employess_activty);
        viewModel = ViewModelProviders.of(this).get(NewViewModel.class);

        context = LocaleHelper.setLocale(GetEmployessActivty.this, App.getAppPreferences().getLanguage(GetEmployessActivty.this));
        resources = context.getResources();

        add = findViewById(R.id.add);
        back = findViewById(R.id.back);
        back.setOnClickListener(this);
        add.setOnClickListener(this);
        teext=findViewById(R.id.teext);
        actiontxt=findViewById(R.id.actiontxt);
        employeesrv = findViewById(R.id.employessrv);
        ChangeLanguage();
        employeesrv.setLayoutManager(new LinearLayoutManager(GetEmployessActivty.this, LinearLayoutManager.VERTICAL, false));

        viewModel.getemployess(this, App.getAppPreferences().GetString(ConstantData.USER_ID)).observe(this, new Observer<GetEmployeelist>() {
            @Override
            public void onChanged(@Nullable GetEmployeelist getEmployeelist) {
                if (getEmployeelist.getSuccess().equalsIgnoreCase("1")) {
                    list = getEmployeelist.getDetails();
                    adapter = new GetEmployeesAdapter(GetEmployessActivty.this, getEmployeelist.getDetails(), new GetEmployeesAdapter.Delete() {
                        @Override
                        public void deleteemp(final int position, String id) {
                            viewModel.deletemp(GetEmployessActivty.this, id).observe(GetEmployessActivty.this, new Observer<DeleteEmployeepojo>() {
                                @Override
                                public void onChanged(@Nullable DeleteEmployeepojo deleteEmployeepojo) {
                                    if (deleteEmployeepojo.getSuccess().equalsIgnoreCase("1")) {
                                        list.remove(list.get(position));
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            });
                        }
                    });
                    employeesrv.setAdapter(adapter);
                    teext.setVisibility(View.GONE);

                }
                else{
                    employeesrv.setVisibility(View.GONE);
                    teext.setVisibility(View.VISIBLE);
                }
            }
        });


    }

    private void ChangeLanguage() {
        actiontxt.setText(resources.getString(R.string.employees));
        add.setText(resources.getString(R.string.add_employee));
        teext.setText(resources.getString(R.string.no_employess));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.add:
                startActivity(new Intent(GetEmployessActivty.this,EmployessActivity.class));
                break;
        }
    }
}
